# GPS-tracking-app
This app implements basic Taxi-like functionalities with GPS tracking.

## Requirements
Minimum supported Android Studio version to be used with this project is AS 3.0
To import project, select Import -> select *settings.gradle* file in root of the project.
 
## Architecture
The project is splitted into four modules following *Clean Architecture* principles:

1. **App** - the app itself. 
2. **Data** - all repository implementations, DAO-s, API clients, API services. 
3. **Device** - device specific classes implementations such as TimeProvider, LocationRepository. 
4. **Domain** - the business logic of the app, repository definitions, models, use cases. This module is pure Java library without any knowledge of Android system. 

Communication between data layer and UI layer is achieve with MVP (Model - View - Presenter) pattern.
The structure of the *App* module is as follows:

1. **Application** - the core application class. 
2. **Base** - all the base UI classes used in the project (BaseActivity, BaseFragment, etc...) 
3. **Injection** - dependency injection relevant stuff. Components and modules. 
4. **Maintenance** - classes whose role is to keep app alive when app is in the background mode.  
5. **Notification** - notification relevant stuff. 
6. **UI** - the core of the app, with subpackages which correspond to each screen in the app. 
7. **Utils** - relevant app util classes. 

## How to build
### Verify code integrity
From project root run command *gradlew clean check* to run all the checks and verification for the project. This will run tests as well. 

In order to run tests separately, run *gradlew clean test*. **NOTE:** Environment variable *JAVA7_HOME* should be set pointing to JDK7 install directory.

### When using Android Studio
When project is imported, click on *Run -> Clean* and then *Run -> Run* to have the app installed on your device.

### From command line
Position to root folder and run *gradlew assembleDebug* (or *assembleRelease* for release version of the app).
The apk that is built can be found under **app/build/outputs/apk/{debug / release}** folder, depending on configuration that is built.

### Installing the APK
If ADB (Android Device Bridge) is installed on the computer used for building the app, execute *adb install -r app\build\outputs\apk\debug\app-debug.apk* to install the apk to mobile device. The **-r** option is used when the app is currently installed on the device and needs to be overwritten during install. It can be omitted when installing the app for the first time.

## Notes
GPS tracking is mocked and does not use real sensor data.
DAO-s are not implemented using ORM but rather SharedPreferences. There was no need to user ORM-s since domain model is not complicated, but can easily be replaced in the future.

## How to use the app
When starting for the first time, onboarding screen will be shown where user needs to enter his personal data and take a profile picture.
 
Current ride screen is shown after onboarding or each next time when the user enters the app. 

Previous rides screen shows all previous ride in descending order, meaning the most recent ride is located at the top of the list.
 
By clicking on item an in previous rides screen, ride detail screen is opened with ride information such as start location, end location, number of passengers and list of ride events (including location updates).

Settings can be opened from main screen and will show user details that were entered in onboarding.