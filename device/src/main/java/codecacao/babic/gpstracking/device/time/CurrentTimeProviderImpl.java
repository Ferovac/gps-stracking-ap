package codecacao.babic.gpstracking.device.time;

import java.util.concurrent.TimeUnit;

import codecacao.babic.gpstracking.domain.time.CurrentTimeProvider;

public final class CurrentTimeProviderImpl implements CurrentTimeProvider {

    @Override
    public long getCurrentTimeMillis() {
        return System.currentTimeMillis();
    }

    @Override
    public long getCurrentTimestamp() {
        return TimeUnit.MILLISECONDS.toSeconds(getCurrentTimeMillis());
    }
}
