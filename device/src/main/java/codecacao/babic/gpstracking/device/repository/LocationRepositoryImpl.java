package codecacao.babic.gpstracking.device.repository;

import java.util.Random;

import codecacao.babic.gpstracking.domain.model.LocationData;
import codecacao.babic.gpstracking.domain.repository.LocationRepository;
import rx.Single;

public final class LocationRepositoryImpl implements LocationRepository {

    private static final Random RANDOM = new Random();

    @Override
    public Single<LocationData> getCurrentLocation() {
        return Single.just(new LocationData(System.currentTimeMillis(), RANDOM.nextInt(50), RANDOM.nextInt(50)));
    }
}
