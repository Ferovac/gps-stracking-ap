package codecacao.babic.gpstracking.domain.usecase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import codecacao.babic.gpstracking.domain.model.LocationData;
import codecacao.babic.gpstracking.domain.repository.RideRepository;
import codecacao.babic.gpstracking.domain.time.CurrentTimeProvider;
import rx.Completable;
import rx.Single;

public final class StartRideUseCaseTest {

    private CurrentTimeProvider mockCurrentTimeProvider;
    private RideRepository mockRideRepository;

    private StartRideUseCase startRideUseCase;

    @Before
    public void setUp() throws Exception {
        mockCurrentTimeProvider = Mockito.mock(CurrentTimeProvider.class);
        mockRideRepository = Mockito.mock(RideRepository.class);

        startRideUseCase = new StartRideUseCaseImpl(mockCurrentTimeProvider, mockRideRepository);
    }

    @Test
    public void testStartingRide() throws Exception {
        Mockito.when(mockCurrentTimeProvider.getCurrentTimeMillis()).thenReturn(1000L);
        Mockito.when(mockRideRepository.getNewRideId()).thenReturn(Single.just(50));
        Mockito.when(mockRideRepository.startRide(Mockito.anyInt(), Mockito.anyString(), Mockito.anyString(), Mockito.anyInt(), Mockito.anyLong(),
                                                  Mockito.any(LocationData.class))).thenReturn(Completable.complete());

        final LocationData locationData = new LocationData(10, 10f, 10f);

        startRideUseCase.execute(new StartRideUseCase.Request("start", "end", 10, locationData)).subscribe();

        Mockito.verify(mockRideRepository, Mockito.times(1)).startRide(50, "start", "end", 10, 1000L, locationData);
    }
}
