package codecacao.babic.gpstracking.domain.usecase;

import com.annimon.stream.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;

import codecacao.babic.gpstracking.domain.model.RideEvent;
import codecacao.babic.gpstracking.domain.model.RideEventType;
import codecacao.babic.gpstracking.domain.repository.RideRepository;
import rx.Single;

public final class GetLastCurrentRideEventUseCaseTest {

    private RideRepository mockRideRepository;

    private GetLastCurrentRideEventUseCase getLastCurrentRideEventUseCase;

    @Before
    public void setUp() throws Exception {
        mockRideRepository = Mockito.mock(RideRepository.class);

        getLastCurrentRideEventUseCase = new GetLastCurrentRideEventUseCaseImpl(mockRideRepository);
    }

    @Test
    public void testGettingLastRideEventWhenThereIsRide() throws Exception {
        final RideEvent rideEvent1 = new RideEvent(1, 1, 10, 10f, 10f, RideEventType.PASSENGER_PICKED);
        final RideEvent rideEvent2 = new RideEvent(2, 1, 12, 10f, 10f, RideEventType.LOCATION_UPDATE);

        Mockito.when(mockRideRepository.getCurrentRideId()).thenReturn(Single.just(Optional.of(1)));
        Mockito.when(mockRideRepository.getRideEvents(Mockito.anyInt())).thenReturn(Single.just(Arrays.asList(rideEvent1, rideEvent2)));

        final Optional<RideEvent> rideEvent = getLastCurrentRideEventUseCase.execute().toBlocking().value();

        Assert.assertEquals(Optional.of(rideEvent1), rideEvent);
    }

    @Test
    public void testGettingLastRideEventWhenThereIsNoRide() throws Exception {
        Mockito.when(mockRideRepository.getCurrentRideId()).thenReturn(Single.just(Optional.of(1)));
        Mockito.when(mockRideRepository.getRideEvents(Mockito.anyInt())).thenReturn(Single.just(Collections.emptyList()));

        final Optional<RideEvent> rideEvent = getLastCurrentRideEventUseCase.execute().toBlocking().value();

        Assert.assertEquals(Optional.empty(), rideEvent);
    }
}
