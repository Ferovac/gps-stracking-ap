package codecacao.babic.gpstracking.domain.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public final class StringUtilsTest {

    private StringUtils stringUtils;

    @Before
    public void setUp() throws Exception {
        stringUtils = new StringUtilsImpl();
    }

    @Test
    public void testEvaluatingNullAsEmpty() throws Exception {
        Assert.assertTrue(stringUtils.isEmpty(null));
    }

    @Test
    public void testEvaluatingEmptyStringAsEmpty() throws Exception {
        Assert.assertTrue(stringUtils.isEmpty(""));
    }

    @Test
    public void testEvaluatingNonEmptyStringAsNonEmpty() throws Exception {
        Assert.assertFalse(stringUtils.isEmpty("not empty"));
    }

    @Test
    public void mapIntegerListToCsvList() throws Exception {
        final List<Integer> items = Arrays.asList(1, 2, 3);

        Assert.assertEquals("1,2,3", stringUtils.mapIntegerListToStringWithSeparator(items, ","));
    }

    @Test
    public void mapCsvIntegerListToIntegerList() throws Exception {
        Assert.assertEquals(Arrays.asList(1, 2, 3), stringUtils.parseToIntegerList("1,2,3", ","));
    }
}
