package codecacao.babic.gpstracking.domain.usecase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import codecacao.babic.gpstracking.domain.model.User;
import codecacao.babic.gpstracking.domain.repository.UserRepository;
import rx.Completable;

public final class SaveUserDataUseCaseTest {

    private UserRepository mockUserRepository;

    private SaveUserDataUseCase saveUserDataUseCase;

    @Before
    public void setUp() throws Exception {
        mockUserRepository = Mockito.mock(UserRepository.class);
        saveUserDataUseCase = new SaveUserDataUseCaseImpl(mockUserRepository);
    }

    @Test
    public void testSavingUser() throws Exception {
        Mockito.when(mockUserRepository.saveUser(Mockito.any(User.class))).thenReturn(Completable.complete());

        saveUserDataUseCase.execute(User.EMPTY).subscribe();

        Mockito.verify(mockUserRepository, Mockito.times(1)).saveUser(Mockito.any(User.class));
    }
}
