package codecacao.babic.gpstracking.domain.usecase;

import com.annimon.stream.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import codecacao.babic.gpstracking.domain.model.User;
import codecacao.babic.gpstracking.domain.repository.UserRepository;
import rx.Single;

public final class GetUserDetailUseCaseTest {

    private UserRepository mockUserRepository;

    private GetUserDataUseCase getUserDataUseCase;

    @Before
    public void setUp() throws Exception {
        mockUserRepository = Mockito.mock(UserRepository.class);

        getUserDataUseCase = new GetUserDataUseCaseImpl(mockUserRepository);
    }

    @Test
    public void testGettingUserDetails() throws Exception {
        Mockito.when(mockUserRepository.getCurrentUser()).thenReturn(Single.just(Optional.of(User.EMPTY)));

        final Optional<User> user = getUserDataUseCase.execute().toBlocking().value();

        Mockito.verify(mockUserRepository, Mockito.times(1)).getCurrentUser();

        Assert.assertEquals(user, Optional.of(User.EMPTY));
    }
}
