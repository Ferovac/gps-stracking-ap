package codecacao.babic.gpstracking.domain.usecase;

import com.annimon.stream.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import codecacao.babic.gpstracking.domain.model.Ride;
import codecacao.babic.gpstracking.domain.repository.RideRepository;
import codecacao.babic.gpstracking.domain.utils.ListUtilsImpl;
import rx.Single;

public final class GetPreviousRidesUseCaseTest {

    private RideRepository mockRideRepository;
    private GetCurrentRideUseCase mockGetCurrentRideUseCase;

    private GetPreviousRidesUseCase getPreviousRidesUseCase;

    @Before
    public void setUp() throws Exception {
        mockRideRepository = Mockito.mock(RideRepository.class);
        mockGetCurrentRideUseCase = Mockito.mock(GetCurrentRideUseCase.class);

        getPreviousRidesUseCase = new GetPreviousRidesUseCaseImpl(mockGetCurrentRideUseCase, mockRideRepository, new ListUtilsImpl());
    }

    @Test
    public void testGettingPreviousRidesWhenRideOngoing() throws Exception {
        final Ride ride1 = new Ride(10, "start", "end", 10);
        final Ride ride2 = new Ride(11, "start", "end", 10);

        Mockito.when(mockGetCurrentRideUseCase.execute()).thenReturn(Single.just(Optional.of(ride2)));
        Mockito.when(mockRideRepository.getAllRides()).thenReturn(Single.just(Arrays.asList(ride1, ride2)));

        final List<Ride> previousRides = getPreviousRidesUseCase.execute().toBlocking().value();

        Assert.assertEquals(previousRides, Collections.singletonList(ride1));
    }

    @Test
    public void testGettingPreviousRidesWhenNoRideOngoing() throws Exception {
        final Ride ride1 = new Ride(10, "start", "end", 10);
        final Ride ride2 = new Ride(11, "start", "end", 10);

        Mockito.when(mockGetCurrentRideUseCase.execute()).thenReturn(Single.just(Optional.empty()));
        Mockito.when(mockRideRepository.getAllRides()).thenReturn(Single.just(Arrays.asList(ride1, ride2)));

        final List<Ride> previousRides = getPreviousRidesUseCase.execute().toBlocking().value();

        Assert.assertEquals(previousRides, Arrays.asList(ride2, ride1));
    }
}
