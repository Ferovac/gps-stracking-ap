package codecacao.babic.gpstracking.domain.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public final class ListUtilsTest {

    private ListUtils listUtils;

    @Before
    public void setUp() throws Exception {
        listUtils = new ListUtilsImpl();
    }

    @Test
    public void testListUtilsReversingList() throws Exception {
        Assert.assertEquals(Arrays.asList(2, 1), listUtils.reverse(new ArrayList<>(Arrays.asList(1, 2))));
    }
}
