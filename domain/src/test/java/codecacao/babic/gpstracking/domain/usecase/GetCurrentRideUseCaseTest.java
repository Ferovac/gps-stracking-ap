package codecacao.babic.gpstracking.domain.usecase;

import com.annimon.stream.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import codecacao.babic.gpstracking.domain.model.Ride;
import codecacao.babic.gpstracking.domain.repository.RideRepository;
import rx.Single;

public final class GetCurrentRideUseCaseTest {

    private RideRepository mockRideRepository;

    private GetCurrentRideUseCase getCurrentRideUseCase;

    @Before
    public void setUp() throws Exception {
        mockRideRepository = Mockito.mock(RideRepository.class);

        getCurrentRideUseCase = new GetCurrentRideUseCaseImpl(mockRideRepository);
    }

    @Test
    public void testGettingRideWhenThereIsOne() throws Exception {
        Mockito.when(mockRideRepository.getCurrentRideId()).thenReturn(Single.just(Optional.of(1)));
        Mockito.when(mockRideRepository.getRideById(Mockito.anyInt())).thenReturn(Single.just(Optional.of(Ride.EMPTY)));

        final Optional<Ride> ride = getCurrentRideUseCase.execute().toBlocking().value();

        Assert.assertEquals(ride, Optional.of(Ride.EMPTY));
    }

    @Test
    public void testGettingRideWhenThereIsntOne() throws Exception {
        Mockito.when(mockRideRepository.getCurrentRideId()).thenReturn(Single.just(Optional.empty()));

        final Optional<Ride> ride = getCurrentRideUseCase.execute().toBlocking().value();

        Assert.assertEquals(ride, Optional.empty());
    }
}
