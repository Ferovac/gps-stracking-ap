package codecacao.babic.gpstracking.domain.usecase;

import com.annimon.stream.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import codecacao.babic.gpstracking.domain.model.LocationData;
import codecacao.babic.gpstracking.domain.model.NoOngoingRideException;
import codecacao.babic.gpstracking.domain.model.RideEventType;
import codecacao.babic.gpstracking.domain.repository.RideRepository;
import codecacao.babic.gpstracking.domain.time.CurrentTimeProvider;
import rx.Completable;
import rx.Single;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.schedulers.TestScheduler;

public final class AddRideEventUseCaseTest {

    private RideRepository mockRideRepository;
    private CurrentTimeProvider mockCurrentTimeProvider;

    private AddRideEventUseCase addRideEventUseCase;

    private TestScheduler testScheduler;

    @Before
    public void setUp() throws Exception {
        testScheduler = Schedulers.test();

        mockRideRepository = Mockito.mock(RideRepository.class);
        mockCurrentTimeProvider = Mockito.mock(CurrentTimeProvider.class);

        addRideEventUseCase = new AddRideEventUseCaseImpl(mockRideRepository, mockCurrentTimeProvider);
    }

    @Test
    public void testAddingRideEventWhenRideOngoing() throws Exception {
        Mockito.when(mockRideRepository.getCurrentRideId()).thenReturn(Single.just(Optional.of(1)));
        Mockito.when(mockRideRepository.addRideEvent(Mockito.anyInt(), Mockito.anyLong(), Mockito.any(LocationData.class), Mockito.any(RideEventType.class)))
               .thenReturn(Completable.complete());

        Mockito.when(mockCurrentTimeProvider.getCurrentTimeMillis()).thenReturn(10L);

        final LocationData locationData = new LocationData(10, 0f, 0f);
        final AddRideEventUseCase.Request request = new AddRideEventUseCase.Request(locationData, RideEventType.LOCATION_UPDATE);

        addRideEventUseCase.execute(request)
                           .subscribe();

        Mockito.verify(mockRideRepository, Mockito.times(1)).getCurrentRideId();
        Mockito.verify(mockRideRepository, Mockito.times(1)).addRideEvent(1, 10L, locationData, RideEventType.LOCATION_UPDATE);
    }

    @Test
    public void testFailingWhenNoCurrentRide() throws Exception {
        final Action0 mockCompleteAction = Mockito.mock(Action0.class);
        final Action1<Throwable> mockActionThrowable = (Action1<Throwable>)Mockito.mock(Action1.class);

        Mockito.when(mockRideRepository.getCurrentRideId()).thenReturn(Single.just(Optional.empty()));

        addRideEventUseCase.execute(new AddRideEventUseCase.Request(LocationData.EMPTY, RideEventType.UNKNOWN))
                           .subscribeOn(testScheduler)
                           .observeOn(testScheduler)
                           .subscribe(mockCompleteAction,
                                      mockActionThrowable);

        testScheduler.triggerActions();

        // To wait for background thread.
        Thread.sleep(10);

        Mockito.verifyZeroInteractions(mockCompleteAction);
        Mockito.verify(mockActionThrowable, Mockito.times(1)).call(Mockito.any(NoOngoingRideException.class));
    }
}
