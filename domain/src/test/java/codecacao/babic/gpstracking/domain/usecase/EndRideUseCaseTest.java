package codecacao.babic.gpstracking.domain.usecase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import codecacao.babic.gpstracking.domain.model.LocationData;
import codecacao.babic.gpstracking.domain.model.RideEventType;
import codecacao.babic.gpstracking.domain.repository.RideRepository;
import rx.Completable;

public final class EndRideUseCaseTest {

    private AddRideEventUseCase mockAddRideEventUseCase;
    private RideRepository mockRideRepository;

    private EndRideUseCase endRideUseCase;

    @Before
    public void setUp() throws Exception {
        mockAddRideEventUseCase = Mockito.mock(AddRideEventUseCase.class);
        mockRideRepository = Mockito.mock(RideRepository.class);

        endRideUseCase = new EndRideUseCaseImpl(mockAddRideEventUseCase, mockRideRepository);
    }

    @Test
    public void testRideEnding() throws Exception {
        Mockito.when(mockAddRideEventUseCase.execute(Mockito.any(AddRideEventUseCase.Request.class))).thenReturn(Completable.complete());
        Mockito.when(mockRideRepository.endRide()).thenReturn(Completable.complete());

        endRideUseCase.execute(LocationData.EMPTY)
                      .subscribe();

        Mockito.verify(mockAddRideEventUseCase, Mockito.times(1)).execute(new AddRideEventUseCase.Request(LocationData.EMPTY, RideEventType.END_RIDE));
        Mockito.verify(mockRideRepository, Mockito.times(1)).endRide();
    }
}
