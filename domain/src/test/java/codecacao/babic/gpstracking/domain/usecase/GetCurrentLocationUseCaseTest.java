package codecacao.babic.gpstracking.domain.usecase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import codecacao.babic.gpstracking.domain.model.LocationData;
import codecacao.babic.gpstracking.domain.repository.LocationRepository;
import rx.Single;

public final class GetCurrentLocationUseCaseTest {

    private LocationRepository mockLocationRepository;

    private GetCurrentLocationUseCase getCurrentLocationUseCase;

    @Before
    public void setUp() throws Exception {
        mockLocationRepository = Mockito.mock(LocationRepository.class);

        getCurrentLocationUseCase = new GetCurrentLocationUseCaseImpl(mockLocationRepository);
    }

    @Test
    public void testGettingLocationData() throws Exception {
        Mockito.when(mockLocationRepository.getCurrentLocation()).thenReturn(Single.just(LocationData.EMPTY));

        getCurrentLocationUseCase.execute()
                                 .subscribe();

        Mockito.verify(mockLocationRepository, Mockito.times(1)).getCurrentLocation();
    }
}
