package codecacao.babic.gpstracking.domain.usecase;

import com.annimon.stream.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import codecacao.babic.gpstracking.domain.model.User;
import codecacao.babic.gpstracking.domain.repository.UserRepository;
import rx.Single;

public final class IsUserCreatedUseCaseTest {

    private UserRepository mockUserRepository;

    private IsUserCreatedUseCase isUserCreatedUseCase;

    @Before
    public void setUp() throws Exception {
        mockUserRepository = Mockito.mock(UserRepository.class);
        isUserCreatedUseCase = new IsUserCreatedUseCaseImpl(mockUserRepository);
    }

    @Test
    public void testReturningTrueWhenUserIsCreated() throws Exception {
        Mockito.when(mockUserRepository.getCurrentUser()).thenReturn(Single.just(Optional.of(User.EMPTY)));

        Assert.assertTrue(isUserCreatedUseCase.execute().toBlocking().value());
    }

    @Test
    public void testReturningTrueWhenUserIsNotCreated() throws Exception {
        Mockito.when(mockUserRepository.getCurrentUser()).thenReturn(Single.just(Optional.empty()));

        Assert.assertFalse(isUserCreatedUseCase.execute().toBlocking().value());
    }
}
