package codecacao.babic.gpstracking.domain.usecase;

import com.annimon.stream.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;

import codecacao.babic.gpstracking.domain.model.Ride;
import codecacao.babic.gpstracking.domain.model.RideDetails;
import codecacao.babic.gpstracking.domain.model.RideEvent;
import codecacao.babic.gpstracking.domain.model.RideEventType;
import codecacao.babic.gpstracking.domain.repository.RideRepository;
import rx.Single;

public final class GetRideDetailsUseCaseTest {

    private RideRepository mockRideRepository;

    private GetRideDetailsUseCase getRideDetailsUseCaseTest;

    @Before
    public void setUp() throws Exception {
        mockRideRepository = Mockito.mock(RideRepository.class);

        getRideDetailsUseCaseTest = new GetRideDetailsUseCaseImpl(mockRideRepository);
    }

    @Test
    public void testGettingRideDetails() throws Exception {
        final Ride ride = new Ride(10, "start", "end", 5);

        final RideEvent rideEvent1 = new RideEvent(100, ride.id, 10f, 10f, RideEventType.STARTED);
        final RideEvent rideEvent2 = new RideEvent(101, ride.id, 10f, 10f, RideEventType.END_RIDE);

        Mockito.when(mockRideRepository.getRideById(Mockito.anyInt())).thenReturn(Single.just(Optional.of(ride)));
        Mockito.when(mockRideRepository.getRideEvents(ride.id)).thenReturn(Single.just(Arrays.asList(rideEvent1, rideEvent2)));

        final RideDetails rideDetails = getRideDetailsUseCaseTest.execute(ride.id).toBlocking().value();

        Assert.assertEquals(rideDetails, new RideDetails(ride, Arrays.asList(rideEvent1, rideEvent2)));
    }
}
