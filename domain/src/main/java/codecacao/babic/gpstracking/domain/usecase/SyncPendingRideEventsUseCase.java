package codecacao.babic.gpstracking.domain.usecase;

import rx.Completable;

public interface SyncPendingRideEventsUseCase {

    Completable execute();
}
