package codecacao.babic.gpstracking.domain.usecase;

import com.annimon.stream.Optional;

import codecacao.babic.gpstracking.domain.model.User;
import codecacao.babic.gpstracking.domain.repository.UserRepository;
import rx.Single;

public final class GetUserDataUseCaseImpl implements GetUserDataUseCase {

    private final UserRepository userRepository;

    public GetUserDataUseCaseImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Single<Optional<User>> execute() {
        return userRepository.getCurrentUser();
    }
}
