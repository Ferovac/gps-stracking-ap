package codecacao.babic.gpstracking.domain.usecase;

import java.util.List;

import codecacao.babic.gpstracking.domain.model.Ride;
import codecacao.babic.gpstracking.domain.repository.RideRepository;
import codecacao.babic.gpstracking.domain.utils.ListUtils;
import rx.Observable;
import rx.Single;

public final class GetPreviousRidesUseCaseImpl implements GetPreviousRidesUseCase {

    private final GetCurrentRideUseCase getCurrentRideUseCase;
    private final RideRepository rideRepository;
    private final ListUtils listUtils;

    public GetPreviousRidesUseCaseImpl(final GetCurrentRideUseCase getCurrentRideUseCase, final RideRepository rideRepository, final ListUtils listUtils) {
        this.getCurrentRideUseCase = getCurrentRideUseCase;
        this.rideRepository = rideRepository;
        this.listUtils = listUtils;
    }

    @Override
    public Single<List<Ride>> execute() {
        return getCurrentRideUseCase.execute()
                                    .flatMap(ride -> ride.isPresent() ? getAllRidesWithoutCurrentOne(ride.get()) : getAllRides())
                                    .map(listUtils::reverse);
    }

    private Single<List<Ride>> getAllRidesWithoutCurrentOne(final Ride currentRide) {
        return getAllRides().flatMapObservable(Observable::from)
                            .filter(ride -> ride.id != currentRide.id)
                            .toList()
                            .toSingle();
    }

    private Single<List<Ride>> getAllRides() {
        return rideRepository.getAllRides();
    }
}
