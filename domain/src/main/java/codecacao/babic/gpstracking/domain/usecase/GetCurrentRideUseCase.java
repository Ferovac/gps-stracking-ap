package codecacao.babic.gpstracking.domain.usecase;

import com.annimon.stream.Optional;

import codecacao.babic.gpstracking.domain.model.Ride;
import rx.Single;

public interface GetCurrentRideUseCase {

    Single<Optional<Ride>> execute();
}
