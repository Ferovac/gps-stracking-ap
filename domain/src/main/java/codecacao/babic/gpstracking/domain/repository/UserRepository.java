package codecacao.babic.gpstracking.domain.repository;

import com.annimon.stream.Optional;

import codecacao.babic.gpstracking.domain.model.User;
import rx.Completable;
import rx.Single;

public interface UserRepository {

    Completable saveUser(User user);

    Single<Optional<User>> getCurrentUser();
}
