package codecacao.babic.gpstracking.domain.model;

public final class Ride {

    public static final Ride EMPTY = new Ride(0, "", "", 0);

    public final int id;
    public final String startLocation;
    public final String endLocation;
    public final int numberOfPassengers;

    public Ride(final int id, final String startLocation, final String endLocation, final int numberOfPassengers) {
        this.id = id;
        this.startLocation = startLocation;
        this.endLocation = endLocation;
        this.numberOfPassengers = numberOfPassengers;
    }

    public Ride copyOnId(final int id) {
        return new Ride(id, startLocation, endLocation, numberOfPassengers);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Ride ride = (Ride) o;

        if (id != ride.id) {
            return false;
        }
        if (numberOfPassengers != ride.numberOfPassengers) {
            return false;
        }
        if (startLocation != null ? !startLocation.equals(ride.startLocation) : ride.startLocation != null) {
            return false;
        }
        return endLocation != null ? endLocation.equals(ride.endLocation) : ride.endLocation == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (startLocation != null ? startLocation.hashCode() : 0);
        result = 31 * result + (endLocation != null ? endLocation.hashCode() : 0);
        result = 31 * result + numberOfPassengers;
        return result;
    }

    @Override
    public String toString() {
        return "Ride{" +
                "id=" + id +
                ", startLocation='" + startLocation + '\'' +
                ", endLocation='" + endLocation + '\'' +
                ", numberOfPassengers=" + numberOfPassengers +
                '}';
    }
}
