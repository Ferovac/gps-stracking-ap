package codecacao.babic.gpstracking.domain.usecase;

import codecacao.babic.gpstracking.domain.model.LocationData;
import rx.Completable;

public interface StartRideUseCase {

    Completable execute(Request request);

    final class Request {

        public final String startLocation;
        public final String endLocation;
        public final int numberOfPassengers;
        public final LocationData locationData;

        public Request(final String startLocation, final String endLocation, final int numberOfPassengers, final LocationData locationData) {
            this.startLocation = startLocation;
            this.endLocation = endLocation;
            this.numberOfPassengers = numberOfPassengers;
            this.locationData = locationData;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final Request request = (Request) o;

            if (numberOfPassengers != request.numberOfPassengers) {
                return false;
            }
            if (startLocation != null ? !startLocation.equals(request.startLocation) : request.startLocation != null) {
                return false;
            }
            if (endLocation != null ? !endLocation.equals(request.endLocation) : request.endLocation != null) {
                return false;
            }
            return locationData != null ? locationData.equals(request.locationData) : request.locationData == null;
        }

        @Override
        public int hashCode() {
            int result = startLocation != null ? startLocation.hashCode() : 0;
            result = 31 * result + (endLocation != null ? endLocation.hashCode() : 0);
            result = 31 * result + numberOfPassengers;
            result = 31 * result + (locationData != null ? locationData.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "Request{" +
                    "startLocation='" + startLocation + '\'' +
                    ", endLocation='" + endLocation + '\'' +
                    ", numberOfPassengers=" + numberOfPassengers +
                    ", locationData=" + locationData +
                    '}';
        }
    }
}
