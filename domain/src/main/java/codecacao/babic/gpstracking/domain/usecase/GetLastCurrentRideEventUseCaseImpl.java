package codecacao.babic.gpstracking.domain.usecase;

import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

import codecacao.babic.gpstracking.domain.model.RideEvent;
import codecacao.babic.gpstracking.domain.model.RideEventType;
import codecacao.babic.gpstracking.domain.repository.RideRepository;
import rx.Single;

public final class GetLastCurrentRideEventUseCaseImpl implements GetLastCurrentRideEventUseCase {

    private final RideRepository rideRepository;

    public GetLastCurrentRideEventUseCaseImpl(final RideRepository rideRepository) {
        this.rideRepository = rideRepository;
    }

    @Override
    public Single<Optional<RideEvent>> execute() {
        return rideRepository.getCurrentRideId()
                             .flatMap(rideId -> rideId.isPresent() ? getLastRideEventForRide(rideId.get()) : Single.just(Optional.empty()));
    }

    private Single<Optional<RideEvent>> getLastRideEventForRide(final int rideId) {
        return rideRepository.getRideEvents(rideId)
                             .map(rideEvents -> Stream.of(rideEvents)
                                                      .filter(rideEvent -> rideEvent.eventType != RideEventType.LOCATION_UPDATE)
                                                      .findLast());
    }
}
