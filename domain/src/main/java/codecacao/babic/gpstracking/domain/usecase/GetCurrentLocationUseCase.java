package codecacao.babic.gpstracking.domain.usecase;

import codecacao.babic.gpstracking.domain.model.LocationData;
import rx.Single;

public interface GetCurrentLocationUseCase {

    Single<LocationData> execute();
}
