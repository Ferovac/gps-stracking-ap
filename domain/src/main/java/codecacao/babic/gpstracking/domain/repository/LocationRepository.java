package codecacao.babic.gpstracking.domain.repository;

import codecacao.babic.gpstracking.domain.model.LocationData;
import rx.Single;

public interface LocationRepository {

    Single<LocationData> getCurrentLocation();
}
