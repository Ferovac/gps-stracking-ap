package codecacao.babic.gpstracking.domain.usecase;

import com.annimon.stream.Optional;

import codecacao.babic.gpstracking.domain.repository.UserRepository;
import rx.Single;

public final class IsUserCreatedUseCaseImpl implements IsUserCreatedUseCase {

    private final UserRepository userRepository;

    public IsUserCreatedUseCaseImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Single<Boolean> execute() {
        return userRepository.getCurrentUser()
                             .map(Optional::isPresent);
    }
}
