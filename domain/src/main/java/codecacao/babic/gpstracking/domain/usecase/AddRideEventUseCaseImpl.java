package codecacao.babic.gpstracking.domain.usecase;

import codecacao.babic.gpstracking.domain.model.LocationData;
import codecacao.babic.gpstracking.domain.model.NoOngoingRideException;
import codecacao.babic.gpstracking.domain.model.RideEventType;
import codecacao.babic.gpstracking.domain.repository.RideRepository;
import codecacao.babic.gpstracking.domain.time.CurrentTimeProvider;
import rx.Completable;

public final class AddRideEventUseCaseImpl implements AddRideEventUseCase {

    private final RideRepository rideRepository;
    private final CurrentTimeProvider currentTimeProvider;

    public AddRideEventUseCaseImpl(final RideRepository rideRepository, final CurrentTimeProvider currentTimeProvider) {
        this.rideRepository = rideRepository;
        this.currentTimeProvider = currentTimeProvider;
    }

    @Override
    public Completable execute(final Request request) {
        return rideRepository.getCurrentRideId()
                             .flatMapCompletable(rideId -> rideId.isPresent() ? addRideEvent(rideId.get(), request.locationData, request.rideEventType)
                                                                              : Completable.error(new NoOngoingRideException()));
    }

    private Completable addRideEvent(final int rideId, final LocationData locationData, final RideEventType rideEventType) {
        return rideRepository.addRideEvent(rideId,
                                           currentTimeProvider.getCurrentTimeMillis(),
                                           locationData,
                                           rideEventType);
    }
}
