package codecacao.babic.gpstracking.domain.delegate;

public interface UserComponentDelegate {

    void initUserComponent();
}
