package codecacao.babic.gpstracking.domain.model;

import java.util.List;

public final class RideDetails {

    public final Ride ride;
    public final List<RideEvent> rideEvents;

    public RideDetails(final Ride ride, final List<RideEvent> rideEvents) {
        this.ride = ride;
        this.rideEvents = rideEvents;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final RideDetails that = (RideDetails) o;

        if (ride != null ? !ride.equals(that.ride) : that.ride != null) {
            return false;
        }
        return rideEvents != null ? rideEvents.equals(that.rideEvents) : that.rideEvents == null;
    }

    @Override
    public int hashCode() {
        int result = ride != null ? ride.hashCode() : 0;
        result = 31 * result + (rideEvents != null ? rideEvents.hashCode() : 0);
        return result;
    }
}
