package codecacao.babic.gpstracking.domain.model;

public final class User {

    public static final User EMPTY = new User("", "", 0, "", "");

    public final String firstName;
    public final String lastName;
    public final int age;
    public final String licencePlate;
    public final String imageUrl;

    public User(final String firstName, final String lastName, final int age, final String licencePlate, final String imageUrl) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.licencePlate = licencePlate;
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final User user = (User) o;

        if (age != user.age) {
            return false;
        }
        if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) {
            return false;
        }
        if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null) {
            return false;
        }
        if (licencePlate != null ? !licencePlate.equals(user.licencePlate) : user.licencePlate != null) {
            return false;
        }
        return imageUrl != null ? imageUrl.equals(user.imageUrl) : user.imageUrl == null;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + age;
        result = 31 * result + (licencePlate != null ? licencePlate.hashCode() : 0);
        result = 31 * result + (imageUrl != null ? imageUrl.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", licencePlate='" + licencePlate + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
