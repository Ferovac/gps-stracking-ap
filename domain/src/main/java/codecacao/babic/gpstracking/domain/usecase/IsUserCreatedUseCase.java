package codecacao.babic.gpstracking.domain.usecase;

import rx.Single;

public interface IsUserCreatedUseCase {

    Single<Boolean> execute();
}
