package codecacao.babic.gpstracking.domain.usecase;

import codecacao.babic.gpstracking.domain.model.LocationData;
import codecacao.babic.gpstracking.domain.model.RideEventType;
import codecacao.babic.gpstracking.domain.repository.RideRepository;
import rx.Completable;

public final class EndRideUseCaseImpl implements EndRideUseCase {

    private final AddRideEventUseCase addRideEventUseCase;
    private final RideRepository rideRepository;

    public EndRideUseCaseImpl(final AddRideEventUseCase addRideEventUseCase, final RideRepository rideRepository) {
        this.addRideEventUseCase = addRideEventUseCase;
        this.rideRepository = rideRepository;
    }

    @Override
    public Completable execute(final LocationData locationData) {
        return addRideEventUseCase.execute(new AddRideEventUseCase.Request(locationData, RideEventType.END_RIDE))
                                  .andThen(rideRepository.endRide());
    }
}
