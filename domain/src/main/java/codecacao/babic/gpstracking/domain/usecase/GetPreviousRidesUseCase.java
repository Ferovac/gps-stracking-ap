package codecacao.babic.gpstracking.domain.usecase;

import java.util.List;

import codecacao.babic.gpstracking.domain.model.Ride;
import rx.Single;

public interface GetPreviousRidesUseCase {

    Single<List<Ride>> execute();
}
