package codecacao.babic.gpstracking.domain.model;

public final class CurrentRideData {

    public final Ride ride;
    public final RideEvent lastRideEvent;

    public CurrentRideData(final Ride ride, final RideEvent lastRideEvent) {
        this.ride = ride;
        this.lastRideEvent = lastRideEvent;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final CurrentRideData that = (CurrentRideData) o;

        if (ride != null ? !ride.equals(that.ride) : that.ride != null) {
            return false;
        }
        return lastRideEvent != null ? lastRideEvent.equals(that.lastRideEvent) : that.lastRideEvent == null;
    }

    @Override
    public int hashCode() {
        int result = ride != null ? ride.hashCode() : 0;
        result = 31 * result + (lastRideEvent != null ? lastRideEvent.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CurrentRideData{" +
                "ride=" + ride +
                ", lastRideEvent=" + lastRideEvent +
                '}';
    }
}
