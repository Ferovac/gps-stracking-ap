package codecacao.babic.gpstracking.domain.utils;

import rx.Completable;

public interface NotificationDisplay {

    Completable showRideOngoingNotification(final String startLocation, final String endLocation);

    Completable stopDisplayingNotification();
}
