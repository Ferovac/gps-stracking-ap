package codecacao.babic.gpstracking.domain.usecase;

import codecacao.babic.gpstracking.domain.model.User;
import codecacao.babic.gpstracking.domain.repository.UserRepository;
import rx.Completable;

public final class SaveUserDataUseCaseImpl implements SaveUserDataUseCase {

    private final UserRepository userRepository;

    public SaveUserDataUseCaseImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Completable execute(final User user) {
        return userRepository.saveUser(user);
    }
}
