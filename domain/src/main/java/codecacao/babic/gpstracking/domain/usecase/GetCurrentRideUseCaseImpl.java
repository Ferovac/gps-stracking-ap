package codecacao.babic.gpstracking.domain.usecase;

import com.annimon.stream.Optional;

import codecacao.babic.gpstracking.domain.model.Ride;
import codecacao.babic.gpstracking.domain.repository.RideRepository;
import rx.Single;

public final class GetCurrentRideUseCaseImpl implements GetCurrentRideUseCase {

    private final RideRepository rideRepository;

    public GetCurrentRideUseCaseImpl(final RideRepository rideRepository) {
        this.rideRepository = rideRepository;
    }

    @Override
    public Single<Optional<Ride>> execute() {
        return rideRepository.getCurrentRideId()
                             .flatMap(currentRideId -> currentRideId.isPresent() ? rideRepository.getRideById(currentRideId.get()) : Single.just(Optional.empty()));
    }
}
