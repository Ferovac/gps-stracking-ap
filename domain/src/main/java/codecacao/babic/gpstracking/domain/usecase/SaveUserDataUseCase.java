package codecacao.babic.gpstracking.domain.usecase;

import codecacao.babic.gpstracking.domain.model.User;
import rx.Completable;

public interface SaveUserDataUseCase {

    Completable execute(User user);
}
