package codecacao.babic.gpstracking.domain.usecase;

import codecacao.babic.gpstracking.domain.model.LocationData;
import rx.Completable;

public interface AddLocationUpdateEventUseCase {

    Completable execute(LocationData locationData);
}
