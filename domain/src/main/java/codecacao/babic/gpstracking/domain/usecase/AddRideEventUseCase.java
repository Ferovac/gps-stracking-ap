package codecacao.babic.gpstracking.domain.usecase;

import codecacao.babic.gpstracking.domain.model.LocationData;
import codecacao.babic.gpstracking.domain.model.RideEventType;
import rx.Completable;

public interface AddRideEventUseCase {

    Completable execute(Request request);

    final class Request {

        public final LocationData locationData;
        public final RideEventType rideEventType;

        public Request(final LocationData locationData, final RideEventType rideEventType) {
            this.locationData = locationData;
            this.rideEventType = rideEventType;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final Request request = (Request) o;

            if (locationData != null ? !locationData.equals(request.locationData) : request.locationData != null) {
                return false;
            }
            return rideEventType == request.rideEventType;
        }

        @Override
        public int hashCode() {
            int result = locationData != null ? locationData.hashCode() : 0;
            result = 31 * result + (rideEventType != null ? rideEventType.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "Request{" +
                    "locationData=" + locationData +
                    ", rideEventType=" + rideEventType +
                    '}';
        }
    }
}
