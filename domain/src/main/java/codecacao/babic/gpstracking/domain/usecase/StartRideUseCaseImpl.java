package codecacao.babic.gpstracking.domain.usecase;

import codecacao.babic.gpstracking.domain.repository.RideRepository;
import codecacao.babic.gpstracking.domain.time.CurrentTimeProvider;
import rx.Completable;

public final class StartRideUseCaseImpl implements StartRideUseCase {

    private final CurrentTimeProvider currentTimeProvider;
    private final RideRepository rideRepository;

    public StartRideUseCaseImpl(final CurrentTimeProvider currentTimeProvider, final RideRepository rideRepository) {
        this.currentTimeProvider = currentTimeProvider;
        this.rideRepository = rideRepository;
    }

    @Override
    public Completable execute(final Request request) {
        return rideRepository.getNewRideId()
                             .flatMapCompletable(rideId -> getStartRideCompletable(request, rideId));
    }

    private Completable getStartRideCompletable(final Request request, final Integer rideId) {
        return rideRepository.startRide(rideId, request.startLocation, request.endLocation, request.numberOfPassengers, currentTimeProvider.getCurrentTimeMillis(),
                                        request.locationData);
    }
}
