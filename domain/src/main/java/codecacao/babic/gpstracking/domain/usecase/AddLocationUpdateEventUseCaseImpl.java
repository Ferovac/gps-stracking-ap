package codecacao.babic.gpstracking.domain.usecase;

import codecacao.babic.gpstracking.domain.model.LocationData;
import codecacao.babic.gpstracking.domain.model.NoOngoingRideException;
import codecacao.babic.gpstracking.domain.repository.RideRepository;
import codecacao.babic.gpstracking.domain.time.CurrentTimeProvider;
import rx.Completable;

public final class AddLocationUpdateEventUseCaseImpl implements AddLocationUpdateEventUseCase {

    private final RideRepository rideRepository;
    private final CurrentTimeProvider currentTimeProvider;

    public AddLocationUpdateEventUseCaseImpl(final RideRepository rideRepository, final CurrentTimeProvider currentTimeProvider) {
        this.rideRepository = rideRepository;
        this.currentTimeProvider = currentTimeProvider;
    }

    @Override
    public Completable execute(final LocationData locationData) {
        return rideRepository.getCurrentRideId()
                             .flatMapCompletable(rideId -> rideId.isPresent() ? addLocationRideEvent(rideId.get(), locationData)
                                                                              : Completable.error(new NoOngoingRideException()));
    }

    private Completable addLocationRideEvent(final int rideId, final LocationData locationData) {
        return rideRepository.addLocationUpdateEvent(rideId, currentTimeProvider.getCurrentTimeMillis(), locationData);
    }
}
