package codecacao.babic.gpstracking.domain.time;

public interface CurrentTimeProvider {

    long getCurrentTimeMillis();

    long getCurrentTimestamp();
}
