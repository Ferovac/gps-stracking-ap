package codecacao.babic.gpstracking.domain.model;

public final class LocationData {

    public static final LocationData EMPTY = new LocationData(0L, 0f, 0f);

    public final long timestamp;
    public final float lat;
    public final float lng;

    public LocationData(final long timestamp, final float lat, final float lng) {
        this.timestamp = timestamp;
        this.lat = lat;
        this.lng = lng;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final LocationData that = (LocationData) o;

        if (timestamp != that.timestamp) {
            return false;
        }
        if (Float.compare(that.lat, lat) != 0) {
            return false;
        }
        return Float.compare(that.lng, lng) == 0;
    }

    @Override
    public int hashCode() {
        int result = (int) (timestamp ^ (timestamp >>> 32));
        result = 31 * result + (lat != +0.0f ? Float.floatToIntBits(lat) : 0);
        result = 31 * result + (lng != +0.0f ? Float.floatToIntBits(lng) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LocationData{" +
                "timestamp=" + timestamp +
                ", lat=" + lat +
                ", lng=" + lng +
                '}';
    }
}
