package codecacao.babic.gpstracking.domain.usecase;

import com.annimon.stream.Optional;

import codecacao.babic.gpstracking.domain.model.User;
import rx.Single;

public interface GetUserDataUseCase {

    Single<Optional<User>> execute();
}
