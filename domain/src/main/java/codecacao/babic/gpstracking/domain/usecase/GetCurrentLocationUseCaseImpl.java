package codecacao.babic.gpstracking.domain.usecase;

import codecacao.babic.gpstracking.domain.model.LocationData;
import codecacao.babic.gpstracking.domain.repository.LocationRepository;
import rx.Single;

public final class GetCurrentLocationUseCaseImpl implements GetCurrentLocationUseCase {

    private final LocationRepository locationRepository;

    public GetCurrentLocationUseCaseImpl(final LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    @Override
    public Single<LocationData> execute() {
        return locationRepository.getCurrentLocation();
    }
}
