package codecacao.babic.gpstracking.domain.repository;

import com.annimon.stream.Optional;

import java.util.List;

import codecacao.babic.gpstracking.domain.model.LocationData;
import codecacao.babic.gpstracking.domain.model.Ride;
import codecacao.babic.gpstracking.domain.model.RideEvent;
import codecacao.babic.gpstracking.domain.model.RideEventType;
import rx.Completable;
import rx.Single;

public interface RideRepository {

    Single<Integer> getNewRideId();

    Single<Optional<Integer>> getCurrentRideId();

    Completable startRide(int rideId, String startLocation, String endLocation, int numberOfPassengers, long timestamp, LocationData locationData);

    Completable sendRideEventToApi(RideEvent rideEvent);

    Completable addRideEvent(int rideId, long timestamp, LocationData locationData, RideEventType rideEventType);

    Completable addLocationUpdateEvent(int rideId, long timestamp, LocationData locationData);

    Completable sendLocationUpdateToApi(RideEvent rideEvent);

    Completable endRide();

    Single<Optional<Ride>> getRideById(int ride);

    Single<List<RideEvent>> getRideEvents(int rideId);

    Single<List<RideEvent>> getRideEventsById(List<Integer> ids);

    Single<List<Integer>> getUnsyncedRideEventIds();

    Completable removeRideEventIdFromUnsyncedIds(int id);

    Single<List<Ride>> getAllRides();
}
