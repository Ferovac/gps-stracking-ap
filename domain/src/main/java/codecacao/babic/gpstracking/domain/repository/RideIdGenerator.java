package codecacao.babic.gpstracking.domain.repository;

public interface RideIdGenerator {

    int generateId();
}
