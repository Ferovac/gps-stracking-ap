package codecacao.babic.gpstracking.domain.usecase;

import com.annimon.stream.Optional;

import codecacao.babic.gpstracking.domain.model.RideDetails;
import codecacao.babic.gpstracking.domain.repository.RideRepository;
import rx.Single;

public final class GetRideDetailsUseCaseImpl implements GetRideDetailsUseCase {

    private final RideRepository rideRepository;

    public GetRideDetailsUseCaseImpl(final RideRepository rideRepository) {
        this.rideRepository = rideRepository;
    }

    @Override
    public Single<RideDetails> execute(final int rideId) {
        return Single.zip(rideRepository.getRideById(rideId).map(Optional::get),
                          rideRepository.getRideEvents(rideId),
                          RideDetails::new);
    }
}
