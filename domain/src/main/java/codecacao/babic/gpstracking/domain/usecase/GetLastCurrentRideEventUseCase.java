package codecacao.babic.gpstracking.domain.usecase;

import com.annimon.stream.Optional;

import codecacao.babic.gpstracking.domain.model.RideEvent;
import rx.Single;

public interface GetLastCurrentRideEventUseCase {

    Single<Optional<RideEvent>> execute();
}
