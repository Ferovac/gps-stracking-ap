package codecacao.babic.gpstracking.domain.model;

public final class RideEvent {

    public final int id;
    public final int rideId;
    public final long timestamp;
    public final float latitude;
    public final float longitude;
    public final RideEventType eventType;

    public RideEvent(final int rideId, final long timestamp, final float latitude, final float longitude, final RideEventType eventType) {
        this(0, rideId, timestamp, latitude, longitude, eventType);
    }

    public RideEvent(final int id, final int rideId, final long timestamp, final float latitude, final float longitude, final RideEventType eventType) {
        this.id = id;
        this.rideId = rideId;
        this.timestamp = timestamp;
        this.latitude = latitude;
        this.longitude = longitude;
        this.eventType = eventType;
    }

    public RideEvent copyOnId(final int id) {
        return new RideEvent(id, rideId, timestamp, latitude, longitude, eventType);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final RideEvent rideEvent = (RideEvent) o;

        if (id != rideEvent.id) {
            return false;
        }
        if (rideId != rideEvent.rideId) {
            return false;
        }
        if (timestamp != rideEvent.timestamp) {
            return false;
        }
        if (Float.compare(rideEvent.latitude, latitude) != 0) {
            return false;
        }
        if (Float.compare(rideEvent.longitude, longitude) != 0) {
            return false;
        }
        return eventType == rideEvent.eventType;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + rideId;
        result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
        result = 31 * result + (latitude != +0.0f ? Float.floatToIntBits(latitude) : 0);
        result = 31 * result + (longitude != +0.0f ? Float.floatToIntBits(longitude) : 0);
        result = 31 * result + (eventType != null ? eventType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RideEvent{" +
                "id=" + id +
                ", rideId=" + rideId +
                ", timestamp=" + timestamp +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", eventType=" + eventType +
                '}';
    }
}
