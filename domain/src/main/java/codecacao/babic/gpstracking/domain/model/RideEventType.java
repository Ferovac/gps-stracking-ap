package codecacao.babic.gpstracking.domain.model;

public enum RideEventType {

    STARTED(0),
    PASSENGER_PICKED(10),
    STOP_OVER(20),
    CONTINUE(30),
    END_RIDE(40),
    LOCATION_UPDATE(50),
    UNKNOWN(60);

    public final int value;

    public static RideEventType from(final int rideEventTypeInt) {

        switch (rideEventTypeInt) {

            case 0:
                return STARTED;

            case 10:
                return PASSENGER_PICKED;

            case 20:
                return STOP_OVER;

            case 30:
                return CONTINUE;

            case 40:
                return END_RIDE;

            case 50:
                return LOCATION_UPDATE;

            default:
                return UNKNOWN;
        }
    }

    RideEventType(final int value) {
        this.value = value;
    }
}
