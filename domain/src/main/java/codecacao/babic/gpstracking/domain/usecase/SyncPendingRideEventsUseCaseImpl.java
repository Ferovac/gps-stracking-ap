package codecacao.babic.gpstracking.domain.usecase;

import codecacao.babic.gpstracking.domain.model.RideEvent;
import codecacao.babic.gpstracking.domain.model.RideEventType;
import codecacao.babic.gpstracking.domain.repository.RideRepository;
import rx.Completable;
import rx.Observable;

public final class SyncPendingRideEventsUseCaseImpl implements SyncPendingRideEventsUseCase {

    private final RideRepository rideRepository;

    public SyncPendingRideEventsUseCaseImpl(final RideRepository rideRepository) {
        this.rideRepository = rideRepository;
    }

    @Override
    public Completable execute() {
        return rideRepository.getUnsyncedRideEventIds()
                             .flatMap(rideRepository::getRideEventsById)
                             .flatMapObservable(Observable::from)
                             .flatMapCompletable(this::sendRideEvent)
                             .toCompletable();
    }

    private Completable sendRideEvent(final RideEvent rideEvent) {
        return rideEvent.eventType == RideEventType.LOCATION_UPDATE ? sendRideEventLocationUpdateToApi(rideEvent) : sendRideEventToApi(rideEvent);
    }

    private Completable sendRideEventLocationUpdateToApi(final RideEvent rideEvent) {
        return rideRepository.sendLocationUpdateToApi(rideEvent)
                             .concatWith(rideRepository.removeRideEventIdFromUnsyncedIds(rideEvent.id))
                             .onErrorComplete();
    }

    private Completable sendRideEventToApi(final RideEvent rideEvent) {
        return rideRepository.sendRideEventToApi(rideEvent)
                             .concatWith(rideRepository.removeRideEventIdFromUnsyncedIds(rideEvent.id))
                             .onErrorComplete();
    }
}
