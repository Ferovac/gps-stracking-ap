package codecacao.babic.gpstracking.domain.usecase;

import codecacao.babic.gpstracking.domain.model.RideDetails;
import rx.Single;

public interface GetRideDetailsUseCase {

    Single<RideDetails> execute(int rideId);
}
