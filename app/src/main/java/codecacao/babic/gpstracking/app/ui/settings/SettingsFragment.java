package codecacao.babic.gpstracking.app.ui.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import codecacao.babic.gpstracking.app.R;
import codecacao.babic.gpstracking.app.base.BaseFragment;
import codecacao.babic.gpstracking.app.base.ScopedPresenter;
import codecacao.babic.gpstracking.app.injection.fragment.FragmentComponent;
import codecacao.babic.gpstracking.app.utils.ui.ImageLoader;

public final class SettingsFragment extends BaseFragment implements SettingsContract.View {

    public static final String TAG = "SettingsFragment";

    @BindView(R.id.fragment_settings_user_first_name_text_view)
    TextView userFirstNameTextView;

    @BindView(R.id.fragment_settings_user_last_name_text_view)
    TextView userLastNameTextView;

    @BindView(R.id.fragment_settings_user_age_text_view)
    TextView userAgeTextView;

    @BindView(R.id.fragment_settings_user_licence_plate)
    TextView userVehicleLicencePlateTextView;

    @BindView(R.id.fragment_settings_image_view)
    ImageView imageView;

    @BindString(R.string.settings_screen_first_name_template)
    String firstNameTemplate;

    @BindString(R.string.settings_screen_last_name_template)
    String lastNameTemplate;

    @BindString(R.string.settings_screen_age_template)
    String ageTemplate;

    @BindString(R.string.settings_screen_licence_plate_template)
    String licencePlateTemplate;

    @Inject
    SettingsContract.Presenter presenter;

    @Inject
    ImageLoader imageLoader;

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    protected void inject(final FragmentComponent component) {
        component.inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_settings;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.init();
    }

    @Override
    public ScopedPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void render(final SettingsScreenViewModel viewModel) {
        populateUserData(viewModel.userViewModel);
    }

    private void populateUserData(final UserViewModel viewModel) {
        userFirstNameTextView.setText(String.format(firstNameTemplate, viewModel.firstName));
        userLastNameTextView.setText(String.format(lastNameTemplate, viewModel.lastName));
        userAgeTextView.setText(String.format(ageTemplate, viewModel.age));
        userVehicleLicencePlateTextView.setText(String.format(licencePlateTemplate, viewModel.licencePlate));
        imageLoader.loadImageCenterCrop(viewModel.imageUrl, imageView);
    }
}
