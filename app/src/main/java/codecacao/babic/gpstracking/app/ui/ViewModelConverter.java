package codecacao.babic.gpstracking.app.ui;

import java.util.List;

import codecacao.babic.gpstracking.app.ui.previousrides.RideViewModel;
import codecacao.babic.gpstracking.app.ui.ridedetails.RideEventViewModel;
import codecacao.babic.gpstracking.app.ui.settings.UserViewModel;
import codecacao.babic.gpstracking.domain.model.Ride;
import codecacao.babic.gpstracking.domain.model.RideEvent;
import codecacao.babic.gpstracking.domain.model.User;

public interface ViewModelConverter {

    List<RideViewModel> toRideViewModels(List<Ride> rides);

    List<RideEventViewModel> toRideEventViewModels(List<RideEvent> rideEvents);

    UserViewModel toUserViewModel(final User user);
}