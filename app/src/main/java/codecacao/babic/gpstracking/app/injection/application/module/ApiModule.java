package codecacao.babic.gpstracking.app.injection.application.module;

import android.content.res.Resources;

import javax.inject.Singleton;

import codecacao.babic.gpstracking.app.BuildConfig;
import codecacao.babic.gpstracking.data.network.configuration.Urls;
import codecacao.babic.gpstracking.data.network.configuration.UrlsImpl;
import codecacao.babic.gpstracking.data.network.converter.ApiConverter;
import codecacao.babic.gpstracking.data.network.converter.ApiConverterImpl;
import codecacao.babic.gpstracking.data.network.service.ApiService;
import codecacao.babic.gpstracking.domain.utils.DateUtils;
import codecacao.babic.gpstracking.domain.utils.StringUtils;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public final class ApiModule {

    @Provides
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return interceptor;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(final HttpLoggingInterceptor interceptor) {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            builder.addNetworkInterceptor(interceptor);
        }

        return builder.build();
    }

    @Provides
    @Singleton
    Urls provideUrls(final Resources resources) {
        return new UrlsImpl(resources);
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(final Urls urls, final OkHttpClient okHttpClient) {
        return new Retrofit.Builder().baseUrl(urls.getServerUrl())
                                     .client(okHttpClient)
                                     .addConverterFactory(GsonConverterFactory.create())
                                     .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                                     .build();
    }

    @Provides
    @Singleton
    ApiService provideGitHubService(final Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @Singleton
    ApiConverter provideApiConverter(final DateUtils dateUtils, final StringUtils stringUtils) {
        return new ApiConverterImpl(dateUtils, stringUtils);
    }

    public interface Exposes {

        ApiConverter apiConverter();

        ApiService apiService();
    }
}
