package codecacao.babic.gpstracking.app.ui.previousrides;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.annimon.stream.Optional;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import codecacao.babic.gpstracking.app.R;

public final class PreviousRidesAdapter extends RecyclerView.Adapter<PreviousRidesAdapter.ViewHolder> {

    public interface PreviousRidesAdapterListener {

        void onRideItemClicked(int rideId);
    }

    private final List<RideViewModel> viewModels = new ArrayList<>();

    private final LayoutInflater inflater;

    private Optional<PreviousRidesAdapterListener> listener = Optional.empty();

    public PreviousRidesAdapter(final LayoutInflater inflater) {
        this.inflater = inflater;
    }

    @Override
    public PreviousRidesAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.adapter_previous_rides_row_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final PreviousRidesAdapter.ViewHolder holder, final int index) {
        holder.populate(listener, viewModels.get(index));
    }

    @Override
    public int getItemCount() {
        return viewModels.size();
    }

    public void setData(final List<RideViewModel> viewModels) {
        this.viewModels.clear();
        this.viewModels.addAll(viewModels);
        notifyDataSetChanged();
    }

    public void setListener(final PreviousRidesAdapterListener listener) {
        this.listener = Optional.ofNullable(listener);
        notifyDataSetChanged();
    }

    static final class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapter_previous_rides_row_item_start_location_text_view)
        TextView startLocation;

        @BindView(R.id.adapter_previous_rides_row_item_end_location_text_view)
        TextView endLocation;

        @BindView(R.id.adapter_previous_rides_row_item_number_of_passengers_text_view)
        TextView numberOfPassengers;

        @BindString(R.string.previous_ride_adapter_row_item_start_location_template)
        String startLocationTemplate;

        @BindString(R.string.previous_ride_adapter_row_item_end_location_template)
        String endLocationTemplate;

        @BindString(R.string.previous_ride_adapter_row_item_number_of_passengers_template)
        String numberOfPassengersTemplate;

        private Optional<PreviousRidesAdapterListener> listener = Optional.empty();
        private Optional<RideViewModel> rideViewModel;

        public ViewHolder(final View rootView) {
            super(rootView);
            bindViews(rootView);
        }

        private void bindViews(final View rootView) {
            ButterKnife.bind(this, rootView);
        }

        @OnClick(R.id.adapter_previous_rides_content_layout)
        void onRootViewClicked() {
            if (rideViewModel.isPresent()) {
                listener.ifPresent(listener -> listener.onRideItemClicked(rideViewModel.get().id));
            }
        }

        void populate(final Optional<PreviousRidesAdapterListener> listener, final RideViewModel viewModel) {
            this.listener = listener;

            rideViewModel = Optional.of(viewModel);

            startLocation.setText(String.format(startLocationTemplate, viewModel.startLocation));
            endLocation.setText(String.format(endLocationTemplate, viewModel.endLocation));
            numberOfPassengers.setText(String.format(numberOfPassengersTemplate, viewModel.numberOfPassengers));
        }
    }
}
