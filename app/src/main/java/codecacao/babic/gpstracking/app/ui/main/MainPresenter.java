package codecacao.babic.gpstracking.app.ui.main;

import android.util.Log;

import javax.inject.Inject;

import codecacao.babic.gpstracking.app.base.BasePresenter;
import codecacao.babic.gpstracking.app.utils.Actions;
import codecacao.babic.gpstracking.domain.usecase.SyncPendingRideEventsUseCase;

public final class MainPresenter extends BasePresenter<MainContract.View> implements MainContract.Presenter {

    @Inject
    SyncPendingRideEventsUseCase syncPendingRideEventsUseCase;

    private boolean syncInProgress;

    public MainPresenter(final MainContract.View view) {
        super(view);
    }

    @Override
    public void activate() {
        super.activate();
        doIfConnectedToInternet(this::syncPendingEvents, Actions.noOpAction0());
    }

    private void syncPendingEvents() {
        if (syncInProgress) {
            return;
        }

        syncInProgress = true;
        viewActionQueue.subscribeTo(syncPendingRideEventsUseCase.execute()
                                                                .subscribeOn(backgroundScheduler),
                                    view -> syncInProgress = false,
                                    throwable -> {
                                        logError(throwable);
                                        syncInProgress = false;
                                    });
    }

    @Override
    protected void onConnectivityChange(final boolean isConnected) {
        super.onConnectivityChange(isConnected);
        if (isConnected) {
            syncPendingEvents();
        }
    }

    @Override
    public void showContentScreen() {
        router.showContentScreen();
    }
}
