package codecacao.babic.gpstracking.app.ui.previousrides;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import codecacao.babic.gpstracking.app.R;
import codecacao.babic.gpstracking.app.base.BaseFragment;
import codecacao.babic.gpstracking.app.base.ScopedPresenter;
import codecacao.babic.gpstracking.app.injection.fragment.FragmentComponent;

public final class PreviousRideFragment extends BaseFragment implements PreviousRidesContract.View, PreviousRidesAdapter.PreviousRidesAdapterListener {

    @BindView(R.id.fragment_previous_rides_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.fragment_previous_rides_retry_button)
    Button retryButton;

    @BindView(R.id.fragment_previous_rides_no_rides_text_view)
    TextView noRidesTextView;

    @Inject
    PreviousRidesContract.Presenter presenter;

    @Inject
    PreviousRidesAdapter adapter;

    public static PreviousRideFragment newInstance() {
        return new PreviousRideFragment();
    }

    @Override
    protected void inject(final FragmentComponent component) {
        component.inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_previous_rides;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();
        presenter.init();
        adapter.setListener(this);
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public ScopedPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void render(final RideScreenViewModel viewModel) {
        adapter.setData(viewModel.rideViewModels);
        retryButton.setVisibility(viewModel.showRetryButton ? View.VISIBLE : View.GONE);
        noRidesTextView.setVisibility(viewModel.showNoRidesScreen ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showRideFetchErrorPrompt() {
        showShortToast(R.string.previous_ride_screen_something_went_wrong_message);
    }

    @Override
    public void onRideItemClicked(final int rideId) {
        presenter.showRideDetails(rideId);
    }
}
