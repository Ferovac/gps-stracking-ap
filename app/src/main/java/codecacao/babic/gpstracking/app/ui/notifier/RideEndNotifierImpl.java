package codecacao.babic.gpstracking.app.ui.notifier;

import rx.Observable;
import rx.subjects.PublishSubject;

public final class RideEndNotifierImpl implements RideEndNotifier {

    private final PublishSubject<EndEvent> publishSubject = PublishSubject.create();

    @Override
    public void notifyRideEnded() {
        publishSubject.onNext(new EndEvent());
    }

    @Override
    public Observable getNotifierObservable() {
        return publishSubject;
    }

    public static final class EndEvent {

        private EndEvent() {

        }
    }
}
