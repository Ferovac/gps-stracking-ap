package codecacao.babic.gpstracking.app.injection.fragment.module;

import android.content.res.Resources;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;

import codecacao.babic.gpstracking.app.ui.main.MainPagerAdapter;
import codecacao.babic.gpstracking.app.ui.previousrides.PreviousRidesAdapter;
import codecacao.babic.gpstracking.app.ui.ridedetails.RideEventsAdapter;
import dagger.Module;
import dagger.Provides;

@Module
public final class FragmentUiModule {

    @Provides
    MainPagerAdapter provideMainPagerAdapter(final FragmentManager fragmentManager, final Resources resources) {
        return new MainPagerAdapter(fragmentManager, resources);
    }

    @Provides
    PreviousRidesAdapter providePreviousRidesAdapter(final LayoutInflater inflater) {
        return new PreviousRidesAdapter(inflater);
    }

    @Provides
    RideEventsAdapter provideRideEventsAdapter(final LayoutInflater inflater) {
        return new RideEventsAdapter(inflater);
    }

    public interface Exposes {

        MainPagerAdapter mainPagerAdapter();

        PreviousRidesAdapter previousRidesAdapter();

        RideEventsAdapter rideEventsAdapter();
    }
}
