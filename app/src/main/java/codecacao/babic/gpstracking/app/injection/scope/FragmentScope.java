package codecacao.babic.gpstracking.app.injection.scope;

import javax.inject.Scope;

@Scope
public @interface FragmentScope { }
