package codecacao.babic.gpstracking.app.notification;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import codecacao.babic.gpstracking.app.ui.main.MainActivity;

public final class NotificationFactoryImpl implements NotificationFactory {

    private static final String RIDE_ONGOING = "Ride ongoing";
    private static final String RIDE_FROM_TO_TEMPLATE = "Ride from %s to %s.";

    private static final int CHANNEL_ID = 100;

    private final Context context;

    public NotificationFactoryImpl(final Context context) {
        this.context = context;
    }

    @Override
    public Notification buildNotification() {
        return new NotificationCompat.Builder(context, String.valueOf(CHANNEL_ID)).setSmallIcon(android.R.mipmap.sym_def_app_icon)
                                                                                  .setContentTitle(RIDE_ONGOING)
                                                                                  .setContentText(RIDE_FROM_TO_TEMPLATE)
                                                                                  .setContentIntent(getPendingIntent())
                                                                                  .build();
    }

    private PendingIntent getPendingIntent() {
        return PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class), 0);
    }
}
