package codecacao.babic.gpstracking.app.injection.activity;

import codecacao.babic.gpstracking.app.ui.main.MainActivity;
import codecacao.babic.gpstracking.app.ui.main.MainPresenter;
import codecacao.babic.gpstracking.app.ui.onboarding.OnboardingActivity;
import codecacao.babic.gpstracking.app.ui.onboarding.OnboardingPresenter;

public interface ActivityComponentInjects {

    void inject(MainActivity activity);

    void inject(MainPresenter presenter);

    void inject(OnboardingActivity activity);

    void inject(OnboardingPresenter presenter);
}
