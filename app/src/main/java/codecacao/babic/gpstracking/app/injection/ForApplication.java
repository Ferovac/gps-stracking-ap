package codecacao.babic.gpstracking.app.injection;

import javax.inject.Qualifier;

@Qualifier
public @interface ForApplication { }
