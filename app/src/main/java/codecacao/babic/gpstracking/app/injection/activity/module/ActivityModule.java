package codecacao.babic.gpstracking.app.injection.activity.module;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.inputmethod.InputMethodManager;

import codecacao.babic.gpstracking.app.injection.ForActivity;
import codecacao.babic.gpstracking.app.injection.activity.DaggerActivity;
import codecacao.babic.gpstracking.app.injection.scope.ActivityScope;
import codecacao.babic.gpstracking.app.ui.Router;
import codecacao.babic.gpstracking.app.ui.RouterImpl;
import codecacao.babic.gpstracking.app.utils.ActivityUtils;
import codecacao.babic.gpstracking.app.utils.ActivityUtilsImpl;
import codecacao.babic.gpstracking.app.utils.ui.ImageLoader;
import codecacao.babic.gpstracking.app.utils.ui.ImageLoaderImpl;
import codecacao.babic.gpstracking.app.utils.ui.KeyboardUtils;
import codecacao.babic.gpstracking.app.utils.ui.KeyboardUtilsImpl;
import codecacao.babic.gpstracking.domain.utils.ListUtils;
import dagger.Module;
import dagger.Provides;

@Module
public final class ActivityModule {

    private final DaggerActivity activity;

    public ActivityModule(final DaggerActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    ActivityUtils provideActivityUtils() {
        return new ActivityUtilsImpl();
    }

    @Provides
    @ForActivity
    Context provideActivityContext() {
        return activity;
    }

    @Provides
    @ActivityScope
    FragmentManager provideFragmentManager() {
        return activity.getSupportFragmentManager();
    }

    @Provides
    @ActivityScope
    ImageLoader provideImageLoader(@ForActivity final Context context) {
        return new ImageLoaderImpl(context);
    }

    @Provides
    @ActivityScope
    InputMethodManager provideInputMethodManager(@ForActivity final Context context) {
        return (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    @Provides
    @ActivityScope
    KeyboardUtils provideKeyboardUtils(final InputMethodManager inputMethodManager) {
        return new KeyboardUtilsImpl(inputMethodManager);
    }

    @Provides
    @ActivityScope
    LayoutInflater provideLayoutInflater(@ForActivity final Context context) {
        return LayoutInflater.from(context);
    }

    @Provides
    @ActivityScope
    Router provideRouter(final FragmentManager fragmentManager, final ListUtils listUtils) {
        return new RouterImpl(activity, fragmentManager, listUtils);
    }

    public interface Exposes {

        ActivityUtils activityUtils();

        @ForActivity
        Context provideActivityContext();

        ImageLoader imageLoader();

        KeyboardUtils keyboardUtils();

        LayoutInflater layoutInflater();

        Router router();
    }
}
