package codecacao.babic.gpstracking.app.maintenance;

public final class ForegroundServiceContract {

    private ForegroundServiceContract() {

    }

    public interface Presenter {

        void activate();

        void deactivate();
    }
}
