package codecacao.babic.gpstracking.app.ui.ridedetails;

import codecacao.babic.gpstracking.app.base.BaseView;
import codecacao.babic.gpstracking.app.base.ScopedPresenter;

public final class RideDetailsContract {

    private RideDetailsContract() {

    }

    public interface Presenter extends ScopedPresenter {

        void init(int rideId);
    }

    public interface View extends BaseView {

        void render(RideDetailScreenViewModel viewModel);
    }
}
