package codecacao.babic.gpstracking.app.ui.ridedetails;

import java.util.List;

public final class RideDetailScreenViewModel {

    public final String startLocation;
    public final String endLocation;
    public final String numberOfPassengers;
    public final List<RideEventViewModel> rideEventViewModels;

    public RideDetailScreenViewModel(final String startLocation, final String endLocation, final String numberOfPassengers, final List<RideEventViewModel> rideEventViewModels) {
        this.startLocation = startLocation;
        this.endLocation = endLocation;
        this.numberOfPassengers = numberOfPassengers;
        this.rideEventViewModels = rideEventViewModels;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final RideDetailScreenViewModel that = (RideDetailScreenViewModel) o;

        if (startLocation != null ? !startLocation.equals(that.startLocation) : that.startLocation != null) {
            return false;
        }
        if (endLocation != null ? !endLocation.equals(that.endLocation) : that.endLocation != null) {
            return false;
        }
        if (numberOfPassengers != null ? !numberOfPassengers.equals(that.numberOfPassengers) : that.numberOfPassengers != null) {
            return false;
        }
        return rideEventViewModels != null ? rideEventViewModels.equals(that.rideEventViewModels) : that.rideEventViewModels == null;
    }

    @Override
    public int hashCode() {
        int result = startLocation != null ? startLocation.hashCode() : 0;
        result = 31 * result + (endLocation != null ? endLocation.hashCode() : 0);
        result = 31 * result + (numberOfPassengers != null ? numberOfPassengers.hashCode() : 0);
        result = 31 * result + (rideEventViewModels != null ? rideEventViewModels.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RideDetailScreenViewModel{" +
                "startLocation='" + startLocation + '\'' +
                ", endLocation='" + endLocation + '\'' +
                ", numberOfPassengers='" + numberOfPassengers + '\'' +
                ", rideEventViewModels=" + rideEventViewModels +
                '}';
    }
}
