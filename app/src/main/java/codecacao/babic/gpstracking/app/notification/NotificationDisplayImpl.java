package codecacao.babic.gpstracking.app.notification;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.annimon.stream.Optional;

import codecacao.babic.gpstracking.app.maintenance.ForegroundGuardService;
import codecacao.babic.gpstracking.domain.utils.NotificationDisplay;
import rx.Completable;

public final class NotificationDisplayImpl implements NotificationDisplay {

    private final Context context;

    private Optional<ServiceConnection> serviceConnection = Optional.empty();

    public NotificationDisplayImpl(final Context context) {
        this.context = context;
    }

    @Override
    public Completable showRideOngoingNotification(final String startLocation, final String endLocation) {
        return Completable.fromAction(() -> showRideOngoingNotificationInternal(startLocation, endLocation));
    }

    private void showRideOngoingNotificationInternal(final String startLocation, final String endLocation) {
        context.bindService(createIntent(), new ServiceConnectionImpl(), Context.BIND_AUTO_CREATE);
        context.startService(createIntent());
    }

    private Intent createIntent() {
        return new Intent(context, ForegroundGuardService.class);
    }

    @Override
    public Completable stopDisplayingNotification() {
        if (serviceConnection.isPresent()) {
            return unbindService();
        } else {
            return Completable.complete();
        }
    }

    private Completable unbindService() {
        return Completable.fromAction(() -> {
            context.unbindService(serviceConnection.get());
            context.stopService(new Intent(context, ForegroundGuardService.class));
            resetServiceConnection();
        });
    }

    private void resetServiceConnection() {
        serviceConnection = Optional.empty();
    }

    private final class ServiceConnectionImpl implements ServiceConnection {

        @Override
        public void onServiceConnected(final ComponentName name, final IBinder service) {
            serviceConnection = Optional.of(this);
        }

        @Override
        public void onServiceDisconnected(final ComponentName name) {
            serviceConnection = Optional.empty();
        }
    }
}
