package codecacao.babic.gpstracking.app.injection.activity;

import codecacao.babic.gpstracking.app.injection.activity.module.ActivityModule;
import codecacao.babic.gpstracking.app.injection.activity.module.ActivityPresenterModule;
import codecacao.babic.gpstracking.app.injection.activity.module.UiAdapterModule;
import codecacao.babic.gpstracking.app.injection.user.UserComponentExposes;

public interface ActivityComponentExposes extends UserComponentExposes,
                                                  ActivityModule.Exposes,
                                                  ActivityPresenterModule.Exposes,
                                                  UiAdapterModule.Exposes { }
