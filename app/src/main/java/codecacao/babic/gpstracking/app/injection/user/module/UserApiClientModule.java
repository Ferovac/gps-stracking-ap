package codecacao.babic.gpstracking.app.injection.user.module;

import codecacao.babic.gpstracking.app.injection.scope.UserScope;
import codecacao.babic.gpstracking.data.network.client.MockRideClient;
import codecacao.babic.gpstracking.data.network.client.RideClient;
import codecacao.babic.gpstracking.data.network.client.RideClientImpl;
import codecacao.babic.gpstracking.data.network.service.ApiService;
import codecacao.babic.gpstracking.domain.utils.DateUtils;
import dagger.Module;
import dagger.Provides;

@Module
public final class UserApiClientModule {

    @Provides
    @UserScope
    MockRideClient provideMockRideClient() {
        return new MockRideClient();

    }

    @Provides
    @UserScope
    RideClient provideRideClient(final MockRideClient mockRideClient, final ApiService apiService, final DateUtils dateUtils) {
//        if (BuildConfig.DEBUG) {
//            return mockRideClient;
//        } else {
//            return new RideClientImpl(apiService, dateUtils);
//        }

        return new RideClientImpl(apiService, dateUtils);
    }

    public interface Exposes {

        MockRideClient mockRideClient();

        RideClient rideClient();
    }
}
