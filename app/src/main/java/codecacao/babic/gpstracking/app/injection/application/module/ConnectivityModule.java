package codecacao.babic.gpstracking.app.injection.application.module;

import android.content.Context;

import javax.inject.Named;
import javax.inject.Singleton;

import codecacao.babic.gpstracking.app.injection.ForApplication;
import codecacao.babic.gpstracking.app.utils.connectivity.ConnectivityManagerWrapper;
import codecacao.babic.gpstracking.app.utils.connectivity.ConnectivityManagerWrapperImpl;
import codecacao.babic.gpstracking.app.utils.connectivity.ConnectivityReceiver;
import codecacao.babic.gpstracking.app.utils.connectivity.ConnectivityReceiverImpl;
import codecacao.babic.gpstracking.app.utils.connectivity.NetworkUtils;
import codecacao.babic.gpstracking.app.utils.connectivity.NetworkUtilsImpl;
import dagger.Module;
import dagger.Provides;
import rx.Scheduler;

@Module
public final class ConnectivityModule {

    @Provides
    @Singleton
    ConnectivityManagerWrapper provideConnectivityManagerWrapper(@ForApplication final Context context) {
        return new ConnectivityManagerWrapperImpl(context);
    }

    @Provides
    @Singleton
    ConnectivityReceiver provideConnectivityReceiver(@ForApplication final Context context, final NetworkUtils networkUtils,
                                                     @Named(ThreadingModule.BACKGROUND_SCHEDULER) final Scheduler backgroundScheduler) {

        return new ConnectivityReceiverImpl(context, networkUtils, backgroundScheduler);
    }

    @Provides
    @Singleton
    NetworkUtils provideNetworkUtils(final ConnectivityManagerWrapper connectivityManagerWrapper) {
        return new NetworkUtilsImpl(connectivityManagerWrapper);
    }

    public interface Exposes {

        ConnectivityReceiver provideConnectivityReceiver();
    }
}
