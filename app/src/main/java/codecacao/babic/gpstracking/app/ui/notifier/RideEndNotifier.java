package codecacao.babic.gpstracking.app.ui.notifier;

import rx.Observable;

public interface RideEndNotifier {

    void notifyRideEnded();

    Observable<RideEndNotifierImpl.EndEvent> getNotifierObservable();
}
