package codecacao.babic.gpstracking.app.injection.application.module;

import javax.inject.Singleton;

import codecacao.babic.gpstracking.device.repository.LocationRepositoryImpl;
import codecacao.babic.gpstracking.domain.repository.LocationRepository;
import dagger.Module;
import dagger.Provides;

@Module
public final class RepositoryModule {

    @Provides
    @Singleton
    LocationRepository provideLocationRepository() {
        return new LocationRepositoryImpl();
    }

    public interface Exposes {

        LocationRepository locationRepository();
    }
}
