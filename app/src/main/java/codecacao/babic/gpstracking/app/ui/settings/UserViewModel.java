package codecacao.babic.gpstracking.app.ui.settings;

public final class UserViewModel {

    public final String firstName;
    public final String lastName;
    public final String age;
    public final String licencePlate;
    public final String imageUrl;

    public UserViewModel(final String firstName, final String lastName, final String age, final String licencePlate, final String imageUrl) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.licencePlate = licencePlate;
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final UserViewModel that = (UserViewModel) o;

        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) {
            return false;
        }
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) {
            return false;
        }
        if (age != null ? !age.equals(that.age) : that.age != null) {
            return false;
        }
        if (licencePlate != null ? !licencePlate.equals(that.licencePlate) : that.licencePlate != null) {
            return false;
        }
        return imageUrl != null ? imageUrl.equals(that.imageUrl) : that.imageUrl == null;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (age != null ? age.hashCode() : 0);
        result = 31 * result + (licencePlate != null ? licencePlate.hashCode() : 0);
        result = 31 * result + (imageUrl != null ? imageUrl.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserViewModel{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age='" + age + '\'' +
                ", licencePlate='" + licencePlate + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
