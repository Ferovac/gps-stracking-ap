package codecacao.babic.gpstracking.app.injection.application.module;

import android.content.Context;
import android.content.res.Resources;

import javax.inject.Singleton;

import codecacao.babic.gpstracking.app.application.GpsTrackingApplication;
import codecacao.babic.gpstracking.app.injection.ForApplication;
import codecacao.babic.gpstracking.app.notification.NotificationDisplayImpl;
import codecacao.babic.gpstracking.app.notification.NotificationFactory;
import codecacao.babic.gpstracking.app.notification.NotificationFactoryImpl;
import codecacao.babic.gpstracking.app.ui.notifier.RideEndNotifier;
import codecacao.babic.gpstracking.app.ui.notifier.RideEndNotifierImpl;
import codecacao.babic.gpstracking.domain.delegate.UserComponentDelegate;
import codecacao.babic.gpstracking.domain.utils.NotificationDisplay;
import dagger.Module;
import dagger.Provides;

@Module
public final class ApplicationModule {

    private final GpsTrackingApplication application;

    public ApplicationModule(final GpsTrackingApplication application) {
        this.application = application;
    }

    @Provides
    @ForApplication
    Context provideApplicationContext() {
        return application;
    }

    @Provides
    @Singleton
    Resources provideResources() {
        return application.getResources();
    }

    @Provides
    @Singleton
    UserComponentDelegate provideUserComponentDelegate() {
        return application;
    }

    @Provides
    @Singleton
    RideEndNotifier provideRideEndNotifier() {
        return new RideEndNotifierImpl();
    }

    @Provides
    @Singleton
    NotificationDisplay provideNotificationDisplay(@ForApplication final Context context) {
        return new NotificationDisplayImpl(context);
    }

    @Provides
    @Singleton
    NotificationFactory provideNotificationFactory(@ForApplication final Context context) {
        return new NotificationFactoryImpl(context);
    }

    public interface Exposes {

        @ForApplication
        Context context();

        Resources resources();

        UserComponentDelegate userComponentDelegate();

        RideEndNotifier RideEndNotifier();

        NotificationDisplay notificationDisplay();

        NotificationFactory notificationFactory();
    }
}
