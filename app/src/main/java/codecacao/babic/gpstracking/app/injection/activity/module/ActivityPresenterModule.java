package codecacao.babic.gpstracking.app.injection.activity.module;

import codecacao.babic.gpstracking.app.injection.activity.DaggerActivity;
import codecacao.babic.gpstracking.app.injection.scope.ActivityScope;
import codecacao.babic.gpstracking.app.ui.main.MainContract;
import codecacao.babic.gpstracking.app.ui.main.MainPresenter;
import codecacao.babic.gpstracking.app.ui.onboarding.OnboardingContract;
import codecacao.babic.gpstracking.app.ui.onboarding.OnboardingPresenter;
import dagger.Module;
import dagger.Provides;

@Module
public final class ActivityPresenterModule {

    private final DaggerActivity daggerActivity;

    public ActivityPresenterModule(final DaggerActivity daggerActivity) {
        this.daggerActivity = daggerActivity;
    }

    @Provides
    @ActivityScope
    OnboardingContract.Presenter provideOnboardingPresenter() {
        final OnboardingPresenter presenter = new OnboardingPresenter((OnboardingContract.View) daggerActivity);
        daggerActivity.getActivityComponent().inject(presenter);

        return presenter;
    }

    @Provides
    @ActivityScope
    MainContract.Presenter provideMainPresenter() {
        final MainPresenter presenter = new MainPresenter((MainContract.View) daggerActivity);
        daggerActivity.getActivityComponent().inject(presenter);

        return presenter;
    }

    public interface Exposes {

    }
}
