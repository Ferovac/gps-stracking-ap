package codecacao.babic.gpstracking.app.ui;

public interface Router {

    void showMainScreen();

    void showContentScreen();

    void showRideDetails(int rideId);

    void showSettingsScreen();

    void goBack();

    boolean showPageInExternalBrowser(String url);
}
