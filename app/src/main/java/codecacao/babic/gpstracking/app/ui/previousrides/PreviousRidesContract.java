package codecacao.babic.gpstracking.app.ui.previousrides;

import codecacao.babic.gpstracking.app.base.BaseView;
import codecacao.babic.gpstracking.app.base.ScopedPresenter;

public final class PreviousRidesContract {

    private PreviousRidesContract() {

    }

    public interface Presenter extends ScopedPresenter {

        void init();

        void showRideDetails(int rideId);
    }

    public interface View extends BaseView {

        void render(RideScreenViewModel viewModel);

        void showRideFetchErrorPrompt();
    }
}
