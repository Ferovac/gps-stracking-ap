package codecacao.babic.gpstracking.app.maintenance;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import codecacao.babic.gpstracking.app.injection.application.module.ThreadingModule;
import codecacao.babic.gpstracking.app.utils.Actions;
import codecacao.babic.gpstracking.domain.model.LocationData;
import codecacao.babic.gpstracking.domain.usecase.AddLocationUpdateEventUseCase;
import codecacao.babic.gpstracking.domain.usecase.GetCurrentLocationUseCase;
import rx.Observable;
import rx.Scheduler;
import rx.Single;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public final class ForegroundServicePresenter implements ForegroundServiceContract.Presenter {

    private static final long GPS_UPDATE_INTERVAL = 5000L;

    @Inject
    GetCurrentLocationUseCase getCurrentLocationUseCase;

    @Inject
    AddLocationUpdateEventUseCase addLocationUpdateEventUseCase;

    @Inject
    @Named(ThreadingModule.MAIN_SCHEDULER)
    Scheduler mainThreadScheduler;

    @Inject
    @Named(ThreadingModule.BACKGROUND_SCHEDULER)
    Scheduler backgroundScheduler;

    private CompositeSubscription compositeSubscription;

    @Override
    public void activate() {
        initCompositeSubscription();
        addSubscription(Observable.interval(GPS_UPDATE_INTERVAL, TimeUnit.MILLISECONDS)
                                  .flatMapSingle(time -> getCurrentLocationUseCase.execute())
                                  .flatMapSingle(this::addRideEventCompletable)
                                  .subscribeOn(backgroundScheduler)
                                  .observeOn(mainThreadScheduler)
                                  .subscribe(Actions.noOpAction1(),
                                             Actions.noOpAction1()));
    }

    private Single<Object> addRideEventCompletable(final LocationData locationData) {
        return addLocationUpdateEventUseCase.execute(locationData)
                                            .toSingleDefault(new Object());
    }

    private void initCompositeSubscription() {
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void deactivate() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
        compositeSubscription = null;
    }

    private void addSubscription(final Subscription subscription) {
        compositeSubscription.add(subscription);
    }
}
