package codecacao.babic.gpstracking.app.utils.connectivity;


import rx.Single;

public interface NetworkUtils {

    Single<Boolean> isConnectedToInternet();

    Single<NetworkData> getActiveNetworkData();
}