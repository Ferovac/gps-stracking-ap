package codecacao.babic.gpstracking.app.ui.onboarding;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.annimon.stream.Optional;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import codecacao.babic.gpstracking.app.R;
import codecacao.babic.gpstracking.app.base.BaseActivity;
import codecacao.babic.gpstracking.app.base.ScopedPresenter;
import codecacao.babic.gpstracking.app.injection.activity.ActivityComponent;
import codecacao.babic.gpstracking.app.utils.ui.ImageLoader;

public final class OnboardingActivity extends BaseActivity implements OnboardingContract.View {

    private static final int REQUEST_IMAGE_CAPTURE = 1000;
    private static final int REQUEST_PERMISSION_CODE = 2000;

    @BindView(R.id.activity_onboarding_first_name_edit_text)
    EditText firstNameEditText;

    @BindView(R.id.activity_onboarding_last_name_edit_text)
    EditText lastNameEditText;

    @BindView(R.id.activity_onboarding_age_edit_text)
    EditText ageEditText;

    @BindView(R.id.activity_onboarding_licence_plate_edit_text)
    EditText licencePlateEditText;

    @BindView(R.id.activity_onboarding_image)
    ImageView imageView;

    @Inject
    OnboardingContract.Presenter presenter;

    @Inject
    ImageLoader imageLoader;

    private boolean pendingImageCapture;

    private Optional<String> currentImagePath = Optional.empty();

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);
        bindViews();
        presenter.init();
    }

    private void bindViews() {
        ButterKnife.bind(this);
    }

    @Override
    protected void inject(final ActivityComponent component) {
        component.inject(this);
    }

    @Override
    protected ScopedPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showFieldsEmptyError() {
        Toast.makeText(this, R.string.onboarding_screen_fields_empty_message_text, Toast.LENGTH_SHORT)
             .show();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                currentImagePath.ifPresent(imageUrl -> imageLoader.loadImageCenterCrop(imageUrl, imageView));
            } else {
                currentImagePath = Optional.empty();
            }

            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_CODE) {
            if (permissions.length > 0 && permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (pendingImageCapture) {
                    dispatchTakePictureIntent();
                }
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @OnClick(R.id.activity_onboarding_create_user)
    void onCreateUserButtonClicked() {
        if (!currentImagePath.isPresent()) {
            Toast.makeText(this, "Please take a image", Toast.LENGTH_SHORT)
                 .show();
        }

        presenter.createUser(firstNameEditText.getText().toString(),
                             lastNameEditText.getText().toString(),
                             ageEditText.getText().toString(),
                             licencePlateEditText.getText().toString(),
                             currentImagePath.get());
    }

    @OnClick(R.id.activity_onboarding_capture_image_button)
    void onCaptureImageButtonClicked() {
        if (!hasPermission()) {
            requestStoragePermission();
            pendingImageCapture = true;
        } else {
            dispatchTakePictureIntent();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestStoragePermission() {
        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CODE);
    }

    private boolean hasPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void dispatchTakePictureIntent() {
        final Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            // Create the File where the photo should go
            final  File photoFile;
            try {
                photoFile = createImageFile();

            } catch (final IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(this, "Error occurred while taking a photo", Toast.LENGTH_SHORT).show();
                return;
            }

            // Continue only if the File was successfully created
            if (photoFile != null) {
                final Uri photoURI = FileProvider.getUriForFile(this,
                                                                getPackageName(),
                                                                photoFile);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile() throws IOException {
        final String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        final String imageFileName = "JPEG_" + timestamp + "_";

        final File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        final File image = File.createTempFile(imageFileName, ".jpg", storageDir);

        // Save a file: path for use with ACTION_VIEW intents
        currentImagePath = Optional.ofNullable(image.getAbsolutePath());

        return image;
    }
}
