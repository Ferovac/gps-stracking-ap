package codecacao.babic.gpstracking.app.ui.main;

import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import butterknife.ButterKnife;
import codecacao.babic.gpstracking.app.R;
import codecacao.babic.gpstracking.app.base.BaseActivity;
import codecacao.babic.gpstracking.app.base.ScopedPresenter;
import codecacao.babic.gpstracking.app.injection.activity.ActivityComponent;

public final class MainActivity extends BaseActivity implements MainContract.View {

    @Inject
    MainContract.Presenter presenter;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();

        if (savedInstanceState == null) {
            presenter.showContentScreen();
        }
    }

    @Override
    protected void inject(final ActivityComponent component) {
        component.inject(this);
    }

    private void bindViews() {
        ButterKnife.bind(this);
    }

    @Override
    protected ScopedPresenter getPresenter() {
        return presenter;
    }
}
