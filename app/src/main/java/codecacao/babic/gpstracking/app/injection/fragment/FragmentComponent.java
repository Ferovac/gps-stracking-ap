package codecacao.babic.gpstracking.app.injection.fragment;

import codecacao.babic.gpstracking.app.injection.activity.ActivityComponent;
import codecacao.babic.gpstracking.app.injection.fragment.module.FragmentModule;
import codecacao.babic.gpstracking.app.injection.fragment.module.FragmentPresenterModule;
import codecacao.babic.gpstracking.app.injection.fragment.module.FragmentUiModule;
import codecacao.babic.gpstracking.app.injection.scope.FragmentScope;
import dagger.Component;

@FragmentScope
@Component(
        dependencies = {
                ActivityComponent.class
        },
        modules = {
                FragmentModule.class,
                FragmentUiModule.class,
                FragmentPresenterModule.class
        }
)
public interface FragmentComponent extends FragmentComponentInjects,
                                           FragmentComponentExposes { }
