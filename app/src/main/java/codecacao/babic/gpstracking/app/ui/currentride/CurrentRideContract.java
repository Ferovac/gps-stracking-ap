package codecacao.babic.gpstracking.app.ui.currentride;

import codecacao.babic.gpstracking.app.base.BaseView;
import codecacao.babic.gpstracking.app.base.ScopedPresenter;

public final class CurrentRideContract {

    public CurrentRideContract() {

    }

    public interface Presenter extends ScopedPresenter {

        void init();

        void startRide(String startLocation, String endLocation, final int numberOfPassengers);

        void passengerPicked();

        void stopOver();

        void continueRide();

        void endRide();
    }

    public interface View extends BaseView {

        void render(CurrentRideViewModel viewModel);

        void showLocationsNotEnteredError();

        void showNoRideState();

        void showRideStartedState();

        void showPassengerPickedState();

        void showRideStoppedOverState();

        void showRideContinuedState();
    }
}
