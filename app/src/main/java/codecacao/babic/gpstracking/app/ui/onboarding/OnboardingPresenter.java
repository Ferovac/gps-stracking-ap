package codecacao.babic.gpstracking.app.ui.onboarding;

import javax.inject.Inject;

import codecacao.babic.gpstracking.app.base.BasePresenter;
import codecacao.babic.gpstracking.app.utils.Actions;
import codecacao.babic.gpstracking.domain.model.User;
import codecacao.babic.gpstracking.domain.usecase.IsUserCreatedUseCase;
import codecacao.babic.gpstracking.domain.usecase.SaveUserDataUseCase;
import codecacao.babic.gpstracking.domain.utils.StringUtils;
import rx.functions.Action1;

public final class OnboardingPresenter extends BasePresenter<OnboardingContract.View> implements OnboardingContract.Presenter {

    @Inject
    IsUserCreatedUseCase isUserCreatedUseCase;

    @Inject
    SaveUserDataUseCase saveUserDataUseCase;

    @Inject
    StringUtils stringUtils;

    public OnboardingPresenter(final OnboardingContract.View view) {
        super(view);
    }

    @Override
    public void init() {
        viewActionQueue.subscribeTo(isUserCreatedUseCase.execute()
                                                        .map(this::mapToIsUserCreatedViewAction)
                                                        .subscribeOn(backgroundScheduler),
                                    this::logError);
    }

    private Action1<OnboardingContract.View> mapToIsUserCreatedViewAction(final boolean isCreated) {
        if (isCreated) {
            return view -> router.showMainScreen();
        } else {
            return Actions.noOpAction1();
        }
    }

    @Override
    public void createUser(final String firstName, final String lastName, final String age, final String licencePlate, final String imageUrl) {
        if (allFieldsValid(firstName, lastName, age, licencePlate)) {
            createUserInternal(firstName, lastName, Integer.parseInt(age), licencePlate, imageUrl);
        } else {
            doIfViewNotNull(OnboardingContract.View::showFieldsEmptyError);
        }
    }

    private boolean allFieldsValid(final String firstName, final String lastName, final String age, final String licencePlate) {
        return !stringUtils.isEmpty(firstName) && !stringUtils.isEmpty(lastName) && isAgeProperInteger(age) && !stringUtils.isEmpty(licencePlate);
    }

    private boolean isAgeProperInteger(final String age) {
        try {
            Integer.parseInt(age);
            return true;

        } catch (final NumberFormatException e) {
            return false;
        }
    }

    private void createUserInternal(final String firstName, final String lastName, final int age, final String licencePlate, final String imageUrlAsString) {
        viewActionQueue.subscribeTo(saveUserDataUseCase.execute(new User(firstName, lastName, age, licencePlate, imageUrlAsString))
                                                       .subscribeOn(backgroundScheduler),
                                    view -> router.showMainScreen(),
                                    this::logError);
    }
}
