package codecacao.babic.gpstracking.app.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentManager;

import codecacao.babic.gpstracking.app.R;
import codecacao.babic.gpstracking.app.ui.main.ContentFragment;
import codecacao.babic.gpstracking.app.ui.main.MainActivity;
import codecacao.babic.gpstracking.app.ui.ridedetails.RideDetailsFragment;
import codecacao.babic.gpstracking.app.ui.settings.SettingsFragment;
import codecacao.babic.gpstracking.domain.utils.ListUtils;

public final class RouterImpl implements Router {

    public static final int CONTAINER_ID = R.id.activity_main_fragment_container;

    private final Activity activity;
    private final FragmentManager fragmentManager;
    private final ListUtils listUtils;

    public RouterImpl(final Activity activity, final FragmentManager fragmentManager, final ListUtils listUtils) {
        this.activity = activity;
        this.fragmentManager = fragmentManager;
        this.listUtils = listUtils;
    }

    @Override
    public void showMainScreen() {
        activity.startActivity(new Intent(activity, MainActivity.class));
        activity.finish();
    }

    @Override
    public void showContentScreen() {
        fragmentManager.beginTransaction()
                       .replace(CONTAINER_ID, ContentFragment.newInstance(), ContentFragment.TAG)
                       .commit();
    }

    @Override
    public void showRideDetails(final int rideId) {
        fragmentManager.beginTransaction()
                       .replace(CONTAINER_ID, RideDetailsFragment.newInstance(rideId), RideDetailsFragment.TAG)
                       .addToBackStack(null)
                       .commit();
    }

    @Override
    public void showSettingsScreen() {
        fragmentManager.beginTransaction()
                       .replace(CONTAINER_ID, SettingsFragment.newInstance(), SettingsFragment.TAG)
                       .addToBackStack(null)
                       .commit();
    }

    @Override
    public boolean showPageInExternalBrowser(final String url) {
        final Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));

        if (canIntentBeResolved(intent)) {
            activity.startActivity(intent);
            return true;
        }

        return false;
    }

    private boolean canIntentBeResolved(final Intent intent) {
        return !listUtils.isEmpty(activity.getPackageManager().queryIntentActivities(intent, 0));
    }

    public void goBack() {
        if (fragmentManager.getBackStackEntryCount() == 0) {
            activity.finish();
        } else {
            fragmentManager.popBackStack();
        }
    }
}
