package codecacao.babic.gpstracking.app.injection.user.module;

import codecacao.babic.gpstracking.app.injection.scope.UserScope;
import codecacao.babic.gpstracking.data.dao.RideDao;
import codecacao.babic.gpstracking.data.dao.RideEventDao;
import codecacao.babic.gpstracking.data.network.client.RideClient;
import codecacao.babic.gpstracking.data.prefs.CurrentRidePreferences;
import codecacao.babic.gpstracking.data.prefs.UserSharedPrefs;
import codecacao.babic.gpstracking.data.repository.RideRepositoryImpl;
import codecacao.babic.gpstracking.data.repository.RideRepositorySchedulerPrefs;
import codecacao.babic.gpstracking.data.repository.UserRepositoryImpl;
import codecacao.babic.gpstracking.domain.repository.RideIdGenerator;
import codecacao.babic.gpstracking.domain.repository.RideRepository;
import codecacao.babic.gpstracking.domain.repository.UserRepository;
import dagger.Module;
import dagger.Provides;

@Module
public final class UserRepositoryModule {

    @Provides
    @UserScope
    RideRepository provideRideRepository(final CurrentRidePreferences currentRidePreferences, final RideClient rideClient, final RideDao rideDao, final RideEventDao rideEventDao,
                                         final RideIdGenerator rideIdGenerator, final RideRepositorySchedulerPrefs rideRepositorySchedulerPrefs) {

        return new RideRepositoryImpl(currentRidePreferences, rideClient, rideDao, rideEventDao, rideIdGenerator, rideRepositorySchedulerPrefs);
    }

    @Provides
    @UserScope
    UserRepository provideUserRepository(final UserSharedPrefs userSharedPrefs) {
        return new UserRepositoryImpl(userSharedPrefs);
    }

    public interface Exposes {

        RideRepository rideRepository();

        UserRepository userRepository();
    }
}
