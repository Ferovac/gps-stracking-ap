package codecacao.babic.gpstracking.app.ui.currentride;

import com.annimon.stream.Optional;

import javax.inject.Inject;

import codecacao.babic.gpstracking.app.base.BasePresenter;
import codecacao.babic.gpstracking.app.ui.notifier.RideEndNotifier;
import codecacao.babic.gpstracking.app.utils.Actions;
import codecacao.babic.gpstracking.domain.model.LocationData;
import codecacao.babic.gpstracking.domain.model.Ride;
import codecacao.babic.gpstracking.domain.model.RideEvent;
import codecacao.babic.gpstracking.domain.model.RideEventType;
import codecacao.babic.gpstracking.domain.usecase.AddRideEventUseCase;
import codecacao.babic.gpstracking.domain.usecase.EndRideUseCase;
import codecacao.babic.gpstracking.domain.usecase.GetCurrentLocationUseCase;
import codecacao.babic.gpstracking.domain.usecase.GetCurrentRideUseCase;
import codecacao.babic.gpstracking.domain.usecase.GetLastCurrentRideEventUseCase;
import codecacao.babic.gpstracking.domain.usecase.StartRideUseCase;
import codecacao.babic.gpstracking.domain.utils.NotificationDisplay;
import codecacao.babic.gpstracking.domain.utils.StringUtils;
import rx.Completable;
import rx.Single;
import rx.functions.Action1;

public final class CurrentRidePresenter extends BasePresenter<CurrentRideContract.View> implements CurrentRideContract.Presenter {

    @Inject
    GetCurrentRideUseCase getCurrentRideUseCase;

    @Inject
    GetLastCurrentRideEventUseCase getLastCurrentRideEventUseCase;

    @Inject
    GetCurrentLocationUseCase getCurrentLocationUseCase;

    @Inject
    StartRideUseCase startRideUseCase;

    @Inject
    AddRideEventUseCase addRideEventUseCase;

    @Inject
    EndRideUseCase endRideUseCase;

    @Inject
    NotificationDisplay notificationDisplay;

    @Inject
    RideEndNotifier rideEndNotifier;

    @Inject
    StringUtils stringUtils;

    public CurrentRidePresenter(final CurrentRideContract.View view) {
        super(view);
    }

    @Override
    public void init() {
        viewActionQueue.subscribeTo(Single.zip(getCurrentRideUseCase.execute(),
                                               getLastCurrentRideEventUseCase.execute(),
                                               ZipResult::new)
                                          .map(this::mapToViewActionForCurrentRide)
                                          .subscribeOn(backgroundScheduler),
                                    this::logError);
    }

    private Action1<CurrentRideContract.View> mapToViewActionForCurrentRide(final ZipResult zipResult) {
        if (zipResult.ride.isPresent() && zipResult.rideEvent.isPresent()) {

            final Ride ride = zipResult.ride.get();
            final CurrentRideViewModel viewModel = new CurrentRideViewModel(ride.startLocation, ride.endLocation);

            final RideEvent rideEvent = zipResult.rideEvent.get();
            if (rideEvent.eventType == RideEventType.STARTED) {
                return view -> {
                    view.render(viewModel);
                    view.showRideStartedState();
                };

            } else if (rideEvent.eventType == RideEventType.PASSENGER_PICKED) {
                return view -> {
                    view.render(viewModel);
                    view.showPassengerPickedState();
                };

            } else if (rideEvent.eventType == RideEventType.STOP_OVER) {
                return view -> {
                    view.render(viewModel);
                    view.showRideStoppedOverState();
                };

            } else if (rideEvent.eventType == RideEventType.CONTINUE) {
                return view -> {
                    view.render(viewModel);
                    view.showRideContinuedState();
                };

            } else {
                // This should not happen, this indicates error.
                return Actions.noOpAction1();
            }

        } else {
            return CurrentRideContract.View::showNoRideState;
        }
    }

    @Override
    public void startRide(final String startLocation, final String endLocation, final int numberOfPassengers) {
        if (!stringUtils.isEmpty(startLocation) && !stringUtils.isEmpty(endLocation)) {
            startRideInternal(startLocation, endLocation, numberOfPassengers);
        } else {
            doIfViewNotNull(CurrentRideContract.View::showLocationsNotEnteredError);
        }
    }

    private void startRideInternal(final String startLocation, final String endLocation, final int numberOfPassengers) {
        viewActionQueue.subscribeTo(getCurrentLocationUseCase.execute()
                                                             .flatMapCompletable(locationData -> getStartRideCompletable(startLocation, endLocation, numberOfPassengers,
                                                                                                                         locationData))
                                                             .andThen(notificationDisplay.showRideOngoingNotification(startLocation, endLocation))
                                                             .subscribeOn(backgroundScheduler),
                                    CurrentRideContract.View::showRideStartedState,
                                    this::onStartRideError);
    }

    private Completable getStartRideCompletable(final String startLocation, final String endLocation, final int numberOfPassengers, final LocationData locationData) {
        return startRideUseCase.execute(new StartRideUseCase.Request(startLocation, endLocation, numberOfPassengers, locationData));
    }

    private void onStartRideError(final Throwable throwable) {
        logError(throwable);
    }

    @Override
    public void passengerPicked() {
        viewActionQueue.subscribeTo(getCurrentLocationUseCase.execute()
                                                             .flatMapCompletable(this::passengerPickedEventCompletable)
                                                             .subscribeOn(backgroundScheduler),
                                    CurrentRideContract.View::showPassengerPickedState,
                                    this::onPassengerPickedError);
    }

    private Completable passengerPickedEventCompletable(final LocationData locationData) {
        return getCompletableForRideEvent(locationData, RideEventType.PASSENGER_PICKED);
    }

    private void onPassengerPickedError(final Throwable throwable) {
        logError(throwable);
    }

    @Override
    public void stopOver() {
        viewActionQueue.subscribeTo(getCurrentLocationUseCase.execute()
                                                             .flatMapCompletable(this::stopOverEventCompletable)
                                                             .subscribeOn(backgroundScheduler),
                                    CurrentRideContract.View::showRideStoppedOverState,
                                    this::onStopOverError);
    }

    private Completable stopOverEventCompletable(final LocationData locationData) {
        return getCompletableForRideEvent(locationData, RideEventType.STOP_OVER);
    }

    private void onStopOverError(final Throwable throwable) {
        logError(throwable);
    }

    @Override
    public void continueRide() {
        viewActionQueue.subscribeTo(getCurrentLocationUseCase.execute()
                                                             .flatMapCompletable(this::continueRideEventCompletable)
                                                             .subscribeOn(backgroundScheduler),
                                    CurrentRideContract.View::showRideContinuedState,
                                    this::continueRideError);
    }

    private Completable continueRideEventCompletable(final LocationData locationData) {
        return getCompletableForRideEvent(locationData, RideEventType.CONTINUE);
    }

    private Completable getCompletableForRideEvent(final LocationData locationData, final RideEventType rideEventType) {
        return addRideEventUseCase.execute(new AddRideEventUseCase.Request(locationData, rideEventType));
    }

    private void continueRideError(final Throwable throwable) {
        logError(throwable);
    }

    @Override
    public void endRide() {
        viewActionQueue.subscribeTo(getCurrentLocationUseCase.execute()
                                                             .flatMapCompletable(endRideUseCase::execute)
                                                             .andThen(notificationDisplay.stopDisplayingNotification())
                                                             .subscribeOn(backgroundScheduler),
                                    view -> {
                                        rideEndNotifier.notifyRideEnded();
                                        view.render(CurrentRideViewModel.EMPTY);
                                        view.showNoRideState();
                                    },
                                    this::onEndRideError);
    }

    private void onEndRideError(final Throwable throwable) {
        logError(throwable);
    }

    private static final class ZipResult {

        private final Optional<Ride> ride;
        private final Optional<RideEvent> rideEvent;

        private ZipResult(final Optional<Ride> ride, final Optional<RideEvent> rideEvent) {
            this.ride = ride;
            this.rideEvent = rideEvent;
        }
    }
}
