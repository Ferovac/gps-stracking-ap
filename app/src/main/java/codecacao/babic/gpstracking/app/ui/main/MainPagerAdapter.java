package codecacao.babic.gpstracking.app.ui.main;

import android.content.res.Resources;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.Arrays;
import java.util.List;

import codecacao.babic.gpstracking.app.R;
import codecacao.babic.gpstracking.app.ui.currentride.CurrentRideFragment;
import codecacao.babic.gpstracking.app.ui.previousrides.PreviousRideFragment;

public final class MainPagerAdapter extends FragmentPagerAdapter {

    private interface FragmentFactory {

        Fragment create();
    }

    private final Resources resources;

    private static final List<ViewPagerPage> VIEW_PAGER_PAGES = Arrays.asList(new ViewPagerPage(R.string.main_screen_view_pager_current_ride_page_title,
                                                                                                CurrentRideFragment::newInstance),
                                                                              new ViewPagerPage(R.string.main_screen_view_pager_previous_rides_page_title,
                                                                                                PreviousRideFragment::newInstance));

    public MainPagerAdapter(final FragmentManager fm, final Resources resources) {
        super(fm);
        this.resources = resources;
    }

    @Override
    public Fragment getItem(final int index) {
        return VIEW_PAGER_PAGES.get(index).fragmentFactory.create();
    }

    @Override
    public int getCount() {
        return VIEW_PAGER_PAGES.size();
    }

    @Override
    public CharSequence getPageTitle(final int index) {
        return resources.getString(VIEW_PAGER_PAGES.get(index).titleResource);
    }

    private static final class ViewPagerPage {

        @StringRes
        final int titleResource;
        final FragmentFactory fragmentFactory;

        private ViewPagerPage(@StringRes final int titleResource, final FragmentFactory fragmentFactory) {
            this.titleResource = titleResource;
            this.fragmentFactory = fragmentFactory;
        }
    }
}
