package codecacao.babic.gpstracking.app.injection.application.module;

import android.content.Context;

import com.google.gson.Gson;

import javax.inject.Singleton;

import codecacao.babic.gpstracking.app.injection.ForApplication;
import codecacao.babic.gpstracking.data.dao.DaoFactory;
import codecacao.babic.gpstracking.data.dao.RideDao;
import codecacao.babic.gpstracking.data.dao.RideEventDao;
import codecacao.babic.gpstracking.data.prefs.CurrentRidePreferences;
import codecacao.babic.gpstracking.data.prefs.CurrentRidePreferencesImpl;
import codecacao.babic.gpstracking.data.prefs.UserSharedPrefs;
import codecacao.babic.gpstracking.data.prefs.UserSharedPrefsImpl;
import codecacao.babic.gpstracking.data.repository.RideIdGeneratorImpl;
import codecacao.babic.gpstracking.data.repository.RideRepositorySchedulerPrefs;
import codecacao.babic.gpstracking.data.repository.RideRepositorySchedulerPrefsImpl;
import codecacao.babic.gpstracking.device.time.CurrentTimeProviderImpl;
import codecacao.babic.gpstracking.domain.repository.RideIdGenerator;
import codecacao.babic.gpstracking.domain.time.CurrentTimeProvider;
import codecacao.babic.gpstracking.domain.utils.StringUtils;
import dagger.Module;
import dagger.Provides;

@Module
public final class DataModule {

    @Provides
    Gson provideGson() {
        return new Gson();
    }

    @Provides
    @Singleton
    CurrentTimeProvider provideCurrentTimeProvider() {
        return new CurrentTimeProviderImpl();
    }

    @Provides
    @Singleton
    DaoFactory provideDaoFactory(@ForApplication final Context context, final Gson gson) {
        return DaoFactory.create(context, gson);
    }

    @Provides
    @Singleton
    RideDao provideRideDao(final DaoFactory daoFactory) {
        return daoFactory.createRideDao();
    }

    @Provides
    @Singleton
    RideEventDao provideRideEventDao(final DaoFactory daoFactory) {
        return daoFactory.createRideEventDao();
    }

    @Provides
    @Singleton
    CurrentRidePreferences provideCurrentRidePreferences(@ForApplication final Context context) {
        return CurrentRidePreferencesImpl.newInstance(context);
    }

    @Provides
    @Singleton
    UserSharedPrefs provideUserSharedPreferences(@ForApplication final Context context) {
        return UserSharedPrefsImpl.create(context);
    }

    @Provides
    @Singleton
    RideIdGenerator provideRideIdGenerator(@ForApplication final Context context) {
        return RideIdGeneratorImpl.create(context);
    }

    @Provides
    @Singleton
    RideRepositorySchedulerPrefs provideRideRepositorySchedulerPrefs(@ForApplication final Context context, final StringUtils stringUtils) {
        return RideRepositorySchedulerPrefsImpl.create(context, stringUtils);
    }

    public interface Exposes {

        CurrentTimeProvider currentTimeProvider();

        RideDao rideDao();

        RideEventDao rideEventDao();

        CurrentRidePreferences currentRidePreferences();

        UserSharedPrefs userSharedPrefs();

        RideIdGenerator rideIdGenerator();

        RideRepositorySchedulerPrefs rideRepositorySchedulerPrefs();
    }
}
