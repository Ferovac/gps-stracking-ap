package codecacao.babic.gpstracking.app.injection.user;

import codecacao.babic.gpstracking.app.application.GpsTrackingApplication;
import codecacao.babic.gpstracking.app.maintenance.ForegroundGuardService;
import codecacao.babic.gpstracking.app.maintenance.ForegroundServicePresenter;

public interface UserComponentInjects {

    void inject(ForegroundGuardService serviceGuard);

    void inject(ForegroundServicePresenter presenter);

    void inject(GpsTrackingApplication application);
}
