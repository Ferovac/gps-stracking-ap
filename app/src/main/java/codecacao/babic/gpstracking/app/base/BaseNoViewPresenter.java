package codecacao.babic.gpstracking.app.base;

public abstract class BaseNoViewPresenter extends BasePresenter<BaseView> {

    public BaseNoViewPresenter() {
        super(new BaseView() { });
    }
}
