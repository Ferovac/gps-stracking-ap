package codecacao.babic.gpstracking.app.ui.previousrides;

public final class RideViewModel {

    public final int id;
    public final String startLocation;
    public final String endLocation;
    public final int numberOfPassengers;

    public RideViewModel(final int id, final String startLocation, final String endLocation, final int numberOfPassengers) {
        this.id = id;
        this.startLocation = startLocation;
        this.endLocation = endLocation;
        this.numberOfPassengers = numberOfPassengers;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final RideViewModel that = (RideViewModel) o;

        if (id != that.id) {
            return false;
        }
        if (numberOfPassengers != that.numberOfPassengers) {
            return false;
        }
        if (startLocation != null ? !startLocation.equals(that.startLocation) : that.startLocation != null) {
            return false;
        }
        return endLocation != null ? endLocation.equals(that.endLocation) : that.endLocation == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (startLocation != null ? startLocation.hashCode() : 0);
        result = 31 * result + (endLocation != null ? endLocation.hashCode() : 0);
        result = 31 * result + numberOfPassengers;
        return result;
    }

    @Override
    public String toString() {
        return "RideViewModel{" +
                "id=" + id +
                ", startLocation='" + startLocation + '\'' +
                ", endLocation='" + endLocation + '\'' +
                ", numberOfPassengers=" + numberOfPassengers +
                '}';
    }
}
