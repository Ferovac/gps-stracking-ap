package codecacao.babic.gpstracking.app.application;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;

import javax.inject.Inject;
import javax.inject.Named;

import codecacao.babic.gpstracking.app.BuildConfig;
import codecacao.babic.gpstracking.app.injection.ComponentFactory;
import codecacao.babic.gpstracking.app.injection.application.ApplicationComponent;
import codecacao.babic.gpstracking.app.injection.application.module.ThreadingModule;
import codecacao.babic.gpstracking.app.injection.user.UserComponent;
import codecacao.babic.gpstracking.app.utils.Actions;
import codecacao.babic.gpstracking.app.utils.connectivity.ConnectivityReceiver;
import codecacao.babic.gpstracking.data.network.client.MockRideClient;
import codecacao.babic.gpstracking.domain.delegate.UserComponentDelegate;
import rx.Scheduler;

public final class GpsTrackingApplication extends Application implements UserComponentDelegate {

    private ApplicationComponent applicationComponent;
    private UserComponent userComponent;

    @Inject
    ConnectivityReceiver connectivityReceiver;

    @Inject
    @Named(ThreadingModule.BACKGROUND_SCHEDULER)
    Scheduler backgroundScheduler;

    @Inject
    @Named(ThreadingModule.MAIN_SCHEDULER)
    Scheduler mainThreadScheduler;

    @Inject
    MockRideClient mockRideClient;

    public static GpsTrackingApplication from(final Context context) {
        return (GpsTrackingApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initApplicationComponent();
        initUserComponent();
        injectMe();
        initStetho();

        checkConnectivity();
        observeConnectivity();
    }

    private void checkConnectivity() {
        connectivityReceiver.isConnected()
                            .subscribeOn(backgroundScheduler)
                            .observeOn(mainThreadScheduler)
                            .subscribe(mockRideClient::setConnected,
                                       Actions.noOpAction1());
    }

    private void observeConnectivity() {
        connectivityReceiver.getConnectivityStatus()
                            .subscribeOn(backgroundScheduler)
                            .observeOn(mainThreadScheduler)
                            .subscribe(mockRideClient::setConnected);
    }

    private void initStetho() {
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this);
        }
    }

    private void initApplicationComponent() {
        applicationComponent = ComponentFactory.createApplicationComponent(this);
    }

    private void injectMe() {
        userComponent.inject(this);
    }

    public UserComponent getUserComponent() {
        return userComponent;
    }

    @Override
    public void initUserComponent() {
        userComponent = ComponentFactory.createUserComponent(applicationComponent);
    }
}
