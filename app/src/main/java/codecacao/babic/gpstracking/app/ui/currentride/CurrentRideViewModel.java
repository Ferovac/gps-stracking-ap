package codecacao.babic.gpstracking.app.ui.currentride;

public final class CurrentRideViewModel {

    public static final CurrentRideViewModel EMPTY = new CurrentRideViewModel("", "");

    public final String startAddress;
    public final String endAddress;

    public CurrentRideViewModel(final String startAddress, final String endAddress) {
        this.startAddress = startAddress;
        this.endAddress = endAddress;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final CurrentRideViewModel that = (CurrentRideViewModel) o;

        if (startAddress != null ? !startAddress.equals(that.startAddress) : that.startAddress != null) {
            return false;
        }
        return endAddress != null ? endAddress.equals(that.endAddress) : that.endAddress == null;
    }

    @Override
    public int hashCode() {
        int result = startAddress != null ? startAddress.hashCode() : 0;
        result = 31 * result + (endAddress != null ? endAddress.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CurrentRideViewModel{" +
                "startAddress='" + startAddress + '\'' +
                ", endAddress='" + endAddress + '\'' +
                '}';
    }
}
