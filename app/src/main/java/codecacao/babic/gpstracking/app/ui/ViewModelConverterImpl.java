package codecacao.babic.gpstracking.app.ui;

import com.annimon.stream.Stream;

import java.util.List;

import codecacao.babic.gpstracking.app.ui.previousrides.RideViewModel;
import codecacao.babic.gpstracking.app.ui.ridedetails.RideEventViewModel;
import codecacao.babic.gpstracking.app.ui.settings.UserViewModel;
import codecacao.babic.gpstracking.domain.model.Ride;
import codecacao.babic.gpstracking.domain.model.RideEvent;
import codecacao.babic.gpstracking.domain.model.User;
import codecacao.babic.gpstracking.domain.utils.DateUtils;
import codecacao.babic.gpstracking.domain.utils.StringUtils;

public final class ViewModelConverterImpl implements ViewModelConverter {

    private final DateUtils dateUtils;
    private final StringUtils stringUtils;

    public ViewModelConverterImpl(final DateUtils dateUtils, final StringUtils stringUtils) {
        this.dateUtils = dateUtils;
        this.stringUtils = stringUtils;
    }

    @Override
    public List<RideViewModel> toRideViewModels(final List<Ride> rides) {
        return Stream.of(rides)
                     .map(this::toRideViewModel)
                     .toList();
    }

    private RideViewModel toRideViewModel(final Ride ride) {
        return new RideViewModel(ride.id, ride.startLocation, ride.endLocation, ride.numberOfPassengers);
    }

    @Override
    public List<RideEventViewModel> toRideEventViewModels(final List<RideEvent> rideEvents) {
        return Stream.of(rideEvents)
                     .map(this::toRideEventViewModel)
                     .toList();
    }

    private RideEventViewModel toRideEventViewModel(final RideEvent rideEvent) {
        return new RideEventViewModel(rideEvent.eventType.name(), dateUtils.convertToSimpleUserReadableDateFormatWithSeconds(rideEvent.timestamp));
    }

    @Override
    public UserViewModel toUserViewModel(final User user) {
        return new UserViewModel(user.firstName, user.lastName, String.valueOf(user.age), user.licencePlate, user.imageUrl);
    }
}
