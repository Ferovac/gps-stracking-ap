package codecacao.babic.gpstracking.app.ui.currentride;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import codecacao.babic.gpstracking.app.R;
import codecacao.babic.gpstracking.app.base.BaseFragment;
import codecacao.babic.gpstracking.app.base.ScopedPresenter;
import codecacao.babic.gpstracking.app.injection.fragment.FragmentComponent;

public final class CurrentRideFragment extends BaseFragment implements CurrentRideContract.View {

    private static final RideViewState NO_RIDE_STATE = new RideViewState(true, true, false, false, false, false);
    private static final RideViewState RIDE_STARTED_STATE = new RideViewState(false, false, true, false, false, false);
    private static final RideViewState RIDE_PASSENGER_PICKED_STATE = new RideViewState(false, false, false, true, false, true);
    private static final RideViewState RIDE_STOP_OVER_STATE = new RideViewState(false, false, false, false, true, false);
    private static final RideViewState RIDE_CONTINUED_STATE = new RideViewState(false, false, false, true, false, true);

    @BindView(R.id.fragment_current_ride_start_location_edit_text)
    EditText startLocationEditText;

    @BindView(R.id.fragment_current_ride_end_location_edit_text)
    EditText endLocationEditText;

    @BindView(R.id.fragment_current_ride_start_ride_button)
    Button startRideButton;

    @BindView(R.id.fragment_current_ride_passenger_picked_button)
    Button passengerPickedButton;

    @BindView(R.id.fragment_current_ride_stop_over_button)
    Button stopOverButton;

    @BindView(R.id.fragment_current_ride_continue_ride_button)
    Button continueRideButton;

    @BindView(R.id.fragment_current_ride_end_ride_button)
    Button endRideButton;

    @BindView(R.id.fragment_current_ride_spinner_view)
    Spinner spinner;

    @Inject
    CurrentRideContract.Presenter presenter;

    public static CurrentRideFragment newInstance() {
        return new CurrentRideFragment();
    }

    @Override
    protected void inject(final FragmentComponent component) {
        component.inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_current_ride;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initAdapter();
        presenter.init();
    }

    private void initAdapter() {
        spinner.setAdapter(new ArrayAdapter<>(getContext(),
                                              android.R.layout.simple_spinner_item,
                                              resources.getStringArray(R.array.current_ride_number_of_passengers_string_array)));
    }

    @Override
    public ScopedPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showLocationsNotEnteredError() {
        showShortToast(R.string.current_ride_screen_locations_not_correct_error_text);
    }

    @Override
    public void render(final CurrentRideViewModel viewModel) {
        startLocationEditText.setText(viewModel.startAddress);
        endLocationEditText.setText(viewModel.endAddress);
    }

    @Override
    public void showNoRideState() {
        populateViewState(NO_RIDE_STATE);
    }

    @Override
    public void showRideStartedState() {
        populateViewState(RIDE_STARTED_STATE);
    }

    @Override
    public void showPassengerPickedState() {
        populateViewState(RIDE_PASSENGER_PICKED_STATE);
    }

    @Override
    public void showRideStoppedOverState() {
        populateViewState(RIDE_STOP_OVER_STATE);
    }

    @Override
    public void showRideContinuedState() {
        populateViewState(RIDE_CONTINUED_STATE);
    }

    private void populateViewState(final RideViewState viewState) {
        startLocationEditText.setEnabled(viewState.editTextsEnabled);
        endLocationEditText.setEnabled(viewState.editTextsEnabled);

        startRideButton.setVisibility(viewState.startRideButtonEnabled ? View.VISIBLE : View.GONE);
        passengerPickedButton.setVisibility(viewState.passengerPickedButtonEnabled ? View.VISIBLE : View.GONE);
        stopOverButton.setVisibility(viewState.stopOverButtonEnabled ? View.VISIBLE : View.GONE);
        continueRideButton.setVisibility(viewState.continueRideButtonEnabled ? View.VISIBLE : View.GONE);
        endRideButton.setVisibility(viewState.endRideButtonEnabled ? View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.fragment_current_ride_start_ride_button)
    void onStartRideButtonClicked() {
        presenter.startRide(startLocationEditText.getText().toString(),
                            endLocationEditText.getText().toString(),
                            Integer.parseInt((String)spinner.getSelectedItem()));
    }

    @OnClick(R.id.fragment_current_ride_passenger_picked_button)
    void onPassengerPickedButtonClicked() {
        presenter.passengerPicked();
    }

    @OnClick(R.id.fragment_current_ride_stop_over_button)
    void onStopOverButtonClicked() {
        presenter.stopOver();
    }

    @OnClick(R.id.fragment_current_ride_continue_ride_button)
    void onContinueRideButtonClicked() {
        presenter.continueRide();
    }

    @OnClick(R.id.fragment_current_ride_end_ride_button)
    void onEndRideButtonClicked() {
        presenter.endRide();
    }

    private static final class RideViewState {

        private final boolean editTextsEnabled;
        private final boolean startRideButtonEnabled;
        private final boolean passengerPickedButtonEnabled;
        private final boolean stopOverButtonEnabled;
        private final boolean continueRideButtonEnabled;
        private final boolean endRideButtonEnabled;

        private RideViewState(final boolean editTextsEnabled, final boolean startRideButtonEnabled, final boolean passengerPickedButtonEnabled, final boolean stopOverButtonEnabled,
                              final boolean continueRideButtonEnabled, final boolean endRideButtonEnabled) {
            this.editTextsEnabled = editTextsEnabled;
            this.startRideButtonEnabled = startRideButtonEnabled;
            this.passengerPickedButtonEnabled = passengerPickedButtonEnabled;
            this.stopOverButtonEnabled = stopOverButtonEnabled;
            this.continueRideButtonEnabled = continueRideButtonEnabled;
            this.endRideButtonEnabled = endRideButtonEnabled;
        }
    }
}
