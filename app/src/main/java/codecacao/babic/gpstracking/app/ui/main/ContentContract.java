package codecacao.babic.gpstracking.app.ui.main;

import codecacao.babic.gpstracking.app.base.BaseView;
import codecacao.babic.gpstracking.app.base.ScopedPresenter;

public final class ContentContract {

    public interface Presenter extends ScopedPresenter {

        void showSettingsScreen();
    }

    public interface View extends BaseView {

    }
}
