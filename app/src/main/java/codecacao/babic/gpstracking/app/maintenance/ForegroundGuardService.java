package codecacao.babic.gpstracking.app.maintenance;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import javax.inject.Inject;

import codecacao.babic.gpstracking.app.application.GpsTrackingApplication;
import codecacao.babic.gpstracking.app.notification.NotificationFactory;

public final class ForegroundGuardService extends Service {

    private static final int NOTIFICATION_ID = 200;

    @Inject
    NotificationFactory notificationFactory;

    @Inject
    ForegroundServiceContract.Presenter presenter;

    @Override
    public void onCreate() {
        super.onCreate();
        injectMe();
        presenter.activate();
    }

    private void injectMe() {
        GpsTrackingApplication.from(this)
                              .getUserComponent()
                              .inject(this);
    }

    @Override
    public int onStartCommand(final Intent intent, final int flags, final int startId) {
        startForeground(NOTIFICATION_ID, notificationFactory.buildNotification());
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(final Intent intent) {
        return new BinderImpl();
    }

    @Override
    public boolean onUnbind(final Intent intent) {
        presenter.deactivate();
        stopForeground(true);
        return super.onUnbind(intent);
    }

    private static final class BinderImpl extends Binder {

    }
}
