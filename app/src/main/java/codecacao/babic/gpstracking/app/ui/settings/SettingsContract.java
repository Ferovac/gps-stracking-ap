package codecacao.babic.gpstracking.app.ui.settings;

import codecacao.babic.gpstracking.app.base.BaseView;
import codecacao.babic.gpstracking.app.base.ScopedPresenter;

public final class SettingsContract {

    private SettingsContract() {

    }

    public interface Presenter extends ScopedPresenter {

        void init();
    }

    public interface View extends BaseView {

        void render(SettingsScreenViewModel viewModel);
    }
}
