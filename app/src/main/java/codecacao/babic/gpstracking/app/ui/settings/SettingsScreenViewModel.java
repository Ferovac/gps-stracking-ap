package codecacao.babic.gpstracking.app.ui.settings;

public final class SettingsScreenViewModel {

    final UserViewModel userViewModel;

    public SettingsScreenViewModel(final UserViewModel userViewModel) {
        this.userViewModel = userViewModel;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final SettingsScreenViewModel that = (SettingsScreenViewModel) o;

        return userViewModel != null ? userViewModel.equals(that.userViewModel) : that.userViewModel == null;
    }

    @Override
    public int hashCode() {
        return userViewModel != null ? userViewModel.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "SettingsScreenViewModel{" +
                "userViewModel=" + userViewModel +
                '}';
    }
}
