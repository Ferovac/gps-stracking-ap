package codecacao.babic.gpstracking.app.injection.fragment.module;

import android.support.v4.app.FragmentManager;

import codecacao.babic.gpstracking.app.injection.fragment.DaggerFragment;
import codecacao.babic.gpstracking.app.injection.scope.FragmentScope;
import dagger.Module;
import dagger.Provides;

@Module
public final class FragmentModule {

    private final DaggerFragment fragment;

    public FragmentModule(final DaggerFragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    @FragmentScope
    FragmentManager provideFragmentManager() {
        return fragment.getChildFragmentManager();
    }
}
