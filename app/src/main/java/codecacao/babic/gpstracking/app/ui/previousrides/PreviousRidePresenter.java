package codecacao.babic.gpstracking.app.ui.previousrides;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import codecacao.babic.gpstracking.app.base.BasePresenter;
import codecacao.babic.gpstracking.app.ui.ViewModelConverter;
import codecacao.babic.gpstracking.app.ui.notifier.RideEndNotifier;
import codecacao.babic.gpstracking.app.utils.Actions;
import codecacao.babic.gpstracking.domain.usecase.GetPreviousRidesUseCase;
import rx.Single;
import rx.functions.Action1;

public final class PreviousRidePresenter extends BasePresenter<PreviousRidesContract.View> implements PreviousRidesContract.Presenter {

    @Inject
    GetPreviousRidesUseCase getPreviousRidesUseCase;

    @Inject
    ViewModelConverter viewModelConverter;

    @Inject
    RideEndNotifier rideEndNotifier;

    public PreviousRidePresenter(final PreviousRidesContract.View view) {
        super(view);
    }

    @Override
    public void init() {
        subscribeToEndRideEvents();
        getPreviousRideData();
    }

    private void subscribeToEndRideEvents() {
        viewActionQueue.subscribeTo(rideEndNotifier.getNotifierObservable()
                                                   .flatMapSingle(endEvent -> getPreviousRideAsViewAction())
                                                   .subscribeOn(backgroundScheduler),
                                    Actions.noOpAction1(),
                                    this::processGetPreviousRidesError);
    }

    private void getPreviousRideData() {
        viewActionQueue.subscribeTo(getPreviousRideAsViewAction().subscribeOn(backgroundScheduler),
                                    this::processGetPreviousRidesError);
    }

    private Single<Action1<PreviousRidesContract.View>> getPreviousRideAsViewAction() {
        return getPreviousRidesUseCase.execute()
                                      .map(viewModelConverter::toRideViewModels)
                                      .map(this::toViewAction);
    }

    private Action1<PreviousRidesContract.View> toViewAction(final List<RideViewModel> viewModels) {
        return view -> view.render(new RideScreenViewModel(viewModels, viewModels.isEmpty(), false));
    }

    private void processGetPreviousRidesError(final Throwable throwable) {
        logError(throwable);
        doIfViewNotNull(view -> {
            view.render(new RideScreenViewModel(Collections.emptyList(), false, true));
            view.showRideFetchErrorPrompt();
        });
    }

    @Override
    public void showRideDetails(final int rideId) {
        router.showRideDetails(rideId);
    }
}
