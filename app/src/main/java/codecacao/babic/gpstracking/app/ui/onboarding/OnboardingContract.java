package codecacao.babic.gpstracking.app.ui.onboarding;

import codecacao.babic.gpstracking.app.base.BaseView;
import codecacao.babic.gpstracking.app.base.ScopedPresenter;

public final class OnboardingContract {

    private OnboardingContract() {

    }

    public interface Presenter extends ScopedPresenter {

        void init();

        void createUser(String firstName, String lastName, String age, String licencePlate, String imageUrl);
    }

    public interface View extends BaseView {

        void showFieldsEmptyError();
    }
}
