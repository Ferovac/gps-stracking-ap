package codecacao.babic.gpstracking.app.injection.application;

import javax.inject.Singleton;

import codecacao.babic.gpstracking.app.injection.application.module.AndroidSystemModule;
import codecacao.babic.gpstracking.app.injection.application.module.ApiModule;
import codecacao.babic.gpstracking.app.injection.application.module.ApplicationModule;
import codecacao.babic.gpstracking.app.injection.application.module.ConnectivityModule;
import codecacao.babic.gpstracking.app.injection.application.module.DataModule;
import codecacao.babic.gpstracking.app.injection.application.module.RepositoryModule;
import codecacao.babic.gpstracking.app.injection.application.module.ThreadingModule;
import codecacao.babic.gpstracking.app.injection.application.module.UseCaseModule;
import codecacao.babic.gpstracking.app.injection.application.module.UtilsModule;
import codecacao.babic.gpstracking.app.injection.user.module.UserRepositoryModule;
import dagger.Component;

@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                AndroidSystemModule.class,
                ApiModule.class,
                ConnectivityModule.class,
                DataModule.class,
                UserRepositoryModule.class,
                ThreadingModule.class,
                RepositoryModule.class,
                UseCaseModule.class,
                UtilsModule.class
        }
)
public interface ApplicationComponent extends ApplicationComponentInjects,
                                              ApplicationComponentExposes { }
