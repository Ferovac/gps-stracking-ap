package codecacao.babic.gpstracking.app.ui.ridedetails;

public final class RideEventViewModel {

    public final String eventType;
    public final String timestamp;

    public RideEventViewModel(final String eventType, final String timestamp) {
        this.eventType = eventType;
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final RideEventViewModel that = (RideEventViewModel) o;

        if (eventType != null ? !eventType.equals(that.eventType) : that.eventType != null) {
            return false;
        }
        return timestamp != null ? timestamp.equals(that.timestamp) : that.timestamp == null;
    }

    @Override
    public int hashCode() {
        int result = eventType != null ? eventType.hashCode() : 0;
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RideEventViewModel{" +
                "eventType='" + eventType + '\'' +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }
}
