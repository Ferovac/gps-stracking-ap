package codecacao.babic.gpstracking.app.injection.user;

import codecacao.babic.gpstracking.app.injection.application.ApplicationComponentExposes;
import codecacao.babic.gpstracking.app.injection.user.module.ServiceModule;
import codecacao.babic.gpstracking.app.injection.user.module.UserUseCaseModule;
import codecacao.babic.gpstracking.app.injection.user.module.UserApiClientModule;
import codecacao.babic.gpstracking.app.injection.user.module.UserRepositoryModule;

public interface UserComponentExposes extends ApplicationComponentExposes,
                                              ServiceModule.Exposes,
                                              UserApiClientModule.Exposes,
                                              UserRepositoryModule.Exposes,
                                              UserUseCaseModule.Exposes { }
