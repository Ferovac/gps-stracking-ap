package codecacao.babic.gpstracking.app.ui.ridedetails;

import javax.inject.Inject;

import codecacao.babic.gpstracking.app.base.BasePresenter;
import codecacao.babic.gpstracking.app.ui.ViewModelConverter;
import codecacao.babic.gpstracking.domain.model.RideDetails;
import codecacao.babic.gpstracking.domain.usecase.GetRideDetailsUseCase;
import rx.functions.Action1;

public final class RideDetailsPresenter extends BasePresenter<RideDetailsContract.View> implements RideDetailsContract.Presenter {

    @Inject
    GetRideDetailsUseCase getRideDetailsUseCase;

    @Inject
    ViewModelConverter viewModelConverter;

    public RideDetailsPresenter(final RideDetailsContract.View view) {
        super(view);
    }

    @Override
    public void init(final int rideId) {
        viewActionQueue.subscribeTo(getRideDetailsUseCase.execute(rideId)
                                                         .map(this::mapToRideScreenViewModel)
                                                         .map(this::mapToViewAction)
                                                         .subscribeOn(backgroundScheduler),
                                    this::logError);
    }

    private RideDetailScreenViewModel mapToRideScreenViewModel(final RideDetails rideDetails) {
        return new RideDetailScreenViewModel(rideDetails.ride.startLocation,
                                             rideDetails.ride.endLocation,
                                             String.valueOf(rideDetails.ride.numberOfPassengers),
                                             viewModelConverter.toRideEventViewModels(rideDetails.rideEvents));
    }

    private Action1<RideDetailsContract.View> mapToViewAction(final RideDetailScreenViewModel viewModel) {
        return view -> view.render(viewModel);
    }
}
