package codecacao.babic.gpstracking.app.injection.application.module;

import codecacao.babic.gpstracking.domain.repository.LocationRepository;
import codecacao.babic.gpstracking.domain.usecase.GetCurrentLocationUseCase;
import codecacao.babic.gpstracking.domain.usecase.GetCurrentLocationUseCaseImpl;
import dagger.Module;
import dagger.Provides;

@Module
public final class UseCaseModule {

    @Provides
    GetCurrentLocationUseCase provideGetCurrentLocationUseCase(final LocationRepository locationRepository) {
        return new GetCurrentLocationUseCaseImpl(locationRepository);
    }

    public interface Exposes {

        GetCurrentLocationUseCase getCurrentLocationUseCase();
    }
}
