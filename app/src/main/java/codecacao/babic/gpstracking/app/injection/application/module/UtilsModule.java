package codecacao.babic.gpstracking.app.injection.application.module;

import javax.inject.Singleton;

import codecacao.babic.gpstracking.app.ui.ViewModelConverter;
import codecacao.babic.gpstracking.app.ui.ViewModelConverterImpl;
import codecacao.babic.gpstracking.app.utils.view.ViewUtils;
import codecacao.babic.gpstracking.app.utils.view.ViewUtilsImpl;
import codecacao.babic.gpstracking.domain.utils.DateUtils;
import codecacao.babic.gpstracking.domain.utils.DateUtilsImpl;
import codecacao.babic.gpstracking.domain.utils.ListUtils;
import codecacao.babic.gpstracking.domain.utils.ListUtilsImpl;
import codecacao.babic.gpstracking.domain.utils.StringUtils;
import codecacao.babic.gpstracking.domain.utils.StringUtilsImpl;
import dagger.Module;
import dagger.Provides;

@Module
public final class UtilsModule {

    @Provides
    @Singleton
    DateUtils provideDateUtils(final StringUtils stringUtils) {
        return new DateUtilsImpl(stringUtils);
    }

    @Provides
    @Singleton
    ListUtils provideListUtils() {
        return new ListUtilsImpl();
    }

    @Provides
    @Singleton
    StringUtils provideStringUtils() {
        return new StringUtilsImpl();
    }

    @Provides
    @Singleton
    ViewModelConverter provideViewModelConverter(final DateUtils dateUtils, final StringUtils stringUtils) {
        return new ViewModelConverterImpl(dateUtils, stringUtils);
    }

    @Provides
    @Singleton
    ViewUtils provideViewUtils() {
        return new ViewUtilsImpl();
    }

    public interface Exposes {

        DateUtils dateUtils();

        ListUtils listUtils();

        StringUtils stringUtils();

        ViewModelConverter viewModelConverter();

        ViewUtils viewUtils();
    }
}
