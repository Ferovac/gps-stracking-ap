package codecacao.babic.gpstracking.app.ui.main;

import codecacao.babic.gpstracking.app.base.BasePresenter;

public final class ContentPresenter extends BasePresenter<ContentContract.View> implements ContentContract.Presenter {

    public ContentPresenter(final ContentContract.View view) {
        super(view);
    }

    @Override
    public void showSettingsScreen() {
        router.showSettingsScreen();
    }
}
