package codecacao.babic.gpstracking.app.ui.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import javax.inject.Inject;

import butterknife.BindView;
import codecacao.babic.gpstracking.app.R;
import codecacao.babic.gpstracking.app.base.BaseFragment;
import codecacao.babic.gpstracking.app.base.ScopedPresenter;
import codecacao.babic.gpstracking.app.injection.fragment.FragmentComponent;

public final class ContentFragment extends BaseFragment implements ContentContract.View {

    public static final String TAG = "ContentFragment";

    @BindView(R.id.fragment_main_view_pager)
    ViewPager viewPager;

    @BindView(R.id.fragment_main_pager_title_strip)
    PagerTitleStrip pagerTitleStrip;

    @Inject
    MainPagerAdapter pagerAdapter;

    @Inject
    ContentContract.Presenter presenter;

    public static ContentFragment newInstance() {
        return new ContentFragment();
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    protected void inject(final FragmentComponent component) {
        component.inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_content;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViewPager();
    }

    private void initViewPager() {
        viewPager.setAdapter(pagerAdapter);
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_content_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == R.id.activity_main_menu_item_settings) {
            presenter.showSettingsScreen();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public ScopedPresenter getPresenter() {
        return presenter;
    }
}
