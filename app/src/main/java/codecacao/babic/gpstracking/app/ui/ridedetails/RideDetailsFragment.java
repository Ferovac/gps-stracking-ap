package codecacao.babic.gpstracking.app.ui.ridedetails;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.annimon.stream.Optional;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import codecacao.babic.gpstracking.app.R;
import codecacao.babic.gpstracking.app.base.BaseFragment;
import codecacao.babic.gpstracking.app.base.ScopedPresenter;
import codecacao.babic.gpstracking.app.injection.fragment.FragmentComponent;

public final class RideDetailsFragment extends BaseFragment implements RideDetailsContract.View {

    public static final String TAG = "RideDetailsFragment";

    private static final String KEY_EXTRAS = "key_extras";

    @BindView(R.id.fragment_ride_details_start_location)
    TextView startLocationTextView;

    @BindView(R.id.fragment_ride_details_end_location)
    TextView endLocationTextView;

    @BindView(R.id.fragment_ride_details_number_of_passengers)
    TextView numberOfPassengersTextView;

    @BindView(R.id.fragment_ride_details_recycler_view)
    RecyclerView recyclerView;

    @BindString(R.string.ride_details_screen_start_location_template)
    String startLocationTemplate;

    @BindString(R.string.ride_details_screen_end_location_template)
    String endLocationTemplate;

    @BindString(R.string.ride_details_screen_number_of_passengers_template)
    String ageTemplate;

    @Inject
    RideDetailsContract.Presenter presenter;

    @Inject
    RideEventsAdapter adapter;

    public static RideDetailsFragment newInstance(final int rideId) {
        final Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_EXTRAS, new Extras(rideId));

        final RideDetailsFragment fragment = new RideDetailsFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        extractArguments();
    }

    private void extractArguments() {
        Optional.ofNullable(getArguments())
                .map(bundle -> (Extras)bundle.getParcelable(KEY_EXTRAS))
                .map(extras -> extras.rideId)
                .ifPresentOrElse(presenter::init,
                                 this::throwArgumentsNotPresentException);
    }

    private void throwArgumentsNotPresentException() {
        throw new IllegalStateException("Arguments not passed under key: " + KEY_EXTRAS);
    }

    @Override
    protected void inject(final FragmentComponent component) {
        component.inject(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_ride_details;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public ScopedPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void render(final RideDetailScreenViewModel viewModel) {
        startLocationTextView.setText(String.format(startLocationTemplate, viewModel.startLocation));
        endLocationTextView.setText(String.format(endLocationTemplate, viewModel.endLocation));
        numberOfPassengersTextView.setText(String.format(ageTemplate, viewModel.numberOfPassengers));

        adapter.setData(viewModel.rideEventViewModels);
    }

    static final class Extras implements Parcelable {

        final int rideId;

        Extras(final int rideId) {
            this.rideId = rideId;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.rideId);
        }

        protected Extras(Parcel in) {
            this.rideId = in.readInt();
        }

        public static final Parcelable.Creator<Extras> CREATOR = new Parcelable.Creator<Extras>() {

            @Override
            public Extras createFromParcel(Parcel source) {
                return new Extras(source);
            }

            @Override
            public Extras[] newArray(int size) {
                return new Extras[size];
            }
        };
    }
}
