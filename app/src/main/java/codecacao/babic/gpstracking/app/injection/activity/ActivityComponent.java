package codecacao.babic.gpstracking.app.injection.activity;

import codecacao.babic.gpstracking.app.injection.activity.module.ActivityModule;
import codecacao.babic.gpstracking.app.injection.activity.module.ActivityPresenterModule;
import codecacao.babic.gpstracking.app.injection.activity.module.UiAdapterModule;
import codecacao.babic.gpstracking.app.injection.scope.ActivityScope;
import codecacao.babic.gpstracking.app.injection.user.UserComponent;
import dagger.Component;

@ActivityScope
@Component(
        dependencies = {
                UserComponent.class
        },
        modules = {
                ActivityModule.class,
                ActivityPresenterModule.class,
                UiAdapterModule.class
        }
)
public interface ActivityComponent extends ActivityComponentInjects,
                                           ActivityComponentExposes {}
