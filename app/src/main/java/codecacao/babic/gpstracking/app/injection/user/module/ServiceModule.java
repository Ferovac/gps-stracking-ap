package codecacao.babic.gpstracking.app.injection.user.module;

import android.content.Context;

import codecacao.babic.gpstracking.app.application.GpsTrackingApplication;
import codecacao.babic.gpstracking.app.injection.ForApplication;
import codecacao.babic.gpstracking.app.maintenance.ForegroundServiceContract;
import codecacao.babic.gpstracking.app.maintenance.ForegroundServicePresenter;
import dagger.Module;
import dagger.Provides;

@Module
public final class ServiceModule {

    @Provides
    ForegroundServiceContract.Presenter provideForegroundServicePresenter(@ForApplication final Context context) {
        final ForegroundServicePresenter presenter = new ForegroundServicePresenter();
        GpsTrackingApplication.from(context).getUserComponent().inject(presenter);

        return presenter;
    }

    public interface Exposes {

        ForegroundServiceContract.Presenter foregroundServicePresenter();
    }
}
