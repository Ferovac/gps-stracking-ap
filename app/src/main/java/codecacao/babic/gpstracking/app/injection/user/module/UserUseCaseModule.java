package codecacao.babic.gpstracking.app.injection.user.module;

import codecacao.babic.gpstracking.domain.repository.RideRepository;
import codecacao.babic.gpstracking.domain.repository.UserRepository;
import codecacao.babic.gpstracking.domain.time.CurrentTimeProvider;
import codecacao.babic.gpstracking.domain.usecase.AddLocationUpdateEventUseCase;
import codecacao.babic.gpstracking.domain.usecase.AddLocationUpdateEventUseCaseImpl;
import codecacao.babic.gpstracking.domain.usecase.AddRideEventUseCase;
import codecacao.babic.gpstracking.domain.usecase.AddRideEventUseCaseImpl;
import codecacao.babic.gpstracking.domain.usecase.EndRideUseCase;
import codecacao.babic.gpstracking.domain.usecase.EndRideUseCaseImpl;
import codecacao.babic.gpstracking.domain.usecase.GetCurrentRideUseCase;
import codecacao.babic.gpstracking.domain.usecase.GetCurrentRideUseCaseImpl;
import codecacao.babic.gpstracking.domain.usecase.GetLastCurrentRideEventUseCase;
import codecacao.babic.gpstracking.domain.usecase.GetLastCurrentRideEventUseCaseImpl;
import codecacao.babic.gpstracking.domain.usecase.GetPreviousRidesUseCase;
import codecacao.babic.gpstracking.domain.usecase.GetPreviousRidesUseCaseImpl;
import codecacao.babic.gpstracking.domain.usecase.GetRideDetailsUseCase;
import codecacao.babic.gpstracking.domain.usecase.GetRideDetailsUseCaseImpl;
import codecacao.babic.gpstracking.domain.usecase.GetUserDataUseCase;
import codecacao.babic.gpstracking.domain.usecase.GetUserDataUseCaseImpl;
import codecacao.babic.gpstracking.domain.usecase.IsUserCreatedUseCase;
import codecacao.babic.gpstracking.domain.usecase.IsUserCreatedUseCaseImpl;
import codecacao.babic.gpstracking.domain.usecase.SaveUserDataUseCase;
import codecacao.babic.gpstracking.domain.usecase.SaveUserDataUseCaseImpl;
import codecacao.babic.gpstracking.domain.usecase.StartRideUseCase;
import codecacao.babic.gpstracking.domain.usecase.StartRideUseCaseImpl;
import codecacao.babic.gpstracking.domain.usecase.SyncPendingRideEventsUseCase;
import codecacao.babic.gpstracking.domain.usecase.SyncPendingRideEventsUseCaseImpl;
import codecacao.babic.gpstracking.domain.utils.ListUtils;
import dagger.Module;
import dagger.Provides;

@Module
public final class UserUseCaseModule {

    @Provides
    StartRideUseCase provideStartRideUseCase(final CurrentTimeProvider currentTimeProvider, final RideRepository rideRepository) {
        return new StartRideUseCaseImpl(currentTimeProvider, rideRepository);
    }

    @Provides
    AddRideEventUseCase provideAddRideEventUseCase(final RideRepository rideRepository, final CurrentTimeProvider currentTimeProvider) {
        return new AddRideEventUseCaseImpl(rideRepository, currentTimeProvider);
    }

    @Provides
    AddLocationUpdateEventUseCase provideAddLocationUpdateEventUseCase(final RideRepository rideRepository, final CurrentTimeProvider currentTimeProvider) {
        return new AddLocationUpdateEventUseCaseImpl(rideRepository, currentTimeProvider);
    }

    @Provides
    EndRideUseCase provideEndRideUseCase(final AddRideEventUseCase addRideEventUseCase, final RideRepository rideRepository) {
        return new EndRideUseCaseImpl(addRideEventUseCase, rideRepository);
    }

    @Provides
    GetCurrentRideUseCase provideGetCurrentRideUseCase(final RideRepository rideRepository) {
        return new GetCurrentRideUseCaseImpl(rideRepository);
    }

    @Provides
    GetLastCurrentRideEventUseCase provideGetLastCurrentRideEventUseCase(final RideRepository rideRepository) {
        return new GetLastCurrentRideEventUseCaseImpl(rideRepository);
    }

    @Provides
    GetPreviousRidesUseCase getPreviousRidesUseCase(final GetCurrentRideUseCase getCurrentRideUseCase, final RideRepository rideRepository, final ListUtils listUtils) {
        return new GetPreviousRidesUseCaseImpl(getCurrentRideUseCase, rideRepository, listUtils);
    }

    @Provides
    GetRideDetailsUseCase provideGetRideDetailsUseCase(final RideRepository rideRepository) {
        return new GetRideDetailsUseCaseImpl(rideRepository);
    }

    @Provides
    GetUserDataUseCase provideGetUserDataUseCase(final UserRepository userRepository) {
        return new GetUserDataUseCaseImpl(userRepository);
    }

    @Provides
    SaveUserDataUseCase provideSaveUserDataUseCase(final UserRepository userRepository) {
        return new SaveUserDataUseCaseImpl(userRepository);
    }

    @Provides
    IsUserCreatedUseCase provideIsUserCreatedUseCase(final UserRepository userRepository) {
        return new IsUserCreatedUseCaseImpl(userRepository);
    }

    @Provides
    SyncPendingRideEventsUseCase provideSyncPendingRideEventsUseCase(final RideRepository rideRepository) {
        return new SyncPendingRideEventsUseCaseImpl(rideRepository);
    }

    public interface Exposes {

        StartRideUseCase startRideUseCase();

        AddRideEventUseCase addRideEventUseCase();

        AddLocationUpdateEventUseCase addLocationUpdateEventUseCase();

        EndRideUseCase endRideUseCase();

        GetCurrentRideUseCase getCurrentRideUseCase();

        GetLastCurrentRideEventUseCase getLastCurrentRideEventUseCase();

        GetPreviousRidesUseCase getPreviousRidesUseCase();

        GetRideDetailsUseCase getRideDetailsUseCase();

        GetUserDataUseCase getUserDataUseCase();

        SaveUserDataUseCase saveUserDataUseCase();

        IsUserCreatedUseCase isUserCreatedUseCase();

        SyncPendingRideEventsUseCase syncPendingRideEventsUseCase();
    }
}
