package codecacao.babic.gpstracking.app.injection.fragment;

import codecacao.babic.gpstracking.app.injection.activity.ActivityComponentExposes;
import codecacao.babic.gpstracking.app.injection.fragment.module.FragmentPresenterModule;
import codecacao.babic.gpstracking.app.injection.fragment.module.FragmentUiModule;

public interface FragmentComponentExposes extends ActivityComponentExposes,
                                                  FragmentUiModule.Exposes,
                                                  FragmentPresenterModule.Exposes { }
