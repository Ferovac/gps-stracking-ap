package codecacao.babic.gpstracking.app.base;

public interface BackPropagatingFragment {

    boolean onBack();
}

