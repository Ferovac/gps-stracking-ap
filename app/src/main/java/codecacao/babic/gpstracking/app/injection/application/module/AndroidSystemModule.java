package codecacao.babic.gpstracking.app.injection.application.module;

import android.app.NotificationManager;
import android.content.Context;

import javax.inject.Singleton;

import codecacao.babic.gpstracking.app.injection.ForApplication;
import dagger.Module;
import dagger.Provides;

@Module
public final class AndroidSystemModule {

    @Provides
    @Singleton
    NotificationManager provideNotificationManager(@ForApplication final Context context) {
        return (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public interface Exposes {

        NotificationManager notificationManager();
    }
}
