package codecacao.babic.gpstracking.app.injection;

import codecacao.babic.gpstracking.app.application.GpsTrackingApplication;
import codecacao.babic.gpstracking.app.injection.activity.ActivityComponent;
import codecacao.babic.gpstracking.app.injection.activity.DaggerActivity;
import codecacao.babic.gpstracking.app.injection.activity.DaggerActivityComponent;
import codecacao.babic.gpstracking.app.injection.activity.module.ActivityModule;
import codecacao.babic.gpstracking.app.injection.activity.module.ActivityPresenterModule;
import codecacao.babic.gpstracking.app.injection.application.ApplicationComponent;
import codecacao.babic.gpstracking.app.injection.application.module.ApplicationModule;
import codecacao.babic.gpstracking.app.injection.application.DaggerApplicationComponent;
import codecacao.babic.gpstracking.app.injection.fragment.DaggerFragment;
import codecacao.babic.gpstracking.app.injection.fragment.DaggerFragmentComponent;
import codecacao.babic.gpstracking.app.injection.fragment.FragmentComponent;
import codecacao.babic.gpstracking.app.injection.fragment.module.FragmentModule;
import codecacao.babic.gpstracking.app.injection.fragment.module.FragmentPresenterModule;
import codecacao.babic.gpstracking.app.injection.user.DaggerUserComponent;
import codecacao.babic.gpstracking.app.injection.user.UserComponent;

public final class ComponentFactory {

    public static ApplicationComponent createApplicationComponent(final GpsTrackingApplication application) {
        return DaggerApplicationComponent.builder()
                                         .applicationModule(new ApplicationModule(application))
                                         .build();
    }

    public static UserComponent createUserComponent(final ApplicationComponent applicationComponent) {
        return DaggerUserComponent.builder().applicationComponent(applicationComponent)
                                  .build();
    }

    public static ActivityComponent createActivityComponent(final DaggerActivity activity, final GpsTrackingApplication application) {
        return DaggerActivityComponent.builder()
                                      .userComponent(application.getUserComponent())
                                      .activityModule(new ActivityModule(activity))
                                      .activityPresenterModule(new ActivityPresenterModule(activity))
                                      .build();
    }

    public static FragmentComponent createFragmentComponent(final DaggerFragment fragment, final ActivityComponent component) {
        return DaggerFragmentComponent.builder()
                                      .activityComponent(component)
                                      .fragmentModule(new FragmentModule(fragment))
                                      .fragmentPresenterModule(new FragmentPresenterModule(fragment))
                                      .build();
    }
}
