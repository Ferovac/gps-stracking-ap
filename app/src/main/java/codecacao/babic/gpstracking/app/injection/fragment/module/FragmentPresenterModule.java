package codecacao.babic.gpstracking.app.injection.fragment.module;

import codecacao.babic.gpstracking.app.injection.fragment.DaggerFragment;
import codecacao.babic.gpstracking.app.injection.scope.FragmentScope;
import codecacao.babic.gpstracking.app.ui.currentride.CurrentRideContract;
import codecacao.babic.gpstracking.app.ui.currentride.CurrentRidePresenter;
import codecacao.babic.gpstracking.app.ui.main.ContentContract;
import codecacao.babic.gpstracking.app.ui.main.ContentPresenter;
import codecacao.babic.gpstracking.app.ui.previousrides.PreviousRidePresenter;
import codecacao.babic.gpstracking.app.ui.previousrides.PreviousRidesContract;
import codecacao.babic.gpstracking.app.ui.ridedetails.RideDetailsContract;
import codecacao.babic.gpstracking.app.ui.ridedetails.RideDetailsPresenter;
import codecacao.babic.gpstracking.app.ui.settings.SettingsContract;
import codecacao.babic.gpstracking.app.ui.settings.SettingsPresenter;
import dagger.Module;
import dagger.Provides;

@Module
public final class FragmentPresenterModule {

    private final DaggerFragment fragment;

    public FragmentPresenterModule(final DaggerFragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    @FragmentScope
    ContentContract.Presenter provideMainPresenter() {
        final ContentPresenter presenter = new ContentPresenter((ContentContract.View) fragment);
        fragment.getFragmentComponent().inject(presenter);

        return presenter;
    }

    @Provides
    @FragmentScope
    CurrentRideContract.Presenter provideCurrentRidePresenter() {
        final CurrentRidePresenter presenter = new CurrentRidePresenter((CurrentRideContract.View) fragment);
        fragment.getFragmentComponent().inject(presenter);

        return presenter;
    }

    @Provides
    @FragmentScope
    PreviousRidesContract.Presenter providePreviousRidePrenter() {
        final PreviousRidePresenter presenter = new PreviousRidePresenter((PreviousRidesContract.View) fragment);
        fragment.getFragmentComponent().inject(presenter);

        return presenter;
    }

    @Provides
    @FragmentScope
    RideDetailsContract.Presenter provideRideDetailsPresenter() {
        final RideDetailsPresenter presenter = new RideDetailsPresenter((RideDetailsContract.View) fragment);
        fragment.getFragmentComponent().inject(presenter);

        return presenter;
    }

    @Provides
    @FragmentScope
    SettingsContract.Presenter provideSettingsPresenter() {
        final SettingsPresenter presenter = new SettingsPresenter((SettingsContract.View) fragment);
        fragment.getFragmentComponent().inject(presenter);

        return presenter;
    }

    public interface Exposes {

    }
}
