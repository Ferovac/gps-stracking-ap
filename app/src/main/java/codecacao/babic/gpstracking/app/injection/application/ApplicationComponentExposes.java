package codecacao.babic.gpstracking.app.injection.application;

import codecacao.babic.gpstracking.app.injection.application.module.AndroidSystemModule;
import codecacao.babic.gpstracking.app.injection.application.module.ApiModule;
import codecacao.babic.gpstracking.app.injection.application.module.ApplicationModule;
import codecacao.babic.gpstracking.app.injection.application.module.ConnectivityModule;
import codecacao.babic.gpstracking.app.injection.application.module.DataModule;
import codecacao.babic.gpstracking.app.injection.application.module.RepositoryModule;
import codecacao.babic.gpstracking.app.injection.application.module.ThreadingModule;
import codecacao.babic.gpstracking.app.injection.application.module.UseCaseModule;
import codecacao.babic.gpstracking.app.injection.application.module.UtilsModule;

public interface ApplicationComponentExposes extends ApplicationModule.Exposes,
                                                     AndroidSystemModule.Exposes,
                                                     ApiModule.Exposes,
                                                     ConnectivityModule.Exposes,
                                                     DataModule.Exposes,
                                                     RepositoryModule.Exposes,
                                                     ThreadingModule.Exposes,
                                                     UseCaseModule.Exposes,
                                                     UtilsModule.Exposes { }
