package codecacao.babic.gpstracking.app.ui.settings;

import javax.inject.Inject;

import codecacao.babic.gpstracking.app.base.BasePresenter;
import codecacao.babic.gpstracking.app.ui.ViewModelConverter;
import codecacao.babic.gpstracking.domain.model.User;
import codecacao.babic.gpstracking.domain.usecase.GetUserDataUseCase;
import rx.functions.Action1;

public final class SettingsPresenter extends BasePresenter<SettingsContract.View> implements SettingsContract.Presenter {

    @Inject
    GetUserDataUseCase getUserDataUseCase;

    @Inject
    ViewModelConverter viewModelConverter;

    public SettingsPresenter(final SettingsContract.View view) {
        super(view);
    }

    @Override
    public void init() {
        getUserData();
    }

    private void getUserData() {
        viewActionQueue.subscribeTo(getUserDataUseCase.execute()
                                                      .map(userOptional -> userOptional.orElseGet(() -> User.EMPTY))
                                                      .map(viewModelConverter::toUserViewModel)
                                                      .map(this::mapToViewAction)
                                                      .subscribeOn(backgroundScheduler),
                                    this::logError);
    }

    private Action1<SettingsContract.View> mapToViewAction(final UserViewModel viewModel) {
        return view -> view.render(new SettingsScreenViewModel(viewModel));
    }
}
