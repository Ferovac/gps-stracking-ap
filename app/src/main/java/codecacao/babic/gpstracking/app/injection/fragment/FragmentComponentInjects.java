package codecacao.babic.gpstracking.app.injection.fragment;

import codecacao.babic.gpstracking.app.ui.currentride.CurrentRideFragment;
import codecacao.babic.gpstracking.app.ui.currentride.CurrentRidePresenter;
import codecacao.babic.gpstracking.app.ui.main.ContentFragment;
import codecacao.babic.gpstracking.app.ui.main.ContentPresenter;
import codecacao.babic.gpstracking.app.ui.previousrides.PreviousRideFragment;
import codecacao.babic.gpstracking.app.ui.previousrides.PreviousRidePresenter;
import codecacao.babic.gpstracking.app.ui.ridedetails.RideDetailsFragment;
import codecacao.babic.gpstracking.app.ui.ridedetails.RideDetailsPresenter;
import codecacao.babic.gpstracking.app.ui.settings.SettingsFragment;
import codecacao.babic.gpstracking.app.ui.settings.SettingsPresenter;

public interface FragmentComponentInjects {

    void inject(ContentFragment fragment);

    void inject(ContentPresenter presenter);

    void inject(CurrentRideFragment fragment);

    void inject(CurrentRidePresenter presenter);

    void inject(PreviousRideFragment fragment);

    void inject(PreviousRidePresenter presenter);

    void inject(RideDetailsFragment fragment);

    void inject(RideDetailsPresenter presenter);

    void inject(SettingsFragment fragment);

    void inject(SettingsPresenter presenter);
}
