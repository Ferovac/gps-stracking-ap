package codecacao.babic.gpstracking.app.notification;

import android.app.Notification;

public interface NotificationFactory {

    Notification buildNotification();
}
