package codecacao.babic.gpstracking.app.ui.ridedetails;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import codecacao.babic.gpstracking.app.R;

public final class RideEventsAdapter extends RecyclerView.Adapter<RideEventsAdapter.ViewHolder> {

    private final List<RideEventViewModel> viewModels = new ArrayList<>();

    private final LayoutInflater inflater;

    public RideEventsAdapter(final LayoutInflater inflater) {
        this.inflater = inflater;
    }

    @Override
    public RideEventsAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.adapter_ride_event_row_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final RideEventsAdapter.ViewHolder holder, final int index) {
        holder.populate(viewModels.get(index));
    }

    @Override
    public int getItemCount() {
        return viewModels.size();
    }

    public void setData(final List<RideEventViewModel> viewModels) {
        this.viewModels.clear();
        this.viewModels.addAll(viewModels);
        notifyDataSetChanged();
    }

    static final class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapter_ride_events_event_type_text_view)
        TextView eventType;

        @BindView(R.id.adapter_ride_events_event_time_text_view)
        TextView eventTime;

        public ViewHolder(final View rootView) {
            super(rootView);
            bindViews(rootView);
        }

        private void bindViews(final View rootView) {
            ButterKnife.bind(this, rootView);
        }

        public void populate(final RideEventViewModel viewModel) {
            eventType.setText(viewModel.eventType);
            eventTime.setText(viewModel.timestamp);
        }

        @OnClick(R.id.adapter_ride_events_content_view)
        void onRootViewClicked() {
            // NO OP
        }
    }
}
