package codecacao.babic.gpstracking.app.injection.user;

import codecacao.babic.gpstracking.app.injection.application.ApplicationComponent;
import codecacao.babic.gpstracking.app.injection.scope.UserScope;
import codecacao.babic.gpstracking.app.injection.user.module.ServiceModule;
import codecacao.babic.gpstracking.app.injection.user.module.UserApiClientModule;
import codecacao.babic.gpstracking.app.injection.user.module.UserRepositoryModule;
import codecacao.babic.gpstracking.app.injection.user.module.UserUseCaseModule;
import dagger.Component;

@UserScope
@Component(
        dependencies = {
                ApplicationComponent.class
        },
        modules = {
                ServiceModule.class,
                UserUseCaseModule.class,
                UserApiClientModule.class,
                UserRepositoryModule.class
        }
)
public interface UserComponent extends UserComponentExposes,
                                       UserComponentInjects { }
