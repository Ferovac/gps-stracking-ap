package codecacao.babic.gpstracking.app.ui.previousrides;

import java.util.List;

public final class RideScreenViewModel {

    public final List<RideViewModel> rideViewModels;
    public final boolean showNoRidesScreen;
    public final boolean showRetryButton;

    public RideScreenViewModel(final List<RideViewModel> rideViewModels, final boolean showNoRidesScreen, final boolean showRetryButton) {
        this.rideViewModels = rideViewModels;
        this.showNoRidesScreen = showNoRidesScreen;
        this.showRetryButton = showRetryButton;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final RideScreenViewModel that = (RideScreenViewModel) o;

        if (showNoRidesScreen != that.showNoRidesScreen) {
            return false;
        }
        if (showRetryButton != that.showRetryButton) {
            return false;
        }
        return rideViewModels != null ? rideViewModels.equals(that.rideViewModels) : that.rideViewModels == null;
    }

    @Override
    public int hashCode() {
        int result = rideViewModels != null ? rideViewModels.hashCode() : 0;
        result = 31 * result + (showNoRidesScreen ? 1 : 0);
        result = 31 * result + (showRetryButton ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RideScreenViewModel{" +
                "rideViewModels=" + rideViewModels +
                ", showNoRidesScreen=" + showNoRidesScreen +
                ", showRetryButton=" + showRetryButton +
                '}';
    }
}
