package codecacao.babic.gpstracking.data.network.model;

import com.google.gson.annotations.SerializedName;

public final class ApiRideEventUpdateRequest {

    @SerializedName("bookingId")
    public final int bookingId;

    @SerializedName("status")
    public final String status;

    @SerializedName("time")
    public final String time;

    @SerializedName("lat")
    public final float lat;

    @SerializedName("lng")
    public final float lng;

    public ApiRideEventUpdateRequest(final int bookingId, final String status, final String time, final float lat, final float lng) {
        this.bookingId = bookingId;
        this.status = status;
        this.time = time;
        this.lat = lat;
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "ApiRideEventUpdateRequest{" +
                "bookingId='" + bookingId + '\'' +
                ", status='" + status + '\'' +
                ", time='" + time + '\'' +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                '}';
    }
}
