package codecacao.babic.gpstracking.data.repository;

import com.annimon.stream.Optional;

import codecacao.babic.gpstracking.data.prefs.UserSharedPrefs;
import codecacao.babic.gpstracking.domain.model.User;
import codecacao.babic.gpstracking.domain.repository.UserRepository;
import rx.Completable;
import rx.Single;

public final class UserRepositoryImpl implements UserRepository {

    private final UserSharedPrefs userSharedPrefs;

    public UserRepositoryImpl(final UserSharedPrefs userSharedPrefs) {
        this.userSharedPrefs = userSharedPrefs;
    }

    @Override
    public Completable saveUser(final User user) {
        return Completable.fromAction(() -> userSharedPrefs.saveUser(user));
    }

    @Override
    public Single<Optional<User>> getCurrentUser() {
        return Single.fromCallable(userSharedPrefs::getUser);
    }
}
