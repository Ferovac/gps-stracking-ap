package codecacao.babic.gpstracking.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.annimon.stream.Optional;

import codecacao.babic.gpstracking.domain.model.User;

public final class UserSharedPrefsImpl implements UserSharedPrefs {

    private static final String PREFS_FILE_NAME = "user_shared_preferences";

    private static final String KEY_USER_FIRST_NAME = "key_user_first_name";
    private static final String KEY_USER_LAST_NAME = "key_user_last_name";
    private static final String KEY_USER_AGE = "key_user_age";
    private static final String KEY_USER_LICENCE_PLATE= "key_user_licence_plate";
    private static final String KEY_USER_PICTURE = "key_user_picture_url";

    private final SharedPreferences preferences;

    public static UserSharedPrefsImpl create(final Context context) {
        return new UserSharedPrefsImpl(context.getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE));
    }

    private UserSharedPrefsImpl(final SharedPreferences preferences) {
        this.preferences = preferences;
    }

    @Override
    public void saveUser(final User user) {
        preferences.edit()
                   .putString(KEY_USER_FIRST_NAME, user.firstName)
                   .putString(KEY_USER_LAST_NAME, user.lastName)
                   .putInt(KEY_USER_AGE, user.age)
                   .putString(KEY_USER_LICENCE_PLATE, user.licencePlate)
                   .putString(KEY_USER_PICTURE, user.imageUrl)
                   .apply();
    }

    @Override
    public Optional<User> getUser() {
        if (preferences.contains(KEY_USER_FIRST_NAME)) {
            return Optional.of(getUserInternal());

        } else {
            clearUser();
            return Optional.empty();
        }
    }

    private User getUserInternal() {
        return new User(preferences.getString(KEY_USER_FIRST_NAME, User.EMPTY.firstName),
                        preferences.getString(KEY_USER_LAST_NAME, User.EMPTY.lastName),
                        preferences.getInt(KEY_USER_AGE, User.EMPTY.age),
                        preferences.getString(KEY_USER_LICENCE_PLATE, User.EMPTY.licencePlate),
                        preferences.getString(KEY_USER_PICTURE, User.EMPTY.imageUrl));
    }

    @Override
    public void clearUser() {
        preferences.edit()
                   .remove(KEY_USER_FIRST_NAME)
                   .remove(KEY_USER_LAST_NAME)
                   .remove(KEY_USER_AGE)
                   .remove(KEY_USER_LICENCE_PLATE)
                   .remove(KEY_USER_PICTURE)
                   .apply();
    }
}
