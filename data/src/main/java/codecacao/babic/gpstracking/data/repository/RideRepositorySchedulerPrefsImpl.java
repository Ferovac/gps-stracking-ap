package codecacao.babic.gpstracking.data.repository;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import codecacao.babic.gpstracking.domain.utils.StringUtils;

public final class RideRepositorySchedulerPrefsImpl implements RideRepositorySchedulerPrefs {

    private static final String EMPTY = "";
    private static final String CSV_SEPARATOR = ",";

    private static final String FILE_NAME = "repository_schedulers_prefs";

    private static final String KEY_RIDE_IDS_TO_SYNC = "key_ride_ids_to_sync";

    private final SharedPreferences preferences;
    private final StringUtils stringUtils;

    public static RideRepositorySchedulerPrefs create(final Context context, final StringUtils stringUtils) {
        return new RideRepositorySchedulerPrefsImpl(context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE), stringUtils);
    }

    public RideRepositorySchedulerPrefsImpl(final SharedPreferences preferences, final StringUtils stringUtils) {
        this.preferences = preferences;
        this.stringUtils = stringUtils;
    }

    @Override
    public synchronized void scheduleRideEventWithIdToBeSynced(final int id) {
        final List<Integer> ids = getRideEventIdsToBeSynced();
        ids.add(id);
        saveRideEventIdsToPrefs(ids);
    }

    @Override
    public synchronized void removeIdFromPendingIdsToBeSynced(final int id) {
        final Set<Integer> ids = new HashSet<>(getRideEventIdsToBeSynced());
        ids.remove(id);
        saveRideEventIdsToPrefs(new ArrayList<>(ids));
    }

    private void saveRideEventIdsToPrefs(final List<Integer> ids) {
        preferences.edit()
                   .putString(KEY_RIDE_IDS_TO_SYNC, stringUtils.mapIntegerListToStringWithSeparator(ids, CSV_SEPARATOR))
                   .apply();
    }

    @Override
    public synchronized List<Integer> getRideEventIdsToBeSynced() {
        return new ArrayList<>(stringUtils.parseToIntegerList(preferences.getString(KEY_RIDE_IDS_TO_SYNC, EMPTY), CSV_SEPARATOR));
    }
}
