package codecacao.babic.gpstracking.data.dao.model;

import com.google.gson.annotations.SerializedName;

public final class DbRideEvent {

    @SerializedName("id")
    public int id;

    @SerializedName("rideId")
    public int rideId;

    @SerializedName("timestamp")
    public long timestamp;

    @SerializedName("lat")
    public float lat;

    @SerializedName("lng")
    public float longitude;

    @SerializedName("eventTypeInt")
    public int eventTypeInt;

    public DbRideEvent() {

    }

    public DbRideEvent(final int id, final int rideId, final long timestamp, final float lat, final float longitude, final int eventTypeInt) {
        this.id = id;
        this.rideId = rideId;
        this.timestamp = timestamp;
        this.lat = lat;
        this.longitude = longitude;
        this.eventTypeInt = eventTypeInt;
    }
}
