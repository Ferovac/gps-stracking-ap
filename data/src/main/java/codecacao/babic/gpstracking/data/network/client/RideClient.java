package codecacao.babic.gpstracking.data.network.client;

import codecacao.babic.gpstracking.domain.model.RideEvent;
import rx.Completable;

public interface RideClient {

    Completable sendRideEvent(RideEvent rideEvent);

    Completable sendRideEventLocationUpdate(RideEvent rideEvent);
}
