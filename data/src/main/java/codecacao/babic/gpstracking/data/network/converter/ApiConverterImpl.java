package codecacao.babic.gpstracking.data.network.converter;

import codecacao.babic.gpstracking.domain.utils.DateUtils;
import codecacao.babic.gpstracking.domain.utils.StringUtils;

public final class ApiConverterImpl implements ApiConverter {

    private static final String EMPTY = "";

    private final DateUtils dateUtils;
    private final StringUtils stringUtils;

    public ApiConverterImpl(final DateUtils dateUtils, final StringUtils stringUtils) {
        this.dateUtils = dateUtils;
        this.stringUtils = stringUtils;
    }
}
