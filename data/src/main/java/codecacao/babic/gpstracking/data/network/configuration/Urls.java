package codecacao.babic.gpstracking.data.network.configuration;

public interface Urls {

    String getServerUrl();
}
