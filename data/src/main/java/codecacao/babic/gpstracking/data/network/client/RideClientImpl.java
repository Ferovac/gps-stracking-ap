package codecacao.babic.gpstracking.data.network.client;

import codecacao.babic.gpstracking.data.network.model.ApiRideEventUpdateRequest;
import codecacao.babic.gpstracking.data.network.model.ApiRideLocationUpdateRequest;
import codecacao.babic.gpstracking.data.network.service.ApiService;
import codecacao.babic.gpstracking.domain.model.RideEvent;
import codecacao.babic.gpstracking.domain.model.RideEventType;
import codecacao.babic.gpstracking.domain.utils.DateUtils;
import codecacao.babic.gpstracking.domain.utils.exception.UnimplementedSwitchClauseException;
import rx.Completable;

public final class RideClientImpl implements RideClient {

    private final ApiService apiService;
    private final DateUtils dateUtils;

    public RideClientImpl(final ApiService apiService, final DateUtils dateUtils) {
        this.apiService = apiService;
        this.dateUtils = dateUtils;
    }

    @Override
    public Completable sendRideEvent(final RideEvent rideEvent) {
        return apiService.sendRideEventUpdate(new ApiRideEventUpdateRequest(rideEvent.rideId, mapToApiStatus(rideEvent.eventType),
                                                                            dateUtils.convertToSimpleUserReadableDateFormat(rideEvent.timestamp), rideEvent.latitude,
                                                                            rideEvent.longitude))
                         .toCompletable();
    }

    @Override
    public Completable sendRideEventLocationUpdate(final RideEvent rideEvent) {
        return apiService.sendRideLocationUpdate(new ApiRideLocationUpdateRequest(rideEvent.rideId, dateUtils.convertToSimpleUserReadableDateFormat(rideEvent.timestamp),
                                                                                  rideEvent.latitude, rideEvent.longitude))
                         .toCompletable();
    }

    private String mapToApiStatus(final RideEventType eventType) {

        switch (eventType) {

            case STARTED:
                return "started";

            case CONTINUE:
                return "continue";

            case PASSENGER_PICKED:
                return "passenger_picked";

            case STOP_OVER:
                return "stop_over";

            case LOCATION_UPDATE:
                return "location_update";

            case END_RIDE:
                return "end_ride";

            default:
                throw new UnimplementedSwitchClauseException("Unknown event type for ride event: " + eventType);

        }
    }
}
