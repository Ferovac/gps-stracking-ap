package codecacao.babic.gpstracking.data.dao;

import com.annimon.stream.Optional;

import java.util.List;

import codecacao.babic.gpstracking.domain.model.RideEvent;

public interface RideEventDao {

    RideEvent saveRideEvent(RideEvent rideEvent);

    Optional<RideEvent> getRideEventById(int id);

    List<RideEvent> getRideEventsForRide(int rideId);

    List<RideEvent> getAllRideEvents();

    void clearAll();
}
