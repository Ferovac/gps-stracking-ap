package codecacao.babic.gpstracking.data.dao;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

public final class DaoFactory {

    private static final String DAO_SHARED_PREFS_FILE_NAME = "dao_prefs";

    private final Gson gson;
    private final SharedPreferences sharedPreferences;

    public static DaoFactory create(final Context context, final Gson gson) {
        return new DaoFactory(gson, context.getSharedPreferences(DAO_SHARED_PREFS_FILE_NAME, Context.MODE_PRIVATE));
    }

    public DaoFactory(final Gson gson, final SharedPreferences sharedPreferences) {
        this.gson = gson;
        this.sharedPreferences = sharedPreferences;
    }

    public RideDao createRideDao() {
        return new RideDaoImpl(sharedPreferences, gson);
    }

    public RideEventDao createRideEventDao() {
        return new RideEventDaoImpl(sharedPreferences, gson);
    }
}
