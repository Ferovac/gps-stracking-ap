package codecacao.babic.gpstracking.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.annimon.stream.Optional;

import codecacao.babic.gpstracking.domain.model.Ride;

public final class CurrentRidePreferencesImpl implements CurrentRidePreferences {

    private static final String FILE_NAME = "current_ride_prefs";

    private static final String KEY_CURRENT_RIDE_ID = "key_current_ride_id";

    private final SharedPreferences preferences;

    public static CurrentRidePreferencesImpl newInstance(final Context context) {
        return new CurrentRidePreferencesImpl(context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE));
    }

    public CurrentRidePreferencesImpl(final SharedPreferences preferences) {
        this.preferences = preferences;
    }

    @Override
    public void saveCurrentRideId(final int id) {
        preferences.edit()
                   .putInt(KEY_CURRENT_RIDE_ID, id)
                   .apply();
    }

    @Override
    public void clearCurrentRideId() {
        preferences.edit()
                   .remove(KEY_CURRENT_RIDE_ID)
                   .apply();
    }

    @Override
    public Optional<Integer> getCurrentRideId() {
        if (preferences.contains(KEY_CURRENT_RIDE_ID)) {
            return Optional.of(preferences.getInt(KEY_CURRENT_RIDE_ID, Ride.EMPTY.id));
        } else {
            return Optional.empty();
        }
    }
}
