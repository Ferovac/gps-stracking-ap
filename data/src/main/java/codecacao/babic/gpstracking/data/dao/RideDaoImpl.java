package codecacao.babic.gpstracking.data.dao;

import android.content.Context;
import android.content.SharedPreferences;

import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import codecacao.babic.gpstracking.data.dao.model.DbRide;
import codecacao.babic.gpstracking.domain.model.Ride;

public final class RideDaoImpl implements RideDao {

    private static final Comparator<DbRide> DB_RIDE_COMPARATOR = (o1, o2) -> Integer.compare(o1.id, o2.id);

    private static final String FILE_NAME = "ride_dao_impl";
    private static final String EMPTY_MAP_AS_JSON = "{}";
    private static final String KEY_RIDE_DAO_CONTENT = "key_ride_dao_content";

    private static final Type MAP_GSON_TYPE = new TypeToken<Map<Integer, DbRide>>(){}.getType();

    private final SharedPreferences preferences;
    private final Gson gson;

    public static RideDaoImpl create(final Context context, final Gson gson) {
        return new RideDaoImpl(context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE), gson);
    }

    public RideDaoImpl(final SharedPreferences preferences, final Gson gson) {
        this.preferences = preferences;
        this.gson = gson;
    }

    @Override
    public synchronized void saveRide(final Ride ride) {
        final Map<Integer, DbRide> mapToDbRide = getRideMap();
        mapToDbRide.put(ride.id, toDbRide(ride));
        saveMap(mapToDbRide);
    }

    private void saveMapToPrefs(final String mapAsString) {
        preferences.edit()
                   .putString(KEY_RIDE_DAO_CONTENT, mapAsString)
                   .apply();
    }

    private String serializeMap(final Map<Integer, DbRide> mapToDbRide) {
        return gson.toJson(mapToDbRide, MAP_GSON_TYPE);
    }

    private DbRide toDbRide(final Ride ride) {
        return new DbRide(ride.id, ride.startLocation, ride.endLocation, ride.numberOfPassengers);
    }

    @Override
    public synchronized Optional<Ride> getRideById(final int id) {
        return Optional.ofNullable(toRide(getRideMap().get(id)));
    }

    private Ride toRide(final DbRide dbRide) {
        return new Ride(dbRide.id, dbRide.startLocation, dbRide.endLocation, dbRide.numberOfPassengers);
    }

    private Map<Integer, DbRide> getRideMap() {
        return deserializeMap(getMapFromPrefs());
    }

    private Map<Integer, DbRide> deserializeMap(final String mapAsString) {
        return gson.fromJson(mapAsString, MAP_GSON_TYPE);
    }

    private String getMapFromPrefs() {
        return preferences.getString(KEY_RIDE_DAO_CONTENT, EMPTY_MAP_AS_JSON);
    }

    @Override
    public synchronized List<Ride> getAllRides() {
        return Stream.of(getRideMap().values())
                     .sorted(DB_RIDE_COMPARATOR)
                     .map(this::toRide)
                     .toList();
    }

    @Override
    public synchronized void clearAll() {
        saveMap(Collections.emptyMap());
    }

    private void saveMap(Map<Integer, DbRide> map) {
        saveMapToPrefs(serializeMap(map));
    }
}
