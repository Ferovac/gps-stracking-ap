package codecacao.babic.gpstracking.data.repository;

import java.util.List;

public interface RideRepositorySchedulerPrefs {

    void scheduleRideEventWithIdToBeSynced(int id);

    void removeIdFromPendingIdsToBeSynced(int id);

    List<Integer> getRideEventIdsToBeSynced();
}
