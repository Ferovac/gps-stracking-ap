package codecacao.babic.gpstracking.data.dao;

import com.annimon.stream.Optional;

import java.util.List;

import codecacao.babic.gpstracking.domain.model.Ride;

public interface RideDao {

    void saveRide(Ride ride);

    Optional<Ride> getRideById(int id);

    List<Ride> getAllRides();

    void clearAll();
}
