package codecacao.babic.gpstracking.data.repository;

import com.annimon.stream.Optional;

import java.util.List;

import codecacao.babic.gpstracking.data.dao.RideDao;
import codecacao.babic.gpstracking.data.dao.RideEventDao;
import codecacao.babic.gpstracking.data.network.client.RideClient;
import codecacao.babic.gpstracking.data.prefs.CurrentRidePreferences;
import codecacao.babic.gpstracking.domain.model.LocationData;
import codecacao.babic.gpstracking.domain.model.Ride;
import codecacao.babic.gpstracking.domain.model.RideEvent;
import codecacao.babic.gpstracking.domain.model.RideEventType;
import codecacao.babic.gpstracking.domain.repository.RideIdGenerator;
import codecacao.babic.gpstracking.domain.repository.RideRepository;
import rx.Completable;
import rx.Observable;
import rx.Single;

public final class RideRepositoryImpl implements RideRepository {

    private final CurrentRidePreferences currentRidePreferences;
    private final RideClient rideClient;
    private final RideDao rideDao;
    private final RideEventDao rideEventDao;
    private final RideIdGenerator rideIdGenerator;
    private final RideRepositorySchedulerPrefs rideRepositorySchedulerPrefs;

    public RideRepositoryImpl(final CurrentRidePreferences currentRidePreferences, final RideClient rideClient, final RideDao rideDao, final RideEventDao rideEventDao,
                              final RideIdGenerator rideIdGenerator, final RideRepositorySchedulerPrefs rideRepositorySchedulerPrefs) {

        this.rideClient = rideClient;
        this.rideDao = rideDao;
        this.rideEventDao = rideEventDao;
        this.rideIdGenerator = rideIdGenerator;
        this.currentRidePreferences = currentRidePreferences;
        this.rideRepositorySchedulerPrefs = rideRepositorySchedulerPrefs;
    }

    @Override
    public Single<Integer> getNewRideId() {
        return Single.fromCallable(rideIdGenerator::generateId);
    }

    @Override
    public Single<Optional<Integer>> getCurrentRideId() {
        return Single.fromCallable(currentRidePreferences::getCurrentRideId);
    }

    @Override
    public Completable startRide(final int rideId, final String startLocation, final String endLocation, final int numberOfPassengers, final long timestamp,
                                 final LocationData locationData) {

        return Single.fromCallable(() -> startRideInternal(rideId, startLocation, endLocation, numberOfPassengers, timestamp, locationData))
                     .flatMapCompletable(this::sendRideEventToApi);
    }

    private RideEvent startRideInternal(final int rideId, final String startLocation, final String endLocation, final int numberOfPassengers, final long timestamp,
                                        final LocationData locationData) {

        currentRidePreferences.saveCurrentRideId(rideId);
        rideDao.saveRide(new Ride(rideId, startLocation, endLocation, numberOfPassengers));

        return rideEventDao.saveRideEvent(new RideEvent(rideId, timestamp, locationData.lat, locationData.lng, RideEventType.STARTED));
    }

    @Override
    public Completable addRideEvent(final int rideId, final long timestamp, final LocationData locationData, final RideEventType rideEventType) {
        return Single.fromCallable(() -> addRideEventInternal(rideId, timestamp, locationData, rideEventType))
                     .flatMapCompletable(this::sendRideEventToApi);
    }

    private RideEvent addRideEventInternal(final int rideId, final long timestamp, final LocationData locationData, final RideEventType rideEventType) {
        return rideEventDao.saveRideEvent(new RideEvent(rideId, timestamp, locationData.lat, locationData.lng, rideEventType));
    }

    @Override
    public Completable sendRideEventToApi(final RideEvent rideEvent) {
        return rideClient.sendRideEvent(rideEvent)
                         .onErrorResumeNext(throwable -> saveRideEventToBeSynced(rideEvent))
                         .onErrorComplete();
    }

    @Override
    public Completable addLocationUpdateEvent(final int rideId, final long timestamp, final LocationData locationData) {
        return Single.fromCallable(() -> addRideEventInternal(rideId, timestamp, locationData, RideEventType.LOCATION_UPDATE))
                     .flatMapCompletable(this::sendLocationUpdateToApi);
    }

    @Override
    public Completable sendLocationUpdateToApi(final RideEvent rideEvent) {
        return rideClient.sendRideEventLocationUpdate(rideEvent)
                         .onErrorResumeNext(throwable -> saveRideEventToBeSynced(rideEvent))
                         .onErrorComplete();
    }

    private Completable saveRideEventToBeSynced(final RideEvent rideEvent) {
        return Completable.fromAction(() -> rideRepositorySchedulerPrefs.scheduleRideEventWithIdToBeSynced(rideEvent.id));
    }

    @Override
    public Completable endRide() {
        return Completable.fromAction(currentRidePreferences::clearCurrentRideId);
    }

    @Override
    public Single<Optional<Ride>> getRideById(final int rideId) {
        return Single.fromCallable(() -> rideDao.getRideById(rideId));
    }

    @Override
    public Single<List<RideEvent>> getRideEvents(final int rideId) {
        return Single.fromCallable(() -> rideEventDao.getRideEventsForRide(rideId));
    }

    @Override
    public Single<List<RideEvent>> getRideEventsById(final List<Integer> ids) {
        return Observable.from(ids)
                         .flatMapSingle(id -> Single.fromCallable(() -> rideEventDao.getRideEventById(id)))
                         .filter(Optional::isPresent)
                         .map(Optional::get)
                         .toList()
                         .toSingle();
    }

    @Override
    public Single<List<Integer>> getUnsyncedRideEventIds() {
        return Single.fromCallable(rideRepositorySchedulerPrefs::getRideEventIdsToBeSynced);
    }

    @Override
    public Completable removeRideEventIdFromUnsyncedIds(final int id) {
        return Completable.fromAction(() -> rideRepositorySchedulerPrefs.removeIdFromPendingIdsToBeSynced(id));
    }

    @Override
    public Single<List<Ride>> getAllRides() {
        return Single.fromCallable(rideDao::getAllRides);
    }
}
