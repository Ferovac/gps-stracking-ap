package codecacao.babic.gpstracking.data.prefs;

import com.annimon.stream.Optional;

import codecacao.babic.gpstracking.domain.model.User;

public interface UserSharedPrefs {

    void saveUser(User user);

    Optional<User> getUser();

    void clearUser();
}
