package codecacao.babic.gpstracking.data.repository;

import android.content.Context;
import android.content.SharedPreferences;

import codecacao.babic.gpstracking.domain.repository.RideIdGenerator;

public final class RideIdGeneratorImpl implements RideIdGenerator {

    private static final String FILE_NAME = "ride_id_generator_prefs";
    private static final String KEY_LAST_GENERATED_RIDE_ID = "key_last_generated_ride_id";

    private static final int DEFAULT_ID = 99;

    private final SharedPreferences preferences;

    public static RideIdGeneratorImpl create(final Context context) {
        return new RideIdGeneratorImpl(context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE));
    }

    private RideIdGeneratorImpl(final SharedPreferences preferences) {
        this.preferences = preferences;
    }

    @Override
    public synchronized int generateId() {
        final int id = preferences.getInt(KEY_LAST_GENERATED_RIDE_ID, DEFAULT_ID);

        preferences.edit()
                   .putInt(KEY_LAST_GENERATED_RIDE_ID, id + 1)
                   .apply();

        return id;
    }
}
