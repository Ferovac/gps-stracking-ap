package codecacao.babic.gpstracking.data.dao.model;

import com.google.gson.annotations.SerializedName;

public final class DbRide {

    @SerializedName("id")
    public int id;

    @SerializedName("startLocation")
    public String startLocation;

    @SerializedName("endLocation")
    public String endLocation;

    @SerializedName("numberOfPassengers")
    public int numberOfPassengers;

    public DbRide() {

    }

    public DbRide(final int id, final String startLocation, final String endLocation, final int numberOfPassengers) {
        this.id = id;
        this.startLocation = startLocation;
        this.endLocation = endLocation;
        this.numberOfPassengers = numberOfPassengers;
    }
}
