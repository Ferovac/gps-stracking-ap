package codecacao.babic.gpstracking.data.network.client;

import codecacao.babic.gpstracking.domain.model.RideEvent;
import rx.Completable;

public final class MockRideClient implements RideClient {

    private boolean connected = true;

    @Override
    public Completable sendRideEvent(final RideEvent rideEvent) {
        return completeIfConnected();
    }

    @Override
    public Completable sendRideEventLocationUpdate(final RideEvent rideEvent) {
        return completeIfConnected();
    }

    public void setConnected(final Boolean connected) {
        this.connected = connected;
    }

    private Completable completeIfConnected() {
        return connected ? Completable.complete()
                         : Completable.error(new MockClientFailureException("Mock failure"));
    }

    public static final class MockClientFailureException extends RuntimeException {

        public MockClientFailureException(final String message) {
            super(message);
        }
    }
}
