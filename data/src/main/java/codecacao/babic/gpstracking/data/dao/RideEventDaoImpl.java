package codecacao.babic.gpstracking.data.dao;

import android.content.SharedPreferences;

import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import codecacao.babic.gpstracking.data.dao.model.DbRideEvent;
import codecacao.babic.gpstracking.domain.model.RideEvent;
import codecacao.babic.gpstracking.domain.model.RideEventType;

public final class RideEventDaoImpl implements RideEventDao {

    static final Comparator<DbRideEvent> RIDE_EVENT_ID_COMPARATOR = (o1, o2) -> Integer.compare(o1.id, o2.id);
    static final Comparator<DbRideEvent> RIDE_EVENT_TIMESTAMP_COMPARATOR = (o1, o2) -> Long.compare(o1.timestamp, o2.timestamp);

    private static final Type MAP_GSON_TYPE = new TypeToken<Map<Integer, DbRideEvent>>() { }.getType();

    private static final String EMPTY_MAP_AS_JSON = "{}";
    private static final String KEY_RIDE_EVENTS = "key_ride_events";

    static final int DEFAULT_RIDE_EVENT_ID = 100;

    private final SharedPreferences preferences;
    private final Gson gson;

    public RideEventDaoImpl(final SharedPreferences preferences, final Gson gson) {
        this.preferences = preferences;
        this.gson = gson;
    }

    @Override
    public synchronized RideEvent saveRideEvent(final RideEvent rideEvent) {
        final Optional<RideEvent> lastRideEvent = getLastRideEvent();
        if (lastRideEvent.isPresent()) {
            return saveRideEventInternal(lastRideEvent.get().id + 1, rideEvent);
        } else {
            return saveRideEventInternal(DEFAULT_RIDE_EVENT_ID, rideEvent);
        }
    }

    private RideEvent saveRideEventInternal(final int rideEventId, final RideEvent rideEvent) {
        final Map<Integer, DbRideEvent> map = getRideEventsMap();
        final RideEvent newRideEvent = rideEvent.copyOnId(rideEventId);

        map.put(newRideEvent.id, toDbRide(newRideEvent));
        saveRideToPrefs(map);

        return newRideEvent;
    }

    private DbRideEvent toDbRide(final RideEvent rideEvent) {
        return new DbRideEvent(rideEvent.id, rideEvent.rideId, rideEvent.timestamp, rideEvent.latitude, rideEvent.longitude, rideEvent.eventType.value);
    }

    private void saveRideToPrefs(final Map<Integer, DbRideEvent> map) {
        preferences.edit()
                   .putString(KEY_RIDE_EVENTS, serializeMap(map))
                   .apply();
    }

    private Optional<RideEvent> getLastRideEvent() {
        return getAllRideEventsAscending().map(this::toRideEvent)
                                          .findLast();
    }

    @Override
    public synchronized Optional<RideEvent> getRideEventById(final int id) {
        return Optional.ofNullable(getRideEventsMap().get(id))
                       .map(this::toRideEvent);
    }

    @Override
    public synchronized List<RideEvent> getRideEventsForRide(final int rideId) {
        return getAllRideEventsAscending().filter(rideEvent -> rideEvent.rideId == rideId)
                                          .map(this::toRideEvent)
                                          .toList();
    }

    private Stream<DbRideEvent> getAllRideEventsAscending() {
        return Stream.of(getRideEventsMap().values())
                     .sorted(RIDE_EVENT_ID_COMPARATOR);
    }

    private RideEvent toRideEvent(final DbRideEvent dbRideEvent) {
        return new RideEvent(dbRideEvent.id, dbRideEvent.rideId, dbRideEvent.timestamp, dbRideEvent.lat, dbRideEvent.longitude, RideEventType.from(dbRideEvent.eventTypeInt));
    }

    private String serializeMap(final Map<Integer, DbRideEvent> map) {
        return gson.toJson(map, MAP_GSON_TYPE);
    }

    private Map<Integer, DbRideEvent> deserializeMap(final String mapAsString) {
        return gson.fromJson(mapAsString, MAP_GSON_TYPE);
    }

    private Map<Integer, DbRideEvent> getRideEventsMap() {
        return deserializeMap(readMapFromPrefs());
    }

    private String readMapFromPrefs() {
        return preferences.getString(KEY_RIDE_EVENTS, EMPTY_MAP_AS_JSON);
    }

    @Override
    public synchronized List<RideEvent> getAllRideEvents() {
        return getAllRideEventsAscending().map(this::toRideEvent)
                                          .toList();
    }

    @Override
    public synchronized void clearAll() {
        saveRideToPrefs(Collections.emptyMap());
    }
}
