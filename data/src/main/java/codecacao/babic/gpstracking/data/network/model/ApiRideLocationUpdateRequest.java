package codecacao.babic.gpstracking.data.network.model;

import com.google.gson.annotations.SerializedName;

public final class ApiRideLocationUpdateRequest {

    @SerializedName("bookingId")
    public final int bookingId;

    @SerializedName("time")
    public final String time;

    @SerializedName("lat")
    public final float lat;

    @SerializedName("lng")
    public final float lng;

    public ApiRideLocationUpdateRequest(final int bookingId, final String time, final float lat, final float lng) {
        this.bookingId = bookingId;
        this.time = time;
        this.lat = lat;
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "ApiRideLocationUpdateRequest{" +
                "bookingId='" + bookingId + '\'' +
                ", time='" + time + '\'' +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                '}';
    }
}
