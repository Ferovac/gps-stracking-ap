package codecacao.babic.gpstracking.data.network.service;

import codecacao.babic.gpstracking.data.network.model.ApiRideEventUpdateRequest;
import codecacao.babic.gpstracking.data.network.model.ApiRideEventUpdateResponse;
import codecacao.babic.gpstracking.data.network.model.ApiRideLocationUpdateRequest;
import codecacao.babic.gpstracking.data.network.model.ApiRideLocationUpdateResponse;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Single;

public interface ApiService {

    @POST("?json=1")
    Single<ApiRideEventUpdateResponse> sendRideEventUpdate(@Body ApiRideEventUpdateRequest request);

    @POST("?json=1")
    Single<ApiRideLocationUpdateResponse> sendRideLocationUpdate(@Body ApiRideLocationUpdateRequest request);
}
