package codecacao.babic.gpstracking.data.prefs;

import com.annimon.stream.Optional;

public interface CurrentRidePreferences {

    void saveCurrentRideId(int id);

    void clearCurrentRideId();

    Optional<Integer> getCurrentRideId();
}
