package codecacao.babic.gpstracking.data.dao;

import android.support.test.InstrumentationRegistry;

import com.annimon.stream.Optional;
import com.google.gson.Gson;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import codecacao.babic.gpstracking.data.dao.model.DbRideEvent;
import codecacao.babic.gpstracking.domain.model.RideEvent;
import codecacao.babic.gpstracking.domain.model.RideEventType;

public final class RideEventDaoTest {

    private RideEventDao rideEventDao;

    @Before
    public void setUp() throws Exception {
        final DaoFactory daoFactory = DaoFactory.create(InstrumentationRegistry.getTargetContext(), new Gson());
        rideEventDao = daoFactory.createRideEventDao();
        rideEventDao.clearAll();
    }

    @After
    public void tearDown() throws Exception {
        rideEventDao.clearAll();
    }

    @Test
    public void testRideEventDaoTimestampComparator() throws Exception {
        final List<DbRideEvent> rideEvents = new ArrayList<>();

        final DbRideEvent ride1 = new DbRideEvent(1, 10, 10, 200, 200, RideEventType.STARTED.value);
        final DbRideEvent ride2 = new DbRideEvent(2, 10, 20, 200, 200, RideEventType.PASSENGER_PICKED.value);
        final DbRideEvent ride3 = new DbRideEvent(3, 10, 30, 200, 200, RideEventType.STOP_OVER.value);

        rideEvents.add(ride3);
        rideEvents.add(ride2);
        rideEvents.add(ride1);

        Collections.sort(rideEvents, RideEventDaoImpl.RIDE_EVENT_TIMESTAMP_COMPARATOR);

        Assert.assertEquals(Arrays.asList(ride1, ride2, ride3), rideEvents);
    }

    @Test
    public void testRideEventDaoIdComparator() throws Exception {
        final List<DbRideEvent> rideEvents = new ArrayList<>();

        final DbRideEvent ride1 = new DbRideEvent(1, 10, 10, 200, 200, RideEventType.STARTED.value);
        final DbRideEvent ride2 = new DbRideEvent(2, 10, 20, 200, 200, RideEventType.PASSENGER_PICKED.value);
        final DbRideEvent ride3 = new DbRideEvent(3, 10, 30, 200, 200, RideEventType.STOP_OVER.value);

        rideEvents.add(ride3);
        rideEvents.add(ride2);
        rideEvents.add(ride1);

        Collections.sort(rideEvents, RideEventDaoImpl.RIDE_EVENT_ID_COMPARATOR);

        Assert.assertEquals(Arrays.asList(ride1, ride2, ride3), rideEvents);
    }

    @Test
    public void testSavingFirstRideEvent() throws Exception {
        final RideEvent rideEvent = new RideEvent(10, 10, 10f, 10f, RideEventType.STARTED);
        final RideEvent savedRideEvent = rideEventDao.saveRideEvent(rideEvent);
        final Optional<RideEvent> rideEventFroDao = rideEventDao.getRideEventById(savedRideEvent.id);

        Assert.assertEquals(rideEvent.copyOnId(RideEventDaoImpl.DEFAULT_RIDE_EVENT_ID), savedRideEvent);
        Assert.assertEquals(rideEvent.copyOnId(RideEventDaoImpl.DEFAULT_RIDE_EVENT_ID), rideEventFroDao.get());
    }

    @Test
    public void testFetchingEventsForRide() throws Exception {
        final RideEvent rideEvent1 = new RideEvent(10, 10, 11f, 12f, RideEventType.STARTED);
        final RideEvent rideEvent2 = new RideEvent(10, 20, 11f, 13f, RideEventType.PASSENGER_PICKED);
        final RideEvent rideEvent3 = new RideEvent(20, 30, 15f, 15f, RideEventType.STARTED);

        rideEventDao.saveRideEvent(rideEvent1);
        rideEventDao.saveRideEvent(rideEvent2);
        rideEventDao.saveRideEvent(rideEvent3);

        Assert.assertEquals(Arrays.asList(rideEvent1.copyOnId(100), rideEvent2.copyOnId(101)),
                            rideEventDao.getRideEventsForRide(rideEvent1.rideId));
    }

    @Test
    public void testFetchingNonExistingRideEvent() throws Exception {
        Assert.assertFalse(rideEventDao.getRideEventById(200).isPresent());
    }

    @Test
    public void testDatabaseClearAll() throws Exception {
        rideEventDao.saveRideEvent(new RideEvent(10, 10, 10, 10, RideEventType.STARTED));
        rideEventDao.saveRideEvent(new RideEvent(10, 10, 10, 10, RideEventType.PASSENGER_PICKED));

        rideEventDao.clearAll();

        Assert.assertEquals(Collections.emptyList(), rideEventDao.getAllRideEvents());
    }
}