package codecacao.babic.gpstracking.data.dao;

import android.support.test.InstrumentationRegistry;

import com.annimon.stream.Optional;
import com.google.gson.Gson;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import codecacao.babic.gpstracking.domain.model.Ride;

public final class RideDaoTest {

    private RideDao rideDao;

    @Before
    public void setUp() throws Exception {
        rideDao = RideDaoImpl.create(InstrumentationRegistry.getTargetContext(), new Gson());
        rideDao.clearAll();
    }

    @After
    public void tearDown() throws Exception {
        rideDao.clearAll();
    }

    @Test
    public void testSavingItemToDao() throws Exception {
        final Ride ride = new Ride(20, "start", "end", 5);

        rideDao.saveRide(ride);

        Assert.assertEquals(Optional.of(ride), rideDao.getRideById(ride.id));
    }

    @Test
    public void testGettingAllRidesInOrder() throws Exception {
        final Ride ride1 = new Ride(1, "start1", "end1", 1);
        final Ride ride2 = new Ride(2, "start2", "end2", 1);
        final Ride ride3 = new Ride(3, "start3", "end3", 1);
        final Ride ride4 = new Ride(4, "start4", "end4", 1);

        rideDao.saveRide(ride4);
        rideDao.saveRide(ride3);
        rideDao.saveRide(ride2);
        rideDao.saveRide(ride1);

        Assert.assertEquals(Arrays.asList(ride1, ride2, ride3, ride4), rideDao.getAllRides());
    }
}
